//
//  TRSearchViewController.m
//  Tip Right
//
//  Created by santanu on 05/06/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import "TRSearchViewController.h"
#import "TRGlobalHeader.h"
#import "TRSearchTableViewCell.h"
#import "TROwnProfileViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "UIImageView+WebCache.h"

@interface TRSearchViewController (){
    
    NSArray *searchResult,*followResult;
    
    NSMutableArray *searchDisplayArr;
    
    NSOperationQueue *operationQueue;
    NSDictionary *searchDict,*resultDict;
    
    int page,dataCount,totalCount;
    NSString *searchText, *followBtn_text;
    
}
@property (strong, nonatomic) IBOutlet UIView *screenView;
@property (weak, nonatomic) IBOutlet UITableView *SearchTable;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UIView *infoView;

-(void)getMoreData;

@end

@implementation TRSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
searchText=@"";
    
    
    self.view.backgroundColor = [UIColor colorWithRed:(248.0f/255.0f) green:(247.0f/255.0f) blue:(243.0f/255.0f) alpha:1.0f];
    
    searchResult=[[NSArray alloc]init];
    searchDisplayArr=[[NSMutableArray alloc]init];
    
    operationQueue=[[NSOperationQueue alloc]init];
    page=0;
    dataCount=0;
    
    _searchBar.showsCancelButton=YES;
    
    _SearchTable.hidden = YES;
    
    [self Header:self.view];
    [self Footer:self.view];
    
    [self getMoreData];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)getMoreData
{
    
    [self startLoading:self.view];
    [operationQueue addOperationWithBlock:^{
        NSString *urlString=[NSString stringWithFormat:@"%@/%@/%@/index?searchparam=%@&page=%d&limit=4&userid=%@",GLOBALAPI,INDEX,USERSEARCH,searchText,page,[[NSUserDefaults standardUserDefaults]valueForKey:UDUSERID ]];
        
        
        
        
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        //urlString =[urlString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        DebugLog(@"url:%@",urlString);
        searchDict=[[NSDictionary alloc]init];
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
        
            [self stopLoading];
            
            if(urlData==nil)
            {
                
                
                
                [[[UIAlertView alloc]initWithTitle:@"Error!!!" message:@"error in internet Connection" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                
                
                
                
            }
            else{
                
                searchDict=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                DebugLog(@"result is:%@",searchDict);
                
                
                if([[searchDict valueForKey:@"status"]isEqualToString:@"Error" ])
                {
                    
                    NSDictionary   *response=[searchDict valueForKey:@"response"];
                    
                    [_SearchTable setUserInteractionEnabled:YES];
                    
                    [[[UIAlertView alloc]initWithTitle:@"Sorry!!!" message:[response valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                    
                }
                else{
                    searchResult=[searchDict valueForKey:@"response"];
                    
                    DebugLog(@"%lu",(unsigned long)[searchResult count]);
                    
                    dataCount+=[searchResult count];
                    
                    totalCount=[[searchDict valueForKey:@"totalcount_search"]intValue];
                    
                    
                    for(NSDictionary *rowDict in searchResult)
                    {
                        
                        [searchDisplayArr addObject:rowDict];
                        
                        
                    }
                    
                    
                    [_SearchTable setUserInteractionEnabled:YES];
                    
                    [_SearchTable setHidden:NO];
                    [_infoView setHidden:YES];
                    
                    [_SearchTable reloadData];

                   
                    
                    
                }
                
            }
            
        }];
        
    }];
    
}

#pragma mark -Tableview_Delegates

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 270;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    DebugLog(@"%ld",[searchDisplayArr count]);
    
    return (NSInteger)[searchDisplayArr count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    DebugLog(@"CELL reuse %ld",indexPath.row);
    
    
    static NSString *cellIdentifier=@"cellIdentifier";
    //NSString *cellIdentifier = [NSString stringWithFormat:@"%ld,%ld",(long)indexPath.section,(long)indexPath.row];
    
    TRSearchTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        DebugLog(@"CELL ALLOC");
        cell = [[TRSearchTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
    }
    
    
    [cell.followBtn setUserInteractionEnabled:YES];
    [cell setUserInteractionEnabled:YES];
    
    cell.selectionStyle=UITableViewCellSelectionStyleDefault;
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    UIImage *img=[UIImage imageNamed:@"noimages"];
    
    
    [cell.userImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[searchDisplayArr objectAtIndex:indexPath.row]objectForKey:@"profileimage" ]]] placeholderImage:img];
    
    
    
    CALayer *imageLayer = cell.userImage.layer;
    [imageLayer setCornerRadius:25.0/375.0*self.view.frame.size.width];
    [imageLayer setBorderWidth:0.1];
    [imageLayer setMasksToBounds:YES];
    [cell.userImage.layer setMasksToBounds:YES];
    
    [cell.userName setText:[[searchDisplayArr objectAtIndex:indexPath.row]objectForKey:@"name" ] ];
    
    //[cell.userName setText:@"barsan"];
    if(IsIphone6plus)
        cell.userName.font=[cell.userName.font fontWithSize:17];
    else if(IsIphone6)
        cell.userName.font=[cell.userName.font fontWithSize:15];
    else
        cell.userName.font=[cell.userName.font fontWithSize:13];
    
    [cell.userDesc setText:@"works at esolz"];
    if(IsIphone6plus)
        [cell.userDesc setFont:[UIFont fontWithName:HELVETICAProLight size:15]];
    else if(IsIphone6)
        [cell.userDesc setFont:[UIFont fontWithName:HELVETICAProLight size:13]];
    else
        [cell.userDesc setFont:[UIFont fontWithName:HELVETICAProLight size:10]];
    
    [cell.picLarge sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[searchDisplayArr objectAtIndex:indexPath.row]objectForKey:@"coverimage" ]]] placeholderImage:[UIImage imageNamed:@"No-image-found"]];
    
    [cell.picSmall1 sd_setImageWithURL:nil placeholderImage:[UIImage imageNamed:@"pic1"]];
    [cell.picSmall2 sd_setImageWithURL:nil placeholderImage:[UIImage imageNamed:@"pic2"]];
    [cell.picSmall3 sd_setImageWithURL:nil placeholderImage:[UIImage imageNamed:@"pic3"]];
    
    
    [cell.moreView setBackgroundColor:[UIColor colorWithRed:222/255.0 green:81/255.0 blue:5/255.0 alpha:1]];
    
    // [cell.moreView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"pic4"]]];
    
    
    
    [cell.followBtn setTitleColor:[UIColor colorWithRed:222/255.0 green:81/255.0 blue:5/255.0 alpha:1] forState:UIControlStateNormal];
    [cell.followBtn setTag:[[[searchDisplayArr objectAtIndex:indexPath.row]objectForKey:@"userid" ] floatValue]];
    
    


    if([[[searchDisplayArr objectAtIndex:indexPath.row]objectForKey:@"check_follow" ] intValue]==1)
    {
        
        [cell.followBtn setTitle:@"Unfollow" forState:UIControlStateNormal];
    }
    else if([[[searchDisplayArr objectAtIndex:indexPath.row]objectForKey:@"check_follow" ] intValue]==0)
    {
       
        [cell.followBtn setTitle:@"Follow" forState:UIControlStateNormal];
    }
    else{
   
        [cell.followBtn setTitle:@"Withdraw" forState:UIControlStateNormal];
        
        
    }
    

    
    
    
    
    CGRect line=cell.separatorLine.frame;
    
    line.size.height=0.5f;
    cell.separatorLine.frame=line;
    
    
    
    
    
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    TRSearchTableViewCell *cCell=(TRSearchTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    
    
    
    TROwnProfileViewController *pushView = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"profile"];
    pushView.userId=[[searchDisplayArr objectAtIndex:indexPath.row]objectForKey:@"userid" ] ;
    //[[NSUserDefaults standardUserDefaults]setObject:[[searchDisplayArr objectAtIndex:indexPath.row]objectForKey:@"userid" ] forKey:@"globalID"];
    
    if(![[[searchDisplayArr objectAtIndex:indexPath.row] objectForKey:@"userid"] isEqualToString:[[NSUserDefaults standardUserDefaults]valueForKey:UDUSERID]])
    {
        pushView.isfromOther = TRUE;
    }

    pushView.FollowCheck=[[[searchDisplayArr objectAtIndex:indexPath.row]objectForKey:@"check_follow" ]intValue] ;
    
    
    
    
    if([[[searchDisplayArr objectAtIndex:indexPath.row]objectForKey:@"check_follow" ] intValue]==1)
    {
        
         pushView.follow_text=@"Unfollow";
    }
    else if([[[searchDisplayArr objectAtIndex:indexPath.row]objectForKey:@"check_follow" ] intValue]==0)
    {
        
   pushView.follow_text=@"Follow";
    }
    else{
        
         pushView.follow_text=@"Withdraw";
        
        
    }
    
    
    
   // pushView.follow_text = cCell.followBtn.titleLabel.text;
    
    [[self navigationController ]pushViewController:pushView animated:YES];
    
    
}


- (IBAction)followClick:(UIButton *)sender {
    
    DebugLog(@"hello user:%ld",sender.tag);
    
//http://www.esolz.co.in/lab3/tipright/index.php/follow_ios/index?userid=123&Unfollow_id=180
    
   
    TRSearchTableViewCell *cCell= (TRSearchTableViewCell *)sender.superview.superview;
    
    [cCell setUserInteractionEnabled:NO];
    
     [sender setUserInteractionEnabled:NO];
    

    
    
    
    
    [operationQueue addOperationWithBlock:^{
        NSString *urlString=[NSString stringWithFormat:@"%@/%@/%@/index?userid=%@&following_id=%ld",GLOBALAPI,INDEX,FOLLOW,[[NSUserDefaults standardUserDefaults]valueForKey:UDUSERID ],sender.tag];
        
        
        
        
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        //urlString =[urlString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        DebugLog(@"url:%@",urlString);
        resultDict=[[NSDictionary alloc]init];
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            if(urlData==nil)
            {
                [cCell setUserInteractionEnabled:NO];
                
                [sender setUserInteractionEnabled:NO];
                
                
                [[[UIAlertView alloc]initWithTitle:@"Error!!!" message:@"error in internet Connection" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                
                
                
                
            }
            else{
                
                resultDict=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                DebugLog(@"result is:%@",resultDict);
                
                NSDictionary   *response=[resultDict valueForKey:@"response"];
                
                
                if([[response valueForKey:@"status"]isEqualToString:@"Success" ])
                {

                
                    page=0;
                    dataCount=0;
                    [searchDisplayArr removeAllObjects];
                        [self getMoreData];
            
                    
                    
                }
                else{
                    
                    [cCell setUserInteractionEnabled:NO];
                    
                    [sender setUserInteractionEnabled:NO];
                    
                    [[[UIAlertView alloc]initWithTitle:@"Sorry!!!" message:[response valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                    
                    
                    
                }
            }
        }];
    }];
    
    
}

- (IBAction)moreClick:(id)sender {
    
}

- (IBAction)userClick:(id)sender {
    
}



- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    
    
    return YES;
    
}// return NO to not become first responder
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    
}// called when text starts editing
- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    return YES;
    
}// return NO to not resign first responder
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    
       
}// called when text ends editing
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    
    
    
}// called when text changes (including clear)
- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text NS_AVAILABLE_IOS(3_0)
{
    if([text length]==0){
        DebugLog(@"Previous");
    }
    
    return YES;
    
}// called before text changes

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    
    page=0;
    dataCount=0;
    [searchDisplayArr removeAllObjects];
    
    [searchBar resignFirstResponder];
    
    //searchText=searchBar.text;
    
    searchBar.text = [searchBar.text stringByTrimmingCharactersInSet:
                     [NSCharacterSet whitespaceCharacterSet]];
    
    
    searchText = [searchBar.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
[self startLoading:self.view];
    
    [operationQueue addOperationWithBlock:^{
        NSString *urlString=[NSString stringWithFormat:@"%@/%@/%@/index?searchparam=%@&page=%d&limit=4&userid=%@",GLOBALAPI,INDEX,USERSEARCH,searchText,page,[[NSUserDefaults standardUserDefaults]valueForKey:UDUSERID ]];
        
        
      
        //urlString =[urlString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        DebugLog(@"url:%@",urlString);
        searchDict=[[NSDictionary alloc]init];
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            [self stopLoading];
            
            if(urlData==nil)
            {
                
             
                
                [[[UIAlertView alloc]initWithTitle:@"Error!!!" message:@"error in internet Connection" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                
                
                
                
            }
            else{
                
                searchDict=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                DebugLog(@"result is:%@",searchDict);
                
                
                if([[searchDict valueForKey:@"status"]isEqualToString:@"Error" ])
                {
                   
                    
                    NSDictionary   *response=[searchDict valueForKey:@"response"];
                    
                    [_SearchTable setUserInteractionEnabled:NO];
                    [_SearchTable setHidden:YES];
                    
                    [_infoView setHidden:NO];
                    [_infoLabel setText:[response valueForKey:@"message"]];
                    
                    // [[[UIAlertView alloc]initWithTitle:@"Sorry!!!" message:[response valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                    
                }
                else{
                    searchResult=[searchDict valueForKey:@"response"];
                    
                    DebugLog(@"%lu",(unsigned long)[searchResult count]);
                    
                    dataCount+=[searchResult count];
                    
                    totalCount=[[searchDict valueForKey:@"totalcount_search"]intValue];
                    
                    
                    for(NSDictionary *rowDict in searchResult)
                    {
                        
                        [searchDisplayArr addObject:rowDict];
                        
                        
                    }
                    
                    
                    [self stopLoading];
                    
                    [_SearchTable setUserInteractionEnabled:YES];
                    [_SearchTable setHidden:NO];
                    [_infoView setHidden:YES];
                    
                    [_SearchTable reloadData];
                    
                    
                }
                
            }
            
        }];
        
    }];
        
 
    
}// called when keyboard search button pressed

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    
    searchBar.text=@"";
    
    [searchBar resignFirstResponder];
    
    [self stopLoading];
    [self viewDidLoad];
   // [self getMoreData];
    
    
}





#pragma UIScrollViewDelegate

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGPoint offset = scrollView.contentOffset;
    CGRect bounds = scrollView.bounds;
    CGSize size = scrollView.contentSize;
    UIEdgeInsets inset = scrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    
    float reload_distance = -60.0f;
    if(y > h + reload_distance)
    {
        
        
        DebugLog(@"total count is%d and current count is %d",totalCount,dataCount);
        
        
        if (dataCount<totalCount)
        {
            
            
            
            [_SearchTable setUserInteractionEnabled:NO];
            
            page++;
            
            [self performSelector:@selector(getMoreData) withObject:nil];
        }
    }
    
    
}



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
