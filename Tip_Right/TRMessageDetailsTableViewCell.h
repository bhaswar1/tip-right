//
//  TRMessageDetailsTableViewCell.h
//  Tip Right
//
//  Created by santanu on 30/06/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TRGlobalMethods.h"
@interface TRMessageDetailsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *otherUserImg;
@property (weak, nonatomic) IBOutlet UILabel *otherUserName;
@property (weak, nonatomic) IBOutlet UILabel *otherUserMsg;
@property (weak, nonatomic) IBOutlet UILabel *otherMsgTime;






@end
