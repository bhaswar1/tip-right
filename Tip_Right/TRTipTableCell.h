//
//  TRTipTableCell.h
//  Tip Right
//
//  Created by santanu on 17/06/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TRGlobalMethods.h"

@interface TRTipTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconImg1;
@property (weak, nonatomic) IBOutlet UIImageView *iconImg2;
@property (weak, nonatomic) IBOutlet UIImageView *iconImg3;
@property (weak, nonatomic) IBOutlet UIImageView *iconImg4;

@property (weak, nonatomic) IBOutlet UITextField *timeField;
@property (weak, nonatomic) IBOutlet UITextField *typeField;
@property (weak, nonatomic) IBOutlet UITextField *priceField;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (weak, nonatomic) IBOutlet UITextView *tipDetails;
@property (weak, nonatomic) IBOutlet UIView *separator;
@property (weak, nonatomic) IBOutlet UILabel *placeholder;
@end
