//
//  TRProfileCellTableViewCell.h
//  Tip Right
//
//  Created by intel on 12/06/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TRProfileCellTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIView *View1;
@property (strong, nonatomic) IBOutlet UIView *View2;
@property (strong, nonatomic) IBOutlet UIImageView *cell_image1;
@property (strong, nonatomic) IBOutlet UIImageView *cell_image2;
@property (strong, nonatomic) IBOutlet UIImageView *cell_image3;
@property (strong, nonatomic) IBOutlet UIImageView *cell_image4;
@property (strong, nonatomic) IBOutlet UIImageView *cell_image5;
@property (strong, nonatomic) IBOutlet UIImageView *cell_image6;
@property (strong, nonatomic) IBOutlet UIImageView *cell_image7;
@property (strong, nonatomic) IBOutlet UIImageView *cell_image8;


@end
