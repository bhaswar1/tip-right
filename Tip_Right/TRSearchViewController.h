//
//  TRSearchViewController.h
//  Tip Right
//
//  Created by santanu on 05/06/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import "TRGlobalMethods.h"

@interface TRSearchViewController : TRGlobalMethods<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>

@end
