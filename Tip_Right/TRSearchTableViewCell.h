//
//  TRSearchTableViewCell.h
//  Tip Right
//
//  Created by santanu on 05/06/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TRSearchTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *userImage;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *userDesc;
@property (weak, nonatomic) IBOutlet UIImageView *picLarge;
@property (weak, nonatomic) IBOutlet UIImageView *picSmall1;
@property (weak, nonatomic) IBOutlet UIImageView *picSmall2;
@property (weak, nonatomic) IBOutlet UIImageView *picSmall3;
@property (weak, nonatomic) IBOutlet UIButton *picLargeBtn;
@property (weak, nonatomic) IBOutlet UIButton *picSmallBtn1;
@property (weak, nonatomic) IBOutlet UIButton *picSmallBtn2;
@property (weak, nonatomic) IBOutlet UIButton *picSmallBtn3;

@property (weak, nonatomic) IBOutlet UIButton *followBtn;

@property (weak, nonatomic) IBOutlet UIButton *moreBtn;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;

@property (weak, nonatomic) IBOutlet UIView *moreView;
@property (weak, nonatomic) IBOutlet UIView *separatorLine;

@end
