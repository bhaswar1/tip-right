//
//  TRGlobalMethods.h
//  Tip Right
//
//  Created by santanu on 29/05/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TRGlobalHeader.h"
#import "UIImageView+WebCache.h"



@interface TRGlobalMethods : UIViewController<UIAlertViewDelegate,UIScrollViewDelegate,UITextFieldDelegate>

-(void)popToViewController:(BOOL)animation;
-(void)Header:(UIView *)mainView;
-(void)Footer:(UIView *)mainView;
-(void)startLoading:(UIView *)loaderView;
-(void)stopLoading;
@property (nonatomic, assign) BOOL tipView;
@property(nonatomic,retain) UILabel *msgCount;
@property(nonatomic,retain)UIImageView *message_count ;






@end
