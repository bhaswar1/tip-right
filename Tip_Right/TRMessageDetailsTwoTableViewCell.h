//
//  TRMessageDetailsTwoTableViewCell.h
//  Tip Right
//
//  Created by santanu on 30/06/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TRMessageDetailsTwoTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *userImg;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *userMsg;
@property (weak, nonatomic) IBOutlet UILabel *msgTime;

@end
