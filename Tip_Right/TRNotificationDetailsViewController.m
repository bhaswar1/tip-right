 //
//  TRNotificationDetailsViewController.m
//  Tip Right
//
//  Created by intel on 26/06/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import "TRNotificationDetailsViewController.h"
#import "TRNotificationsViewController.h"
#import "UIImageView+WebCache.h"
#import "TROwnProfileViewController.h"

@interface TRNotificationDetailsViewController ()<UITableViewDataSource, UITableViewDelegate,UITextFieldDelegate>
{
    NSOperationQueue *operationQueue,*innerOperationQueue;
    NSDictionary *result, *tipDict, *result1, *result3;
    NSMutableArray *response,*response2;
    NSArray *tipImageArray, *tipArr;
    NSMutableArray *DisplayResponse, *commentArr;
    UITableView *_contentTable;
    UITextField *commentField;
    UILabel *cell_div, *commentlbl;
    int commentPage,commentCount,totalCommentCount;
    
    UIView *popupView,*blackView,*commentView;
    UITableView *CommentTable;
    int liked;
    
    __weak IBOutlet UIScrollView *scrollView;
    
    __weak IBOutlet UIButton *copyBet;
    
    UILabel *timeLabel,*tipType,*currency;
    UIImageView *logo,*time_div,*tipType_div,*priceDiv;
    NSString *dateTime;
    
    BOOL isTime,isType,isPrice;
    int followedBy;
}
@end

@implementation TRNotificationDetailsViewController
@synthesize tipId,like;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   
   
    [self Header:self.view];
    [self Footer:self.view];
    
    commentArr = [[NSMutableArray alloc] init];
    DisplayResponse = [[NSMutableArray alloc] init];
    _profilePic.layer.cornerRadius = _profilePic.frame.size.width/2;
    _profilePic.clipsToBounds = YES;
   tipImageArray=@[@"horse",@"ball",@"hand",@"football"];

        operationQueue =[[NSOperationQueue alloc]init];
    innerOperationQueue =[[NSOperationQueue alloc]init];
}

-(void)viewDidAppear:(BOOL)animated
{
    
    [self startLoading:self.view];
    
    
    CALayer *imageLayer2 = copyBet.layer;
    [imageLayer2 setCornerRadius:15.0/375.0*self.view.frame.size.width];
    
    [imageLayer2 setMasksToBounds:YES];
    
   // [copyBet setBackgroundImage:[UIImage imageNamed:@"copy-ber"] forState:UIControlStateNormal];
    
    [copyBet setBackgroundColor:[UIColor colorWithRed:(255.0f/255.0f) green:(127.0f/255.0f) blue:(0.0f/255.0f) alpha:1]];

    [copyBet setTitle:@"COPY BET" forState:UIControlStateNormal];
    
    //http://esolz.co.in/lab3/tipright/index.php/profile_ios/tipdetail?id=163
    
//    if ([[[response objectAtIndex:0] objectForKey:@"tip_image"]length]>0) {
//         [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, 280.0/568.0*self.view.frame.size.height)];
//    }
//    else{
//    
//        CGRect imageRect=_banner_Image.frame;
//        
//        imageRect.size.height=0;
//        _banner_Image.frame=imageRect;
//        
//        
//         [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, 75.0/568.0*self.view.frame.size.height)];
//    }
//    
   
    UIImageView *delete_image = [[UIImageView alloc] init];
    delete_image.frame = CGRectMake(scrollView.frame.size.width-25/375.0*self.view.frame.size.width, 22.0/677.0*self.view.frame.size.height, 20, 20);
    delete_image.image = [UIImage imageNamed:@"close-if"];
    delete_image.layer.cornerRadius = delete_image.frame.size.width/2;
    [scrollView addSubview:delete_image];
    
    UIButton *delete_cross = [[UIButton alloc] init];
    delete_cross.frame = CGRectMake(scrollView.frame.size.width-35, 9, 35, 50);
    
    
    //[delete_cross setBackgroundColor:[UIColor redColor]];
    
    //delete_cross.tag = indexPath.row;
    
    [delete_cross addTarget:self action:@selector(tipDelete:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [scrollView addSubview:delete_cross];
    
    delete_image.hidden = YES;
    delete_cross.hidden = YES;
    
    

    
    [operationQueue addOperationWithBlock:^{
        
        NSString *urlString=[NSString stringWithFormat:@"%@/%@/%@/tipdetail?id=%@",GLOBALAPI,INDEX, PROFILE,tipId];
        
        
        DebugLog(@" url:%@",urlString);
        
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        result=[[NSDictionary alloc]init];
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
            if(urlData==nil)
            {
                
                [[[UIAlertView alloc]initWithTitle:@"Error!!!" message:@"error in internet Connection" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                
            }
            else{
               
                       [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                
                result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                DebugLog(@"Tip Details result is:%@",result);
                
                if([[result valueForKey:@"status"]isEqualToString:@"Error" ])
                {
                    response=[result objectForKey:@"response"];
                    
                }
                else{
                    
                   

                    
                    
                        response=[[result valueForKey:@"response"]valueForKeyPath:@"data"];
                    
                    if ([[[response objectAtIndex:0] objectForKey:@"user_id"] isEqualToString:[[NSUserDefaults standardUserDefaults]valueForKey:UDUSERID ]]) {
                        delete_image.hidden = NO;
                        delete_cross.hidden = NO;
                        
                        delete_cross.tag = [[[response objectAtIndex:0] objectForKey:@"count_tip_id"] intValue];
                    }
                    else
                    {
                        delete_image.hidden = YES;
                        delete_cross.hidden = YES;
                    }
                    
                    
                    if ([[[response objectAtIndex:0] objectForKey:@"tip_image"]length]>0) {
                        [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, 280.0/568.0*self.view.frame.size.height)];
                    }
                    else{
                        
                        CGRect imageRect=_banner_Image.frame;
                        
                        imageRect.size.height=0;
                        _banner_Image.frame=imageRect;
                        
                        
                        [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, 75.0/568.0*self.view.frame.size.height)];
                    }
                    
                    
                    
                    DebugLog(@"scroll frame height %f",scrollView.contentSize.height);
                
                    
                    
                    [_profilePic sd_setImageWithURL:[[response objectAtIndex:0] objectForKey:@"profileimage"]placeholderImage:[UIImage imageNamed:@"profile-pic-2"] ] ;
                  
                    [_profName setText:[NSString stringWithFormat:@"%@",[[response objectAtIndex:0] objectForKey:@"user_name"] ]];
                    
                    [_banner_Image sd_setImageWithURL:[[response objectAtIndex:0] objectForKey:@"tip_image"] placeholderImage:[UIImage imageNamed:@"loading-image"] ];
                    
                    
                    DebugLog(@"comment count%@",response);
                    
                    if ([[[response objectAtIndex:0] objectForKey:@"like_status"]boolValue] ) {
                        _heartImage.image = [UIImage imageNamed:@"heart"];
                    }
                    else{
                      _heartImage.image = [UIImage imageNamed:@"heart-1"];
                    }
                    
                    
                    liked=[[[response objectAtIndex:0] objectForKey:@"like"]intValue];
                    
                            _like_status.text = [NSString stringWithFormat:@" %d people liked",liked  ];
                    
                    
                    tipArr=[[NSArray alloc]init];
                    
                    tipArr =[[response objectAtIndex:0]objectForKey:@"tip"];
                    DebugLog(@"TIP ARRAY:%@",[tipArr objectAtIndex:0]);
                    
#pragma mark- time difference calculation
                    
                    NSDate *now=[NSDate date];
                    
                    
                    NSDateFormatter *formatter2 = [[NSDateFormatter alloc]init];
                    [formatter2 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                    // [formatter2 setTimeZone:[NSTimeZone timeZoneWithName:@"Japan"]];
                    NSString *nowS=[formatter2 stringFromDate:now];
                    
                    
                    NSDate *dateJ = [formatter2 dateFromString:nowS];
                    
                    //    NSLog(@"CURRENT DATE:%@",dateJ);
                    
                    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
                    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                    
                    NSDate *date = [formatter dateFromString:[NSString stringWithFormat:@"%@",[[tipArr objectAtIndex:0]objectForKey:@"added_date"]]];
                    
                    //NSLog(@"old DATE:%@ and %@",[[dataDict objectAtIndex:indexPath.row] objectForKey:@"record_date"] ,date);
                    
                    NSTimeInterval interval = [dateJ timeIntervalSinceDate:date];
                    
                    
                    
                    int hours = (int)interval / 3600;
                    // integer division to get the hours part
                    
                    int days=hours/24;
                    int minutes = (interval - (hours*3600)) / 60; // interval minus hours part (in seconds) divided by 60 yields minutes
                    
                    
                    //   NSString *timeDiff = [NSString stringWithFormat:@"%d:%d:%02d",days, hours, minutes];
                    
                    //    NSLog(@"%@",timeDiff);
                    
                    
                    //
                    
                    if (days>=365) {
                        
               
                        int year = days/365;
                        [_timeAgo
                         setText:[NSString stringWithFormat:@"%d %@",year,@"Years ago"]];
                        
                    }
                    
                    else if ((days/30)>0) {
                        
                        [_timeAgo setText:[NSString stringWithFormat:@"%d Months ago",(int)days/30]];
                    }
                    
                    /*   else if(days>0)
                     {
                     
                     [timeAgo setText:[NSString stringWithFormat:@"%d %@",days,@"Days ago"]];
                     }
                     else */if (hours>0) {
                         
                         
                         
                         [_timeAgo setText:[NSString stringWithFormat:@"%d %@",hours,@"Hours ago"]];
                     }
                     else if (minutes>0) {
                         
                         
                         [_timeAgo setText:[NSString stringWithFormat:@"%d %@",minutes,@"Minutes ago"]];
                         
                     }
                    
                     else
                     {
                         [_timeAgo setText:@"Just Now"];
                     }
                    

                    
                    
                    
          
                    

                    }
                
                [self performSelector:@selector(displayData) withObject:nil];
                     }];
            }
            
    
    }];
    
}

-(void)displayData
{
    int height=26;
    int totalHeight=0;
    int yPos =_banner_Image.frame.origin.y+_banner_Image.frame.size.height+20;
    
    for(NSDictionary *innerDict in tipArr)
    {
        
        int yposInit=yPos;
        
        isTime=NO;
        isType=NO;
        isPrice=NO;
        
        
        
        
        
        timeLabel = [[UILabel alloc] init];
        time_div = [[UIImageView alloc] init];
        tipType = [[UILabel alloc] init];
        tipType_div = [[UIImageView alloc] init];
        currency = [[UILabel alloc] init];
        logo = [[UIImageView alloc] init];
        priceDiv=[[UIImageView alloc]init];
        
        
        
        dateTime = [[innerDict valueForKey:@"date_time"]stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        if(![dateTime isEqualToString:@""])
        {

            
            
            timeLabel.frame = CGRectMake(25.0/375.0*self.view.frame.size.width+10, yPos+2,self.view.frame.size.width-50.0/375.0*self.view.frame.size.width, height);
            time_div.frame = CGRectMake(25.0/375.0*self.view.frame.size.width+10, timeLabel.frame.origin.y+timeLabel.frame.size.height, timeLabel.frame.size.width,0.5);
            
            [scrollView addSubview:timeLabel];
            [scrollView addSubview:time_div];
            
            
            
            
            isTime=YES;
            
            yPos+=height+5;
            totalHeight+=35;
            
        }
        
        if([[[innerDict valueForKey:@"tip_type"]stringByReplacingOccurrencesOfString:@" " withString:@""]  length]>0 )
        {
            
            

            
            
            tipType.frame = CGRectMake(25.0/375.0*self.view.frame.size.width+10, yPos+2,self.view.frame.size.width-50.0/375.0*self.view.frame.size.width, height);
            
            tipType_div.frame = CGRectMake(25.0/375.0*self.view.frame.size.width+10, tipType.frame.origin.y+tipType.frame.size.height, tipType.frame.size.width,0.5);
            
            
            [scrollView addSubview:tipType];
            [scrollView addSubview:tipType_div];
            isType=YES;
            
            yPos+=height+5;
             totalHeight+=35;
            
        }
        
        
        
        if([[[innerDict valueForKey:@"price"]stringByReplacingOccurrencesOfString:@" " withString:@""]  length]>0 )
        {
            
            

            
            
            currency.frame = CGRectMake(25.0/375.0*self.view.frame.size.width+10, yPos+2,self.view.frame.size.width-50.0/375.0*self.view.frame.size.width, height);
            
            
            
            priceDiv.frame = CGRectMake(25.0/375.0*self.view.frame.size.width+10, currency.frame.origin.y+currency.frame.size.height, currency.frame.size.width,0.5 );
            
            
            [scrollView addSubview:currency];
            [scrollView addSubview:priceDiv];
            
            isPrice=YES;
            
            yPos+=height+5;
             totalHeight+=35;
            
        }
        

        
        
        
        if(isType || isTime || isPrice)
        {

            
                   logo.frame = CGRectMake(0/375.0*self.view.frame.size.width+5, yposInit+((yPos-yposInit)/2)-14.0/375.0*self.view.frame.size.width, 25.0/375.0*self.view.frame.size.width, 28.0/667.0*self.view.frame.size.height);
            
            
            [scrollView addSubview:logo];
        }
        else{

            
            
                logo.frame = CGRectMake(self.view.frame.size.width/2-13.0/375.0*self.view.frame.size.width, yPos-5.0/667.0*self.view.frame.size.height, 25.0/375.0*self.view.frame.size.width, 28.0/667.0*self.view.frame.size.height);
            
            
            [scrollView addSubview:logo];
            
            UIImageView *logoDiv;
            
            logoDiv=[[UIImageView alloc]init];
            

            
                 logoDiv.frame = CGRectMake(0, logo.frame.origin.y+logo.frame.size.height+2, self.view.frame.size.width,0.5 );
            
            //logoDiv.image = [UIImage imageNamed:@"divider"];
            
            [logoDiv setBackgroundColor:[UIColor lightGrayColor]];
            
            [scrollView addSubview:logoDiv];
            
            yPos+=height+5;
             totalHeight+=35;
        }
        
        

        
         UILabel *descriptionLabel=[[UILabel alloc]initWithFrame:CGRectMake(25.0/375.0*self.view.frame.size.width+10, yPos+5, self.view.frame.size.width-50.0/375.0*self.view.frame.size.width, 0)];
        
        descriptionLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:11.0];
        descriptionLabel.text = [NSString stringWithFormat:@"%@",[innerDict valueForKey:@"description"] ];
        descriptionLabel.textAlignment = NSTextAlignmentCenter;
        descriptionLabel.textColor = [UIColor blackColor];
        
        descriptionLabel.numberOfLines = 0;
        
        
        
        //[cell2.userMsg sizeToFit];
        
        CGSize size = [ descriptionLabel.text sizeWithFont:descriptionLabel.font
                                         constrainedToSize:CGSizeMake(descriptionLabel.frame.size.width, MAXFLOAT)
                                             lineBreakMode:NSLineBreakByWordWrapping];
        CGRect  labelFrame =  descriptionLabel.frame;
        
        labelFrame.size.height = size.height;
        descriptionLabel.frame = labelFrame;
        
        [scrollView addSubview:descriptionLabel];
        
        
        
        timeLabel.font = [UIFont fontWithName:@"Helvetica Neue LT Pro" size:13.0];
        
        dateTime = [[innerDict valueForKey:@"date_time"]stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        if(![dateTime isEqualToString:@""])
        {
            DebugLog(@"DATE TIME:%@",dateTime);
            timeLabel.text = [NSString stringWithFormat:@"%@",[innerDict valueForKey:@"date_time"]];
        }
        timeLabel.textAlignment = NSTextAlignmentCenter;
        timeLabel.textColor = [UIColor blackColor];
        
        
        
        
        //time_div.image = [UIImage imageNamed:@"divider"];
           [time_div setBackgroundColor:[UIColor lightGrayColor]];
        
        
        
        tipType.font = [UIFont fontWithName:@"Helvetica Neue LT Pro" size:13.0];
        
        
        tipType.text =[innerDict valueForKey:@"tip_type"] ;
        
        tipType.textAlignment = NSTextAlignmentCenter;
        tipType.textColor = [UIColor blackColor];
        
        
        
       // tipType_div.image = [UIImage imageNamed:@"divider"];
           [tipType_div setBackgroundColor:[UIColor lightGrayColor]];
        
        
        
        currency.font = [UIFont fontWithName:@"Helvetica Neue LT Pro" size:13.0];
        currency.text = [NSString stringWithFormat:@"%@",[innerDict valueForKey:@"price"] ];
        currency.textAlignment = NSTextAlignmentCenter;
        currency.textColor = [UIColor blackColor];
        
        //priceDiv.image = [UIImage imageNamed:@"divider"];
            [priceDiv setBackgroundColor:[UIColor lightGrayColor]];
        
        
        if(![[innerDict valueForKey:@"tip_status"] isEqualToString:@""])
        {
            logo.image = [UIImage imageNamed:[tipImageArray objectAtIndex:[[innerDict valueForKey:@"tip_status"] integerValue]-1]];
            //[cell addSubview:logo];
        }
        
        
        
    
        
        
        
           totalHeight+=50;
        
     
        
        yPos=yPos+size.height+50;
        
        // yPos=yPos+35;
      
    }
         [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollView.contentSize.height+totalHeight)];
    
   
    
    UILabel *comment_div = [[UILabel alloc] init];
    comment_div.frame = CGRectMake(0, yPos, self.view.frame.size.width-10, 0.5f);
    comment_div.backgroundColor = [UIColor grayColor];
    comment_div.alpha = 0.2f;
    [scrollView addSubview:comment_div];
      [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollView.contentSize.height+comment_div.frame.size.height)];
    
    commentField = [[UITextField alloc] init];
    commentField.frame = CGRectMake(0, yPos+1, self.view.frame.size.width-60, 37);
    commentField.backgroundColor = [UIColor whiteColor];
    commentField.delegate = self;
    commentField.autocorrectionType=UITextAutocorrectionTypeNo;
    
    commentField.font = [UIFont fontWithName:@"Helvetica Neue LT Pro" size:15.0];
    [scrollView addSubview:commentField];
          [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollView.contentSize.height+commentField.frame.size.height)];
    
    commentField.placeholder = @"Comment";
    [commentField setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    commentField.layer.sublayerTransform = CATransform3DMakeTranslation(20.f, 4.0f, 0.0f);
    
    // commentField.tag=(indexPath.row+1)*999;
    
    
    UIImageView *send_img = [[UIImageView alloc] init];
    send_img.frame = CGRectMake(commentField.frame.origin.x+commentField.frame.size.width+20, commentField.frame.origin.y+10, 18, 19);
    send_img.image = [UIImage imageNamed:@"icon1"];
    [scrollView addSubview:send_img];
      [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollView.contentSize.height+send_img.frame.size.height)];
    
    UIButton *send_btn = [[UIButton alloc] init];
    send_btn.frame = CGRectMake(commentField.frame.origin.x+commentField.frame.size.width+20, commentField.frame.origin.y+10, 18, 19);
    
    // send_btn.tag=(indexPath.row+1)*999+1;
    
    
    [send_btn addTarget:self action:@selector(postComment) forControlEvents:UIControlEventTouchUpInside];
    
    [scrollView addSubview:send_btn];
       [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollView.contentSize.height+send_btn.frame.size.height)];
    
    cell_div = [[UILabel alloc] init];
    cell_div.frame = CGRectMake(0, commentField.frame.origin.y+commentField.frame.size.height+5, self.view.frame.size.width-10, 2);
    cell_div.backgroundColor = [UIColor colorWithRed:(248.0f/255.0f) green:(247.0f/255.0f) blue:(243.0f/255.0f) alpha:1.0];
    [scrollView addSubview:cell_div];
     [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollView.contentSize.height+cell_div.frame.size.height)];
    
    [self performSelector:@selector(loadCommentData) withObject:nil];
}

-(void)loadCommentData
{
    
    [operationQueue addOperationWithBlock:^{
        
        NSString *urlString=[NSString stringWithFormat:@"%@/%@/%@/load_comments?tipid=%@&page=%d&limit=1&loggedinid=%@",GLOBALAPI,INDEX, DASHBOARD,tipId ,0,[[NSUserDefaults standardUserDefaults] valueForKey:UDUSERID]];
        
        DebugLog(@"url:%@",urlString);
        
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        //urlString =[urlString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        result1=[[NSDictionary alloc]init];
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
            if(urlData==nil)
            {
                
                [[[UIAlertView alloc]initWithTitle:@"Error!!!" message:@"error in internet Connection" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                
            }
            else{
                
                [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                
                result1=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                DebugLog(@"Load Comments result is:%@",result1);
                
                
                if([[result1 valueForKey:@"status"]isEqualToString:@"Error" ])
                {
                    
                    response2 = [result1 valueForKey:@"response"];
                    
                    
                    commentlbl=[[UILabel alloc]initWithFrame:CGRectMake(0, commentField.frame.origin.y+commentField.frame.size.height+5,self.view.frame.size.width, 100)];
                    
                    
                    
                    [commentlbl setTextAlignment:NSTextAlignmentCenter];
                    
                  //  [commentlbl setText:@"No comment Found"];
                    
                    commentlbl.textColor = [UIColor blackColor];
                    commentlbl.font = [UIFont fontWithName:HELVETICAPro size:18.0];
                    [scrollView addSubview:commentlbl];
                       [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollView.contentSize.height+commentlbl.frame.size.height)];
                    
                    UILabel *cell_div2 = [[UILabel alloc] init];
                    cell_div2.frame = CGRectMake(0, commentlbl.frame.origin.y+commentlbl.frame.size.height, self.view.frame.size.width-10, 2);
                    cell_div2.backgroundColor = [UIColor colorWithRed:(248.0f/255.0f) green:(247.0f/255.0f) blue:(243.0f/255.0f) alpha:1.0];
                    [scrollView addSubview:cell_div2];
                           [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollView.contentSize.height+cell_div2.frame.size.height)];
                    
                }
                else{
                    response2=[result1 valueForKey:@"response"];
                    
                    DebugLog(@"comment count%lu",(unsigned long)[response2 count]);
                    
                    if(commentlbl)
                    {
                               [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollView.contentSize.height-commentlbl.frame.size.height)];
                    [commentlbl removeFromSuperview];
                    }
                    if(commentView)
                    {   [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollView.contentSize.height-commentView.frame.size.height)];
                        [commentView removeFromSuperview];
                    }
                    
                  commentView=[[UIView alloc]initWithFrame:CGRectMake(0, cell_div.frame.origin.y+cell_div.frame.size.height,self.view.frame.size.width, 100)];
                    
                    UIImageView *otherProfile_img = [[UIImageView alloc] init];
                    otherProfile_img.frame = CGRectMake(10, 10, 40, 40);
                    //    otherProfile_img.layer.cornerRadius = otherProfile_img.frame.size.width/2;
                    
                    [otherProfile_img sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[response2 objectAtIndex:0]objectForKey:@"profileimage"] ]] placeholderImage:nil];
                    
                    CALayer *imageLayer = otherProfile_img.layer;
                    [imageLayer setCornerRadius:20.5];
                    [imageLayer setBorderWidth:0.1];
                    [imageLayer setMasksToBounds:YES];
                    
                    
                    [commentView addSubview:otherProfile_img];
                    
                    UILabel *otherName = [[UILabel alloc] init];
                    otherName.frame = CGRectMake(otherProfile_img.frame.origin.x+otherProfile_img.frame.size.width+10, 19, 150, 15);
                    otherName.text = [NSString stringWithFormat:@"%@ : ",[[response2 objectAtIndex:0]objectForKey:@"name"]];
                    otherName.textColor = [UIColor blackColor];
                    otherName.font = [UIFont boldSystemFontOfSize:15.0];
                    [commentView addSubview:otherName];
                    [otherName sizeToFit];
                    
                    
                                     //   UILabel *commentContent=[[UILabel alloc]initWithFrame:CGRectMake( otherProfile_img.frame.origin.x+otherProfile_img.frame.size.width+10, 25, 250,  45)];
                    
                    UIButton *profile_btn = [[UIButton alloc] init];
                    profile_btn.frame = CGRectMake(0, 0, otherName.frame.origin.x+otherName.frame.size.width, otherProfile_img.frame.origin.y+otherProfile_img.frame.size.height+10);
                    
                    
                    profile_btn.tag = [[[response2 objectAtIndex:0] objectForKey:@"userid"] intValue];
                    followedBy = [[[response2 objectAtIndex:0] objectForKey:@"followed_by"] intValue];
                    
                    [profile_btn addTarget:self action:@selector(viewUser1:) forControlEvents:UIControlEventTouchUpInside];
                    [commentView addSubview:profile_btn];
                    
                    
                    UILabel *commentContent=[[UILabel alloc]initWithFrame:CGRectMake( otherName.frame.origin.x+otherName.frame.size.width+5, 6, [UIScreen mainScreen].bounds.size.width-(otherName.frame.origin.x+otherName.frame.size.width+25),  45)];
                    
                    [commentContent setText:[NSString stringWithFormat:@"%@",[[response2 objectAtIndex:0]objectForKey:@"comment"]]];
                    commentContent.textColor = [UIColor blackColor];
                    //[commentContent setBackgroundColor:[UIColor greenColor]];
                    
                    commentContent.font = [UIFont fontWithName:@"Helvetica Neue" size:12.0];
                    commentContent.numberOfLines = 3;
                    commentContent.lineBreakMode = YES;
                    
                    [commentView addSubview:commentContent];
                    
                    
                    
                    UIButton *viewComments=[[UIButton alloc]initWithFrame:CGRectMake(0, commentContent.frame.origin.y+commentContent.frame.size.height+15, [UIScreen mainScreen].bounds.size.width, 20)];
                    
                    [viewComments setTitle:@"View All Comments" forState:UIControlStateNormal];
                    
                    [viewComments addTarget:self action:@selector(loadCommentView:) forControlEvents:UIControlEventTouchUpInside];
                    
                    [viewComments setBackgroundColor:[UIColor clearColor]];
                    
                    [viewComments.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:12]];
                    
                    [viewComments setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                    

                    
                    [commentView addSubview:viewComments];
                    
                    
                    [scrollView addSubview:commentView];
                              [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollView.contentSize.height+commentView.frame.size.height)];
                    
                    UILabel *cell_div2 = [[UILabel alloc] init];
                    cell_div2.frame = CGRectMake(0, commentView.frame.origin.y+commentView.frame.size.height, self.view.frame.size.width-10, 2);
                    cell_div2.backgroundColor = [UIColor colorWithRed:(248.0f/255.0f) green:(247.0f/255.0f) blue:(243.0f/255.0f) alpha:1.0];
                    [scrollView addSubview:cell_div2];
                       [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollView.contentSize.height+cell_div2.frame.size.height)];
                    
                }

                    
                    [self stopLoading];
                    
                    
                         [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, scrollView.contentSize.height+50.0/568.0*self.view.frame.size.height)];
                    
                        DebugLog(@"scroll frame height %f",scrollView.contentSize.height);
                    [scrollView setHidden:NO];
            
                 }];
                
            }
            
       
        
    }];
}




-(void)postComment
{
        NSCharacterSet *charSet=[NSCharacterSet whitespaceCharacterSet];
    
    if([[commentField.text stringByTrimmingCharactersInSet:charSet] length]>0)
    {
        
    
  
    
    [self startLoading:commentView];
    

    
    
  
    
    
   [innerOperationQueue addOperationWithBlock:^{
        NSString *urlString2=[NSString stringWithFormat:@"%@/%@/%@/insert_comment?comment=%@&userid=%@&tipid=%@",GLOBALAPI,INDEX, DASHBOARD,commentField.text ,[[NSUserDefaults standardUserDefaults]valueForKey:UDUSERID ],tipId];
        
        
        
        
        //urlString2 = [urlString2 stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        urlString2 =[urlString2 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        DebugLog(@"url:%@",urlString2);
        
        result3=[[NSDictionary alloc]init];
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString2]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            [commentField setText:@""];
            
            
            if(urlData==nil)
            {
                
                [self stopLoading];
                
                
                [[[UIAlertView alloc]initWithTitle:@"Error!!!" message:@"error in internet Connection" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                
            }
            else{
                
                [self stopLoading];
                
                result3=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                DebugLog(@"result is:%@",result3);
                
                
                if([[result3 valueForKey:@"status"]isEqualToString:@"Error" ])
                {
                    
                    [[[UIAlertView alloc]initWithTitle:@"Error" message:[result3 valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                    
                    
                    
                }
                else{
                    
                  
                    
             [self performSelector:@selector(loadCommentData) withObject:nil];
                    
                }
                
            }
            
        }];
        
    }];
    
    }
    else{
        
        commentField.placeholder=@"comment cannot be blank";
        [commentField setValue:[UIColor redColor] forKeyPath:@"_placeholderLabel.textColor"];
        

    }
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
        return 70;
  
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   
        return [commentArr count];
        
 
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    // NSString *CellIdentifier = [NSString stringWithFormat:@"%ld,%ld",(long)indexPath.section,(long)indexPath.row];
    
    static NSString *CellIdentifier = @"cellIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    //    if (cell == nil)
    //    {
    DebugLog(@"CELL ALLOC");
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    // }
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor whiteColor];
    
    
       commentView=[[UIView alloc]initWithFrame:CGRectMake(0, 0,tableView.frame.size.width, tableView.frame.size.height-5)];
        
        UIImageView *otherProfile_img = [[UIImageView alloc] init];
        otherProfile_img.frame = CGRectMake(10, 10, 40, 40);
        //    otherProfile_img.layer.cornerRadius = otherProfile_img.frame.size.width/2;
        
        [otherProfile_img sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[commentArr objectAtIndex:indexPath.row]objectForKey:@"profileimage"] ]] placeholderImage:nil];
        
        CALayer *imageLayer = otherProfile_img.layer;
        [imageLayer setCornerRadius:20.5];
        [imageLayer setBorderWidth:0.1];
        [imageLayer setMasksToBounds:YES];
        
        
        [commentView addSubview:otherProfile_img];
        
        UILabel *otherName = [[UILabel alloc] init];
        otherName.frame = CGRectMake(otherProfile_img.frame.origin.x+otherProfile_img.frame.size.width+10, 19, 150, 15);
        otherName.text = [NSString stringWithFormat:@"%@ : ",[[commentArr objectAtIndex:indexPath.row]objectForKey:@"name"]];
        otherName.textColor = [UIColor blackColor];
        otherName.font = [UIFont boldSystemFontOfSize:13.0];
        [commentView addSubview:otherName];
        [otherName sizeToFit];
        
        
        UILabel *commentContent=[[UILabel alloc]initWithFrame:CGRectMake( otherName.frame.origin.x+otherName.frame.size.width+5, 5, [UIScreen mainScreen].bounds.size.width-(otherName.frame.origin.x+otherName.frame.size.width+60),  45)];
        
        [commentContent setText:[NSString stringWithFormat:@"%@",[[commentArr objectAtIndex:indexPath.row]objectForKey:@"comment"]]];
        commentContent.textColor = [UIColor blackColor];
        
        commentContent.numberOfLines = 3;
        commentContent.lineBreakMode = YES;
        commentContent.font = [UIFont fontWithName:@"Helvetica Neue" size:10.0];
        
        [commentView addSubview:commentContent];
        
    
        [cell addSubview:commentView];
        
    return cell;
}

-(void)loadCommentView:(UIButton *)sender
{
    
    commentArr=[[NSMutableArray alloc]init];
    commentPage=0;
    commentCount=0;
    totalCommentCount=0;
    
    
    [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%d",(int)sender.tag] forKey:@"tipTableId"];
    
    
    
    
    blackView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
    [blackView setBackgroundColor:[UIColor blackColor]];
    
    [blackView setAlpha:0.9];
    
    // [blackView setHidden:YES];
    
    [self.view addSubview:blackView];
    
    
    
    
    popupView=[[UIView alloc]initWithFrame:CGRectMake(20.0/320.0*self.view.frame.size.width,40.0/568.0*self.view.frame.size.height, 280.0/320.0*self.view.frame.size.width, 500.0/568.0*self.view.frame.size.height)];
    
    // [popupView setHidden:YES];
    [popupView setBackgroundColor:[UIColor whiteColor]];
    [popupView setAlpha:1];
    [popupView.layer setZPosition:99];
    
    [self.view addSubview:popupView];
    
    UILabel *Comments=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, popupView.frame.size.width,50.0/568.0*self.view.frame.size.height)];
    
    [Comments setBackgroundColor:[UIColor colorWithRed:222/255.0 green:81/255.0 blue:5/255.0 alpha:1]];
    
    
    [Comments setText:@"COMMENTS"];
    [Comments setTextAlignment:NSTextAlignmentCenter];
    
    [Comments setFont:[UIFont boldSystemFontOfSize:18]];
    [popupView addSubview:Comments];
    
    
    if(CommentTable)
        [CommentTable removeFromSuperview];
    
    
    CommentTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 50.0/568.0*self.view.frame.size.height, popupView.frame.size.width, 400.0/568.0*self.view.frame.size.height) style:UITableViewStylePlain];
    
    [CommentTable setTag:22];
    
    
    [CommentTable setDelegate:self];
    [CommentTable setDataSource:self];
    [CommentTable setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    
    
    [popupView addSubview:CommentTable];
    
    
    UIButton *backBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 450.0/568.0*self.view.frame.size.height, popupView.frame.size.width, 50.0/568.0*self.view.frame.size.height)];
    
    [backBtn setBackgroundColor:[UIColor colorWithRed:222/255.0 green:81/255.0 blue:5/255.0 alpha:1]];
    [backBtn.titleLabel setFont:[UIFont boldSystemFontOfSize:18]];
    
    [backBtn setTitle:@"CLOSE" forState:UIControlStateNormal];
    
    [backBtn addTarget:self action:@selector(closeComment) forControlEvents:UIControlEventTouchUpInside];
    
    
    [popupView addSubview:backBtn];
    
    [self loadCommentTable: commentPage];
    
}


-(void)loadCommentTable:(int)count
{
    
   
    [CommentTable setHidden:YES];
    [self startLoading:popupView];
    
    
    
    
    [innerOperationQueue addOperationWithBlock:^{
        NSString *urlString2=[NSString stringWithFormat:@"%@/%@/%@/load_comments?tipid=%@&page=%d&limit=10&loggedinid=%@",GLOBALAPI,INDEX, DASHBOARD,tipId ,count,[[NSUserDefaults standardUserDefaults] valueForKey:UDUSERID]];
        
        
        DebugLog(@"url:%@",urlString2);
        
        urlString2 = [urlString2 stringByReplacingOccurrencesOfString:@" " withString:@""];
        //  urlString =[urlString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        result3=[[NSDictionary alloc]init];
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString2]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            [CommentTable setHidden:NO];
            [self stopLoading];
            
            if(urlData==nil)
            {
                
                
                
                [[[UIAlertView alloc]initWithTitle:@"Error!!!" message:@"error in internet Connection" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                
                
                
                
            }
            else{
                
                result3=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                DebugLog(@"result is:%@",result3);
                
                
                if([[result3 valueForKey:@"status"]isEqualToString:@"Error" ])
                {
                    
                    response2=[result3 valueForKey:@"response"];
                    
                    
                    
                }
                else{
                    response2=[result3 valueForKey:@"response"];
                    
                    DebugLog(@"comment count%lu",(unsigned long)[response2 count]);
                    
                    commentCount+=[response2 count];
                    totalCommentCount = [[result3 valueForKey:@"total"]intValue];
                    
                    
                    
                    for(NSDictionary *commentDict in response2 )
                    {
                        
                        [commentArr addObject:commentDict];
                        
                        
                    }
                    
                    [self stopLoading];
                    
                    [CommentTable setUserInteractionEnabled:YES];
                    [CommentTable reloadData];
                    
                    
                }
                
            }
            
       
            
            
        }];
        
    }];
    
    
    
}

-(void)viewUser1:(UIButton *)sender{
    
    // DebugLog(@"Response:%d, %@",(int)sender.tag, cell.followedBy.tag);
    
  //  UITableViewCell *cCell= (UITableViewCell *)sender.superview;
    
  //  NSIndexPath *indexPath = [_contentTable indexPathForCell:cCell];
    
    
    TROwnProfileViewController *pushView = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"profile"];
    pushView.userId = [NSString stringWithFormat:@"%ld", sender.tag];
    
    //[[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%ld", sender.tag] forKey:@"globalID"];
    
    if(sender.tag != [[[NSUserDefaults standardUserDefaults]valueForKey:UDUSERID] intValue])
    {
        pushView.isfromOther = TRUE;
    }
    pushView.isfromOther = TRUE;
    
        pushView.FollowCheck = followedBy ;
    
        if(followedBy == 1)
        {
    
            pushView.follow_text=@"Unfollow";
    
        }
        else    if(followedBy==0)
        {
    
            pushView.follow_text=@"Follow";
    
    
        }
        else{
            pushView.follow_text=@"Withdraw";
    
        }
    
    
    
    
    [[self navigationController ]pushViewController:pushView animated:YES];
    
}


-(void)closeComment
{
    [popupView setHidden:YES];
    [blackView setHidden:YES];
    [popupView removeFromSuperview];
    [blackView removeFromSuperview];
    [CommentTable removeFromSuperview];
    
    
}
- (IBAction)copyBetClicked:(UIButton *)sender {
    

    
    
    [innerOperationQueue addOperationWithBlock:^{
        NSString *urlString2=[NSString stringWithFormat:@"%@/%@/%@/insert_bet?userid=%@&tipid=%@",GLOBALAPI,INDEX, DASHBOARD ,[[NSUserDefaults standardUserDefaults]valueForKey:UDUSERID ],tipId];
        
        urlString2 =[urlString2 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        DebugLog(@"url:%@",urlString2);
        
        result3=[[NSDictionary alloc]init];
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString2]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            if(urlData==nil)
            {
                
                [[[UIAlertView alloc]initWithTitle:@"Error!!!" message:@"error in internet Connection" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                
            }
            else{
                
                result3=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                DebugLog(@"result is:%@",result3);
                
                
                if([[result3 valueForKey:@"status"]isEqualToString:@"Error" ])
                {
                    
                    [[[UIAlertView alloc]initWithTitle:@"Error" message:[result3 valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                    
                }
                else{
                    
                    [copyBet setTitle:@"COPIED" forState:UIControlStateNormal];
                    
                   
                    
                }
                
            }
            
        }];
        
    }];
    

    
}

- (IBAction)likeClick:(UIButton *)sender {
   
  [innerOperationQueue addOperationWithBlock:^{
        NSString *urlString2=[NSString stringWithFormat:@"%@/%@/%@/insert_likes?userid=%@&tipid=%@",GLOBALAPI,INDEX, DASHBOARD ,[[NSUserDefaults standardUserDefaults]valueForKey:UDUSERID ],tipId];
        
        urlString2 =[urlString2 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        DebugLog(@"url:%@",urlString2);
        
        result3=[[NSDictionary alloc]init];
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString2]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            if(urlData==nil)
            {
                
                
                
                [[[UIAlertView alloc]initWithTitle:@"Error!!!" message:@"error in internet Connection" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                
                
                
                
            }
            else{
                
                result3=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                DebugLog(@"result is:%@",result3);
                
                
                if([[result3 valueForKey:@"status"]isEqualToString:@"Error" ])
                {
                    
                    [[[UIAlertView alloc]initWithTitle:@"Error" message:[result3 valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                    
                    
                    
                }
                else{
                   
                 
                if([[result3 valueForKey:@"return_status"]boolValue])
                {
                        _heartImage.image = [UIImage imageNamed:@"heart"];
                    
                    liked++;
                    
                     _like_status.text = [NSString stringWithFormat:@" %d people liked",liked ]  ;
                }  else
                    {
                    _heartImage.image = [UIImage imageNamed:@"heart-1"];
                        liked--;
                        
                               _like_status.text = [NSString stringWithFormat:@" %d people liked",liked]  ;
                    
                    }
                 
                    
                }
                
            }
            
        }];
        
    }];
    
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)viewUser:(id)sender {
    
    TROwnProfileViewController *pushView = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"profile"];
    pushView.userId=[[response objectAtIndex:0] objectForKey:@"user_id"];
    pushView.isfromOther=TRUE;
    
   
    //[[NSUserDefaults standardUserDefaults]setObject:[[response objectAtIndex:0] objectForKey:@"user_id"] forKey:@"globalID"];
    
    [[self navigationController ]pushViewController:pushView animated:YES];
    
}


- (void)tipDelete:(UIButton *)cross
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"You would like to delete?"
                                                    message:nil
                                                   delegate:self
                                          cancelButtonTitle:@"No"  otherButtonTitles:@"Yes", nil];
    alert.tag=99;
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    DebugLog(@"ALERT . TAG====%ld",(long)alertView.tag);
    if (alertView.tag == 99) {
        
        if(buttonIndex == 0)
        {
            // Do something
        }
        else
        {
            [UIView animateWithDuration:0.0f animations:^{
                
                [self startLoading:self.view];
                
            }completion:^(BOOL finished){
                
                
                // http://www.esolz.co.in/lab3/tipright/index.php/editprofile_ios/tip_delete?tipid=525&loggedin_id=158
                
                
                NSString *tipid=[[response objectAtIndex:0]objectForKey:@"count_tip_id"];
                
                DebugLog(@"Tip Id:%@",tipid);
                
                
                [innerOperationQueue addOperationWithBlock:^{
                    NSString *urlString2=[NSString stringWithFormat:@"%@/%@/editprofile_ios/tip_delete?tipid=%@&loggedin_id=%@",GLOBALAPI,INDEX,tipid, [[NSUserDefaults standardUserDefaults]valueForKey:UDUSERID ]];
                    
                    urlString2 =[urlString2 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                    
                    DebugLog(@"url:%@",urlString2);
                    
                    result3=[[NSDictionary alloc]init];
                    
                    NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString2]];
                    
                    [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                        if(urlData==nil)
                        {
                            
                            [[[UIAlertView alloc]initWithTitle:@"Error!!!" message:@"error in internet Connection" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                            
                        }
                        else{
                            
                            result3=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                            DebugLog(@"result is:%@",result3);
                            
                            
                            if([[result3 valueForKey:@"status"]isEqualToString:@"Error" ])
                            {
                                
                                [[[UIAlertView alloc]initWithTitle:@"Error" message:[result3 valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                                
                            }
                            else{
                                
                                [self stopLoading];
                                
                                CATransition *Transition=[CATransition animation];
                                [Transition setDuration:0.4f];
                                //[Transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
                                [Transition setType:kCATransitionPush];
                                [Transition setSubtype:kCATransitionFromLeft];
                                [[[[self navigationController] view] layer] addAnimation:Transition forKey:@"transition"];
                                [self.navigationController popViewControllerAnimated:NO];
                                
                                
//                                TROwnProfileViewController *ownProfile = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"profile"];
//                                ownProfile.userId = [[NSUserDefaults standardUserDefaults]valueForKey:UDUSERID];
//                                [[self navigationController ]pushViewController:ownProfile animated:YES];
                                
                            }
                            
                        }
                        
                    }];
                    
                }];
                
                
            }];
            
        }
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
