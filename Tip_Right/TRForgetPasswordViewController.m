

//
//  TRForgetPasswordViewController.m
//  Tip Right
//
//  Created by santanu on 09/07/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import "TRForgetPasswordViewController.h"

@interface TRForgetPasswordViewController ()<UITextFieldDelegate>

{
    
    NSOperationQueue *opQueue;
    NSDictionary *json, *result, *response;
    NSCharacterSet *whitespace;
    
    
}
@property (weak, nonatomic) IBOutlet UITextField *userEmail;
@property (weak, nonatomic) IBOutlet UITextField *verificationCode;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UIButton *sendBtn;
@property (weak, nonatomic) IBOutlet UIButton *confirmCode;
@property (weak, nonatomic) IBOutlet UILabel *verificationLbl;
@property (weak, nonatomic) IBOutlet UILabel *passLbl;
@property (weak, nonatomic) IBOutlet UILabel *confirmLbl;
@property (weak, nonatomic) IBOutlet UITextField *confirmPass;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLbl;


-(BOOL)isvalidEmail:(NSString *)checkString;

@end


@implementation TRForgetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _confirmCode.alpha=0.5;
    
    [_verificationCode setAlpha:0.5];
    [_password setAlpha:0.5];
    
    [self Header:self.view];
    
    opQueue=[[NSOperationQueue alloc]init];
    whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)sendRequest:(id)sender {
    
    
    [self startLoading:self.view];
    
    
    
    if([self isvalidEmail:_userEmail.text])
    {
        [opQueue addOperationWithBlock:^{
            
            //http://www.esolz.co.in/lab3/tipright/index.php/dashboard_ios/check_email?email=santanu.mondal@esolzmail.com
            
            NSString *urlString=[NSString stringWithFormat:@"%@/%@/%@/check_email?email=%@",GLOBALAPI,INDEX,DASHBOARD,_userEmail.text];
            
            
            DebugLog(@"firing url:%@",urlString);
            
            urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            result=[[NSDictionary alloc]init];
            
            NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
            
            if(urlData==nil)
            {
                
                [[[UIAlertView alloc]initWithTitle:@"Error!!!" message:@"error in internet Connection" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                
            }
            else{
                
                [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                    
                    result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                    DebugLog(@" result is:%@",result);
                    
                    response = [result objectForKey:@"response"];
                    
                    
                    if([[response valueForKey:@"status"]isEqualToString:@"Success" ])
                    {
                        [self stopLoading];
                        
                        [[[UIAlertView alloc]initWithTitle:@"Check your Email!" message:[response valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                        
                        [_emailLbl setHidden:NO];
                        
                        
                        [_userEmail setUserInteractionEnabled:NO];
                        [_userEmail setTextColor:[UIColor darkGrayColor]];
                        
                        
                        [_userEmail setFrame:CGRectMake(141.0/320.0*self.view.frame.size.width, 70.0/568.0*self.view.frame.size.height, 164.0/320.0*self.view.frame.size.width, 30/568.0*self.view.frame.size.height)];
                        
                        [_sendBtn setFrame:CGRectMake(33.0/320.0*self.view.frame.size.width, 276.0/568.0*self.view.frame.size.height, 100.0/320.0*self.view.frame.size.width, 40/568.0*self.view.frame.size.height)];
                        
                        
                        [_verificationLbl setHidden:NO];
                        
                        [_verificationCode setHidden:NO];
                        [_verificationCode setUserInteractionEnabled:YES];
                        [_verificationCode setAlpha:1];
                        
                        [_passLbl setHidden:NO];
                        [_password setHidden:NO];
                        
                        [_password setUserInteractionEnabled:YES];
                        [_password setAlpha:1];
                        
                        [_confirmLbl setHidden:NO];
                        [_confirmPass setHidden:NO];
                        
                        [_confirmPass setUserInteractionEnabled:YES];
                        [_confirmPass setAlpha:1];
                        
                        
                        [_confirmCode setAlpha:1];
                        [_confirmCode setUserInteractionEnabled:YES];
                        [_confirmCode setHidden:NO];
                        
                        [_sendBtn setTitle:@"Resend" forState:UIControlStateNormal];
                        [_verificationCode becomeFirstResponder];
                        
                        
                        [_infoLabel setText:@"Please Put the verification code you got from email and confirm with you new desired Password."];
                        
                        
                        
                        
                    }
                    else{
                        [self stopLoading];
                        
                        [[[UIAlertView alloc]initWithTitle:@"Error!!!" message:[response valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                        
                    }
                }];
            }
        }];
        
        
    }
    else{
        
        [self stopLoading];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please Enter a Valid Email!"
                                                        message:Nil
                                                       delegate:self
                                              cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
        
        
    }
    
    
}
- (IBAction)confirmPassword:(id)sender {
    
    
    [self startLoading:self.view];
    
    
    
    if([[_verificationCode.text stringByTrimmingCharactersInSet:whitespace] length]>0 && [[_password.text stringByTrimmingCharactersInSet:whitespace] length]>0  && [[_confirmPass.text stringByTrimmingCharactersInSet:whitespace] length]>0)
    {
        if ([[_password.text stringByTrimmingCharactersInSet:whitespace] length] < 6 ||[[_password.text stringByTrimmingCharactersInSet:whitespace] length] > 20 || [[_confirmPass.text stringByTrimmingCharactersInSet:whitespace] length] < 6 ||[[_confirmPass.text stringByTrimmingCharactersInSet:whitespace] length] > 20) {
            
            [self stopLoading];
            
            
           UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Enter password within a range of 6 to 20 characters"
                                               message:Nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
            
        }
        else
        {
        if([_password.text isEqualToString:_confirmPass.text])
        {
            
            [opQueue addOperationWithBlock:^{
                
                //http://www.esolz.co.in/lab3/tipright/index.php/dashboard_ios/change_password?verification_code=1435302781MTgw1234&new_password=abc
                
                NSString *urlString=[NSString stringWithFormat:@"%@/%@/%@/change_password?verification_code=%@&new_password=%@",GLOBALAPI,INDEX,DASHBOARD,_verificationCode.text,_password.text];
                
                
                DebugLog(@"firing url:%@",urlString);
                
                urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                
                result=[[NSDictionary alloc]init];
                
                NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
                
                if(urlData==nil)
                {
                    
                    [[[UIAlertView alloc]initWithTitle:@"Error!!!" message:@"error in internet Connection" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                    
                }
                else{
                    
                    [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                        
                        result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                        DebugLog(@" result is:%@",result);
                        
                        response = [result objectForKey:@"response"];
                        
                        
                        if([[response valueForKey:@"status"]isEqualToString:@"Success" ])
                        {
                            [self stopLoading];
                            
                            [[[UIAlertView alloc]initWithTitle:@"Password Changed!!!" message:[response valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                            
                            UIViewController *Login = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"Login"];
                            [[self navigationController ]pushViewController:Login animated:YES];
                            
                            
                        }
                        else{
                            [self stopLoading];
                            
                            [[[UIAlertView alloc]initWithTitle:@"Sorry!!!" message:[response valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                            [_verificationCode becomeFirstResponder];
                        }
                    }];
                }
            }];
            
        }
        else{
            [self stopLoading];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Password and confirm password Must be same."
                                                            message:Nil
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
            
        }
    }
        
    }
    else{
        
        [self stopLoading];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please Fill all the Fields"
                                                        message:Nil
                                                       delegate:self
                                              cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
        
        
    }
    
    
    
    
    
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}
-(BOOL)isvalidEmail:(NSString *)checkString
{
    checkString = [checkString lowercaseString];
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:checkString];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
