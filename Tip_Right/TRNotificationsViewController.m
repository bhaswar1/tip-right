//
//  TRNotificationsViewController.m
//  Tip Right
//
//  Created by santanu on 04/06/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import "TRNotificationsViewController.h"
#import "TRGlobalHeader.h"
#import "TRNotificationCell.h"
#import <QuartzCore/QuartzCore.h>
#import "UIImageView+WebCache.h"
#import "TRNotificationDetailsViewController.h"
#import "TROwnProfileViewController.h"

@interface TRNotificationsViewController ()
{
    NSOperationQueue *operationQueue,*innerOperationQueue;
    NSDictionary *result,*result2,*result3;
    NSMutableArray *response,*response2,*response3;
    NSArray *tipImageArray;
    
    BOOL isReloadComplete;
    
    __weak IBOutlet UITableView *notificationTable;
    UIView *popupView,*blackView;
    
    __weak IBOutlet UILabel *noNotiLabel;
    int page,dataCount,totalCount;
    NSMutableArray *notificationArr;
}
@property (strong, nonatomic) IBOutlet UIView *screenView;

@end

@implementation TRNotificationsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
       // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(countInitialisation) name:@"Received_Notification" object:nil];
    
}
-(void)viewDidAppear:(BOOL)animated
{
    page=0;
    
    dataCount=0;
    
    notificationArr=[[NSMutableArray alloc]init];
    
    
    [self Header:self.view];
    [self Footer:self.view];
    response = [[NSMutableArray alloc] init];
    [notificationTable setUserInteractionEnabled:NO];
    [self startLoading:notificationTable];
    
    [self loadData];

    
}

-(void)countInitialisation
{
    page = 0;
    dataCount = 0;
    totalCount = 0;
    notificationArr = [[NSMutableArray alloc] init];
    [self loadData];
    
}

-(void)loadData
{
    //http://www.esolz.co.in/lab3/tipright/index.php/notification_ios/index?user_id=141&page=0&limit=1
    
    NSString *urlString=[NSString stringWithFormat:@"%@/%@/%@/index?user_id=%@&page=%d&limit=10",GLOBALAPI,INDEX,NOTIFICATION,[[NSUserDefaults standardUserDefaults]valueForKey:UDUSERID ],page];
    
    DebugLog(@"url:%@",urlString);
    
    urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
    //urlString =[urlString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    result=[[NSDictionary alloc]init];
    
    NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
    
    if(urlData==nil)
    {
        
        [[[UIAlertView alloc]initWithTitle:@"Error!!!" message:@"error in internet Connection" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
        
    }
    else{
        
        [self stopLoading];
        
        result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
        DebugLog(@"Notification result is:%@",result);
        
        if([[result valueForKey:@"status"]isEqualToString:@"Error" ])
        {
            response=[result valueForKeyPath:@"response"];
            
            noNotiLabel.text=[response valueForKey:@"message"];
            
            [noNotiLabel setHidden:NO];
            
            
//            
//            [[[UIAlertView alloc]initWithTitle:@"Sorry!!!" message:[response valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
            
        }
        else{
            
            
            [notificationTable setHidden:NO];
            
            response = [result valueForKeyPath:@"response"];
            DebugLog(@"RESPONSE VALUE:%@",response);
            DebugLog(@"%lu",(unsigned long)[response count]);
            
            dataCount+=[response count];
            
            totalCount=[[result valueForKey:@"total_notification"]intValue];
            
            for(NSDictionary *rowDict in response)
            {
                [notificationArr addObject:rowDict];
            }
            
            [notificationTable setUserInteractionEnabled:YES];
            [notificationTable reloadData];
            
        }
        
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    DebugLog(@"RESPONSE:%ld",[notificationArr count]);
    return [notificationArr count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier=@"cellIdentifier";
    TRNotificationCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        DebugLog(@"CELL ALLOC");
        cell = [[TRNotificationCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
    }
    
    cell.selectionStyle=UITableViewCellStyleDefault;
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    
    UIImage *img=[UIImage imageNamed:@"noimages"];
    
    [cell.userImage sd_setImageWithURL:[NSURL URLWithString:[[notificationArr objectAtIndex:indexPath.row] objectForKey:@"profileimage"]] placeholderImage:img];
    
   
    cell.userImage.layer.cornerRadius = cell.userImage.frame.size.width/2;
    cell.userImage.clipsToBounds = YES;
    if(IsIphone6plus)
        cell.userName.font=[cell.userName.font fontWithSize:17];
    else if(IsIphone6)
        cell.userName.font=[cell.userName.font fontWithSize:15];
    else
        cell.userName.font=[cell.userName.font fontWithSize:13];
    
    
    cell.userName.text = [[notificationArr objectAtIndex:indexPath.row] valueForKey:@"name"];
    
    if(IsIphone6plus)
        [cell.contentLabel setFont:[UIFont fontWithName:HELVETICAProLight size:14]];
    else if(IsIphone6)
        [cell.contentLabel setFont:[UIFont fontWithName:HELVETICAProLight size:12]];
    else
        [cell.contentLabel setFont:[UIFont fontWithName:HELVETICAProLight size:10]];
    
    if ([[[notificationArr objectAtIndex:indexPath.row] objectForKey:@"post"] isEqualToString:@"like"]) {
        
        cell.contentLabel.text=@"liked your post";
        
        cell.actionImage.image=[UIImage imageNamed:@"like"];
    }
    else    if ([[[notificationArr objectAtIndex:indexPath.row] objectForKey:@"post"] isEqualToString:@"comment"])
    {
        cell.contentLabel.text=@"commented on your post";
        
        cell.actionImage.image=[UIImage imageNamed:@"comment"];
    }
    
    
        else if ([[[notificationArr objectAtIndex:indexPath.row] objectForKey:@"post"] isEqualToString:@"follow_request"])
        {
        
            cell.contentLabel.text=@"has sent you follow request";
            
          
        }
    else if ([[[notificationArr objectAtIndex:indexPath.row] objectForKey:@"post"] isEqualToString:@"request_accepted"])
        {
            
            cell.contentLabel.text=@"has accepted your follow request";
            
        
        }
    
    else if ([[[notificationArr objectAtIndex:indexPath.row] objectForKey:@"post"] isEqualToString:@"follow"])
    {
        
        cell.contentLabel.text=@"has started following you";
        
        
    }
    
    
    if(IsIphone6plus)
        [cell.dateLabel setFont:[UIFont fontWithName:HELVETICAProLight size:12]];
    else if(IsIphone6)
        [cell.dateLabel setFont:[UIFont fontWithName:HELVETICAProLight size:11]];
    else
        [cell.dateLabel setFont:[UIFont fontWithName:HELVETICAProLight size:10]];
    //[cell.dateLabel setText:@"05/20/2015"];
    
    NSString *date_time = [[notificationArr objectAtIndex:indexPath.row] objectForKey:@"date"];
    NSString *year = [date_time substringWithRange:NSMakeRange(0, 4)];
    DebugLog(@"YEAR:%@",year);
    NSString *month = [date_time substringWithRange:NSMakeRange(5, 2)];
    DebugLog(@"YEAR:%@",month);
    NSString *date = [date_time substringWithRange:NSMakeRange(8, 2)];
    DebugLog(@"YEAR:%@",date);
    
    [cell.dateLabel setText:[NSString stringWithFormat:@"%@/%@/%@",month,date,year]];
    
      CGRect line=cell.separatorLine.frame;
    
      line.size.height=0.05;
     cell.separatorLine.frame=line;
        UILabel *separator = [[UILabel alloc] init];
        separator.frame = CGRectMake(0, 99, cell.frame.size.width, 1.0f);
        separator.backgroundColor = [UIColor colorWithRed:(248.0f/255.0f) green:(247.0f/255.0f) blue:(243.0f/255.0f) alpha:1.0f];
        [cell.contentView addSubview:separator];
    
    
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    

    
    if ([[[notificationArr objectAtIndex:indexPath.row] objectForKey:@"post"] isEqualToString:@"follow_request"] || [[[notificationArr objectAtIndex:indexPath.row] objectForKey:@"post"] isEqualToString:@"request_accepted"]|| [[[notificationArr objectAtIndex:indexPath.row] objectForKey:@"post"] isEqualToString:@"follow"]) {
        
        
        TROwnProfileViewController *pushView = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"profile"];
        pushView.userId=[[notificationArr objectAtIndex:indexPath.row] objectForKey:@"userid"];
       
        
        if([[[notificationArr objectAtIndex:indexPath.row] objectForKey:@"post"] isEqualToString:@"follow_request"])
        {
        pushView.isFromRequest=YES;
        }
        else{
           pushView.isfromOther=YES;
     
            
            pushView.FollowCheck=[[[notificationArr objectAtIndex:indexPath.row] objectForKey:@"follow_status"]intValue];
            
            
            if([[[notificationArr objectAtIndex:indexPath.row] objectForKey:@"follow_status"]intValue]==1)
            {
                
                pushView.follow_text=@"Unfollow";
                
            }
            else    if([[[notificationArr objectAtIndex:indexPath.row] objectForKey:@"follow_status"]intValue]==0)
            {
                
                pushView.follow_text=@"Follow";
                
                
            }
            else{
                pushView.follow_text=@"Withdraw";
                
            }
            
       
        
        }
        
        [[self navigationController ]pushViewController:pushView animated:YES];
    }
    
    else{
        TRNotificationDetailsViewController *pushView = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"notiDetails"];
        
        DebugLog(@"Tip Id:%@",[notificationArr objectAtIndex:indexPath.row]);
        
    if ([[[notificationArr objectAtIndex:indexPath.row] objectForKey:@"post"] isEqualToString:@"like"]) {
        pushView.like = TRUE;
        
    }
    
    
    pushView.tipId=[NSString stringWithFormat:@"%@",[[notificationArr objectAtIndex:indexPath.row] objectForKey:@"tipid"]];
        
    [[self navigationController ]pushViewController:pushView animated:YES];
    }
}

#pragma UIScrollViewDelegate

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGPoint offset = scrollView.contentOffset;
    CGRect bounds = scrollView.bounds;
    CGSize size = scrollView.contentSize;
    UIEdgeInsets inset = scrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    
    float reload_distance = -60.0f;
    if(y > h + reload_distance)
    {
        
        DebugLog(@"total count is%d and current count is %d",totalCount,dataCount);
        
        if (dataCount<totalCount)
        {
            [notificationTable setUserInteractionEnabled:NO];
            [self startLoading:notificationTable];
            
            page++;
            
            [self performSelector:@selector(loadData) withObject:nil];
        }
    }
    
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
