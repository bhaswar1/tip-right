//
//  TRTipViewController.m
//  Tip Right
//
//  Created by santanu on 08/06/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import "TRTipViewController.h"
#import "TRGlobalMethods.h"
#import "TRTipTableCell.h"
#import "AFHTTPClient.h"
#import "AFHTTPRequestOperation.h"
@interface TRTipViewController ()<UITextViewDelegate>
{
    int rowCount;
    UIPickerView *timePicker,*typePicker;
    NSMutableArray *timeArr;
    NSArray *typeArr,*imageOpArr;
    TRTipTableCell *Ccell;
    UIImagePickerController *ipc;
    UIPopoverController *popover;
    UIImage *selectedImage;
    NSMutableDictionary *cellTypeDict;
    
    
    NSMutableData *responseData;
    NSMutableDictionary *jsonDict;
    NSURLConnection *conn;
    BOOL pickerOpen;
    
    __weak IBOutlet UIActivityIndicatorView *loader;
   
    
}
@property (strong, nonatomic) IBOutlet UIView *screenView;
@property (weak, nonatomic) IBOutlet UITableView *tipTable;
@property (weak, nonatomic) IBOutlet UIButton *addTipBtn;

@property (weak, nonatomic) IBOutlet UIButton *postBtn;
@property (weak, nonatomic) IBOutlet UIButton *imagePicBtn;

@property (weak, nonatomic) IBOutlet UIButton *galleryBtn;
@property (weak, nonatomic) IBOutlet UIButton *cameraBtn;
@property (weak, nonatomic) IBOutlet UIView *imagePickerView;
@property (assign, nonatomic) CGPoint lastContentOffset;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScroll;
@property (weak, nonatomic) IBOutlet UIImageView *uploadImage;
@property (strong, nonatomic) IBOutlet UIButton *cancelBtn;

@end

@implementation TRTipViewController
@synthesize tipTypeSelected;


#pragma mark - NSURL delegates

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    
    DebugLog(@"Response Data:%@",response);
    responseData = [[NSMutableData alloc] init];
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    
    [responseData appendData:data];
    DebugLog(@"Response Data:%@",responseData);
    
}


- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    
    //initialize convert the received data to string with UTF8 encoding
    NSString *htmlSTR = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    DebugLog(@" html url%@" , htmlSTR);
    
    NSError *e = nil;
    
    jsonDict=[[NSMutableDictionary alloc]init];
    jsonDict = [NSJSONSerialization JSONObjectWithData:responseData options: NSJSONReadingMutableContainers error: &e];
    
    DebugLog(@"data %@",jsonDict);
    if(connection==conn)
    {
        
        @try {
            if([[jsonDict valueForKey:@"status"]boolValue])
            {
                
                
                
            }
            else{
                DebugLog(@"error:%@",[jsonDict valueForKey:@"message"]);
                
                
                
            }
            
            
        }
        @catch (NSException * e) {
            DebugLog(@"%@" , e);
            
            [self stopLoading];
            [[[UIAlertView alloc]initWithTitle:@"Error!!" message:@"Error in server connection" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
            
            
        }
    }
    
    
}


- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
    
    DebugLog(@"%@" , error);
    
    
}



- (void)viewDidLoad {
    [super viewDidLoad];
    rowCount=1;
    timeArr=[[NSMutableArray alloc]init];
    typeArr=[[NSArray alloc]init];
    cellTypeDict=[[NSMutableDictionary alloc]init];
    pickerOpen=false;
    
    
    self.tipView = YES;
    [self Header:_screenView ];
    
    
    for(int i=0;i<24;i++)
    {
        for(int j=0 ; j<60 ;j+=5)
        {
            [timeArr addObject:[NSString stringWithFormat:@"%02d:%02d",i,j]];
            
        }
        
    }
    
    //DebugLog(@"timeArr%@",timeArr);
    
    typeArr=@[@"Match",@"Meeting"];
    
    
    if(tipTypeSelected !=5)
    {
        CGRect Size=_postBtn.frame;
        
        Size.origin.x=_addTipBtn.frame.origin.x;
        
        Size.size.width=Size.size.width+_addTipBtn.frame.size.width;
        
        _postBtn.frame=Size;
        
        
        [_addTipBtn setHidden:YES];
        
        
    }
    // Do any additional setup after loading the view.
}

-(void)goBack
{
    CATransition *Transition=[CATransition animation];
    [Transition setDuration:0.4f];
    //[Transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
    [Transition setType:kCATransitionPush];
    [Transition setSubtype:kCATransitionFromLeft];
    [[[[self navigationController] view] layer] addAnimation:Transition forKey:@"transition"];
    [self.navigationController popViewControllerAnimated:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -Tableview_Delegates

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 270;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return rowCount;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //    static NSString *cellIdentifier=@"CellIdentifier";
    //    //NSString *cellIdentifier = [NSString stringWithFormat:@"%ld,%ld",(long)indexPath.section,(long)indexPath.row];
    //
    //    TRTipTableCell *cell=[tableView dequeueReusableCellWithIdentifier: cellIdentifier];
    //
    //
    //
    //    if (cell == nil)
    //    {
    //        DebugLog(@"CELL ALLOC");
    //        cell = [[TRTipTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    //
    //         UIView *cellView=[[[NSBundle mainBundle]loadNibNamed:@"extendedDesign" owner:self options:nil]objectAtIndex:0];
    //
    //
    //
    //
    //
    //        [cell addSubview:cellView];
    //
    //
    //    }
    
    
    //  static NSString *cellIdentifier=@"cellIdentifier";
    
    NSString *cellIdentifier = [NSString stringWithFormat:@"%ld,%ld",(long)indexPath.section,(long)indexPath.row];
    
    
    TRTipTableCell *cell = (TRTipTableCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
        
    {
        DebugLog(@"cell alloc");
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"extendedDesign" owner:self options:nil];
        
        cell = [nib objectAtIndex:0];
        
        
        // [cellArr addObject:[NSString stringWithFormat:@"%ld,%ld",(long)indexPath.section,(long)indexPath.row]];
        
        // [[NSUserDefaults standardUserDefaults]setObject:cellArr forKey:@"cellArr"];
        
        
        
    }
    
    
//    if(tipTypeSelected ==5)
//    {
        UITapGestureRecognizer *TapRecognizer1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tipClicked:)];
        [TapRecognizer1 setNumberOfTouchesRequired:1];
        [cell.iconImg1 setUserInteractionEnabled:YES];
        [cell.iconImg1 addGestureRecognizer:TapRecognizer1];
        
        //[cell.iconImg1 setTag:((indexPath.row+1)*100)+1];
        
        
        
        UITapGestureRecognizer *TapRecognizer2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tipClicked:)];
        [TapRecognizer2 setNumberOfTouchesRequired:1];
        [cell.iconImg2 setUserInteractionEnabled:YES];
        [cell.iconImg2 addGestureRecognizer:TapRecognizer2];
        
        
        UITapGestureRecognizer *TapRecognizer3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tipClicked:)];
        [TapRecognizer3 setNumberOfTouchesRequired:1];
        [cell.iconImg3 setUserInteractionEnabled:YES];
        [cell.iconImg3 addGestureRecognizer:TapRecognizer3];
        
        
        UITapGestureRecognizer *TapRecognizer4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tipClicked:)];
        [TapRecognizer4 setNumberOfTouchesRequired:1];
        [cell.iconImg4 setUserInteractionEnabled:YES];
        [cell.iconImg4 addGestureRecognizer:TapRecognizer4];
        
        if(indexPath.row==0)
        {
        
          [cell.deleteBtn setHidden:YES];
        }

        
   // }
    if(tipTypeSelected==1)
    {
        CALayer *imageLayer = cell.iconImg1.layer;
        
        [imageLayer setCornerRadius:27.5];
        [imageLayer setBorderColor:[UIColor colorWithRed:222/255.0 green:81/255.0 blue:5/255.0 alpha:1].CGColor];
        [imageLayer setBorderWidth:3];
        [cell.iconImg1.layer setMasksToBounds:YES];
        
        
        [cellTypeDict setValue:@"1" forKey:[NSString stringWithFormat:@"%ld",indexPath.row]];
        
        DebugLog(@"Type 1:%@",cellTypeDict);
        
    }
    if(tipTypeSelected==2)
    {
        CALayer *imageLayer = cell.iconImg2.layer;
        
        [imageLayer setCornerRadius:27.5];
        [imageLayer setBorderColor:[UIColor colorWithRed:222/255.0 green:81/255.0 blue:5/255.0 alpha:1].CGColor];
        [imageLayer setBorderWidth:3];
        [cell.iconImg2.layer setMasksToBounds:YES];
        
        [cellTypeDict setValue:@"2" forKey:[NSString stringWithFormat:@"%ld",indexPath.row]];
        DebugLog(@"Type 2:%@",cellTypeDict);
    }
    if(tipTypeSelected==3)
    {
        CALayer *imageLayer = cell.iconImg3.layer;
        
        [imageLayer setCornerRadius:27.5];
        [imageLayer setBorderColor:[UIColor colorWithRed:222/255.0 green:81/255.0 blue:5/255.0 alpha:1].CGColor];
        [imageLayer setBorderWidth:3];
        [cell.iconImg3.layer setMasksToBounds:YES];
        
        [cellTypeDict setValue:@"3" forKey:[NSString stringWithFormat:@"%ld",indexPath.row]];
        DebugLog(@"Type 3:%@",cellTypeDict);
    }
    if(tipTypeSelected==4)
    {
        CALayer *imageLayer = cell.iconImg4.layer;
        
        [imageLayer setCornerRadius:27.5];
        [imageLayer setBorderColor:[UIColor colorWithRed:222/255.0 green:81/255.0 blue:5/255.0 alpha:1].CGColor];
        [imageLayer setBorderWidth:3];
        [cell.iconImg4.layer setMasksToBounds:YES];
        
        [cellTypeDict setValue:@"4" forKey:[NSString stringWithFormat:@"%ld",indexPath.row]];
        DebugLog(@"Type 4:%@",cellTypeDict);
        
    }
    
    timePicker=[[UIPickerView alloc]init];
    timePicker.dataSource = self;
    timePicker.delegate = self;
    [cell.timeField setInputView:timePicker];
    
    
    typePicker=[[UIPickerView alloc]init];
    typePicker.dataSource = self;
    typePicker.delegate = self;
    //[cell.typeField setInputView:typePicker];
    
    
    
    
    cell.selectionStyle=UITableViewCellStyleDefault;
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    if(tipTypeSelected !=5)
    {
        [cell.deleteBtn setHidden:YES];
        [cell.tipDetails setHidden:YES];
        
        CGRect rectSep=cell.separator.frame;
        
        rectSep.origin.y=rectSep.origin.y-70.0/568.0*self.view.frame.size.height;
        
        cell.separator.frame=rectSep;
        
        [cell.separator.layer setZPosition:999];
        
        [cell.placeholder setHidden:YES];
        
        
        
    }
    
    
    [cell.deleteBtn addTarget:self action:@selector(deleteTip:) forControlEvents:UIControlEventTouchUpInside];
    
    
   
    cell.tipDetails.textColor = [UIColor lightGrayColor];
    
    
    return cell;
}

-(void)tipClicked:(UITapGestureRecognizer *)recognizer
{
    
    Ccell=(TRTipTableCell *)[[recognizer.view superview] superview];
    
    NSIndexPath *indexPath = [_tipTable indexPathForCell:Ccell];
    
    DebugLog(@"form no%ld",(long)indexPath.row);
    
    [Ccell.iconImg1.layer setBorderColor:[UIColor colorWithRed:206/255.0 green:206/255.0 blue:206/255.0 alpha:1].CGColor];
    [Ccell.iconImg2.layer setBorderColor:[UIColor colorWithRed:206/255.0 green:206/255.0 blue:206/255.0 alpha:1].CGColor];
    [Ccell.iconImg3.layer setBorderColor:[UIColor colorWithRed:206/255.0 green:206/255.0 blue:206/255.0 alpha:1].CGColor];
    [Ccell.iconImg4.layer setBorderColor:[UIColor colorWithRed:206/255.0 green:206/255.0 blue:206/255.0 alpha:1].CGColor];
    
    
    [cellTypeDict setValue:[NSString stringWithFormat:@"%ld",recognizer.view.tag] forKey:[NSString stringWithFormat:@"%ld",indexPath.row]];
    
    DebugLog(@"Tip Selected: %@",cellTypeDict);
    
    
    CALayer *imageLayer = recognizer.view.layer;
    [imageLayer setCornerRadius:27.5];
    [imageLayer setBorderColor:[UIColor colorWithRed:222/255.0 green:81/255.0 blue:5/255.0 alpha:1].CGColor];
    [imageLayer setBorderWidth:3];
    [recognizer.view.layer setMasksToBounds:YES];
}


- (IBAction)addTipClicked:(id)sender {
    
    
  
    
    
    NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
    
    
    [indexPaths addObject:[NSIndexPath indexPathForRow:rowCount inSection:0]];
    rowCount++;
    [_tipTable beginUpdates];
    [_tipTable insertRowsAtIndexPaths:indexPaths  withRowAnimation:UITableViewRowAnimationLeft];
    
    [_tipTable endUpdates];
    
    CGRect tableRect = _tipTable.frame;
    
    tableRect.size.height=tableRect.size.height+floor((216.0/568.0*self.view.frame.size.height));
    
    _tipTable.frame=tableRect;
    
    
    [_mainScroll setContentSize:CGSizeMake(_tipTable.frame.size.width, _tipTable.frame.size.height)];
    
    DebugLog(@"scroll frame height %f",_mainScroll.contentSize.height);
    DebugLog(@"table frame height %f",_tipTable.frame.size.height);
    
    
    //    [_tipTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:rowCount-1 inSection:0]atScrollPosition:UITableViewScrollPositionBottom
    //                             animated:YES];
    CGPoint bottomOffset = CGPointMake(0, _mainScroll.contentSize.height - _mainScroll.bounds.size.height);
    [_mainScroll setContentOffset:bottomOffset animated:YES];
    
    if(rowCount>0)
        
        [_mainScroll setHidden:NO];
    [_postBtn setAlpha:1];
    [_postBtn setUserInteractionEnabled:YES];
    
    
}

- (void)deleteTip:(UIButton *)sender {
    
    Ccell=(TRTipTableCell *)[[[sender superview] superview]superview];
    
    NSIndexPath *indexPath = [_tipTable indexPathForCell:Ccell];
    
    DebugLog(@"%ld",(long)indexPath.row);
    
    
    [cellTypeDict removeObjectForKey:[NSString stringWithFormat:@"%ld",indexPath.row]]
    ;
    
    DebugLog(@"%@",cellTypeDict);
    
    NSMutableArray *indexPathsD = [[NSMutableArray alloc] init];
    
    
    [indexPathsD addObject:indexPath];
    
    rowCount--;
    [_tipTable beginUpdates];
    [_tipTable deleteRowsAtIndexPaths:indexPathsD withRowAnimation:UITableViewRowAnimationRight];
    [_tipTable endUpdates];
    
    //[_tipTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:rowCount-1 inSection:0]atScrollPosition:UITableViewScrollPositionBottom
    // animated:YES];
    
    
    
    CGRect tableRect = _tipTable.frame;
    
    tableRect.size.height=tableRect.size.height-floor((216.0/568.0*self.view.frame.size.height));
    
    _tipTable.frame=tableRect;
    
    
    [_mainScroll setContentSize:CGSizeMake(_tipTable.frame.size.width, _tipTable.frame.size.height)];
    
    DebugLog(@"scroll frame height %f",_mainScroll.contentSize.height);
    DebugLog(@"table frame height %f",_tipTable.frame.size.height);
    
    CGPoint bottomOffset = CGPointMake(0, _mainScroll.contentSize.height - _mainScroll.bounds.size.height);
    [_mainScroll setContentOffset:bottomOffset animated:YES];
    
    if(rowCount==0)
    {
        [_mainScroll setHidden:YES];
        
        [_postBtn setAlpha:0.5];
        [_postBtn setUserInteractionEnabled:NO];
        
        
    }
}

- (IBAction)postClicked:(id)sender {
    
   // [loader setHidden:NO];
   // [loader startAnimating];
    
    _postBtn.userInteractionEnabled = NO;
    
    [self startLoading:self.view];
    
    
    
    NSMutableArray *cellArr =[[NSMutableArray alloc]init];
    
    
    
    
    for(int count=0;count<rowCount;count++)
    {
        
        NSMutableDictionary *tipDict =[[NSMutableDictionary alloc]init];
        
        NSIndexPath *indexPath=   [NSIndexPath indexPathForRow:count inSection:0];
        
        TRTipTableCell *cell = (TRTipTableCell *)[_tipTable cellForRowAtIndexPath:indexPath];
        
        //  tip_status :1=horse,2=ball,3=hand,4=footbal
        // date_time : time of the tip
        // tip_type   : 0=match,1=meeting
        // price     :price of the tip
        
        DebugLog(@"Tip Status:%@",cellTypeDict);
        if (![cellTypeDict isKindOfClass:[NSNull class]] && [cellTypeDict count]== rowCount) {
            
            [tipDict setObject:[cellTypeDict valueForKey:[NSString stringWithFormat:@"%ld",indexPath.row]] forKey:@"tip_status"];
        }
        else
        {
//            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:@"Please Select Tip Type!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//            
//            [alert show];
        }
        
        
        
        [tipDict setObject:cell.timeField.text forKey:@"date_time"];
        
//        if([cell.typeField.text isEqualToString:@"Match"])
//            [tipDict setValue:@"0" forKey:@"tip_type"];
//        else   if([cell.typeField.text isEqualToString:@"Meeting"])
//            [tipDict setValue:@"1" forKey:@"tip_type"];
        
        [tipDict setValue:cell.typeField.text forKey:@"tip_type"];
        
        [tipDict setObject:cell.priceField.text forKey:@"price"];
        
        
     
        [tipDict setObject:cell.tipDetails.text forKey:@"description"];
      
        
        
        [cellArr addObject:tipDict];
        
        
    }
    
    DebugLog(@"tipDict:%@",cellArr);
    
    NSData *imageData = [NSData dataWithData:UIImagePNGRepresentation(selectedImage)];
    
    /// NSString *base64String = [imageData base64EncodedStringWithOptions:0];
    
    
    
    
    
    //http://esolz.co.in/lab3/tipright/index.php/dashboard_ios/insert_tip?user_id=141&details=
    
    int count = 0;
    
    if(tipTypeSelected ==5)
    {
        int filled = 0, blank = 0;
        
        for (NSDictionary *tempDict in cellArr) {
            if ([[tempDict allKeys] containsObject:@"tip_status"]) {
                count++;
            }
            
            if ([[tempDict objectForKey:@"date_time"] isEqualToString:@""] && [[tempDict objectForKey:@"price"] isEqualToString:@""] && [[tempDict objectForKey:@"tip_type"] isEqualToString:@""]) {
                
                blank++;
                
            }
            else
            {
                filled++;
            }
            
            
        }
        
        
        DebugLog(@"Tip Status Count:%d",count);
        
        if (count == [cellArr count]) {
            
            if (blank == [cellArr count] || blank == 0)
            {
                
                NSError *error;
                NSData *jsonData = [NSJSONSerialization dataWithJSONObject:cellArr options:NSJSONWritingPrettyPrinted error:&error];
                NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                
                
                NSString *encodedText = [self encodeToPercentEscapeString:jsonString];
                
                
                NSString *urlString=[NSString stringWithFormat:@"%@/%@/%@/insert_tip?user_id=%@",GLOBALAPI,INDEX,DASHBOARD,[[NSUserDefaults standardUserDefaults] valueForKey:UDUSERID]];
                
                DebugLog(@"post url in create group : %@",urlString);
                
                AFHTTPClient *client= [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:urlString]];
                
                
                
                NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:encodedText, @"details", nil];
                
                
                
                NSMutableURLRequest *request = [client multipartFormRequestWithMethod:@"POST" path:nil parameters:parameters constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
                    
                    
                    if ([imageData length]>0) {
                        
                        
                        
                        NSString *imagename = [NSString stringWithFormat:@"photo"];
                        
                        
                        
                        [formData appendPartWithFileData: imageData name:imagename fileName:@"temp.jpeg" mimeType:@"image/jpeg"];
                        
                        
                    }
                    
                    
                }];
                
                
                AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
                
                
                [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation * _operation, id responseObject) {
                    
                    
                    
                    //  NSString *response = [_operation responseString];
                    
                    
                    
                    // DebugLog(@"response: [%@]",response);
                    
                    
                    
                    //[loader stopAnimating];
                    [self stopLoading];
                    
                    
                    NSError *error;
                    jsonDict = [NSJSONSerialization JSONObjectWithData:responseObject //1
                                
                                                               options:kNilOptions
                                
                                                                 error:&error];
                    DebugLog(@"json returns: %@",jsonDict);
                    
                    if ([[jsonDict valueForKey:@"status"]isEqualToString:@"Success"]) {
                        
                        
                        TRGlobalMethods *pushView = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"Home"];
                        [[self navigationController ]pushViewController:pushView animated:YES];
                        _postBtn.userInteractionEnabled = YES;
                    }
                    else{
                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@",[jsonDict valueForKey:@"message"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        
                        [alert show];
                        _postBtn.userInteractionEnabled = YES;
                        
                    }
                    
                    
                    
                } failure:^(AFHTTPRequestOperation * _operation, NSError *error) {
                    
                    
                    
                    
                    // [loader stopAnimating];
                    [self stopLoading];
                    
                    
                    if([_operation.response statusCode] == 403){
                        
                        
                        
                        DebugLog(@"Upload Failed");
                        
                        
                        
                        return;
                        
                    }
                    
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:@"Failed to upload!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    
                    
                    
                    [alert show];
                    
                }];
                
                
                
                [operation start];
                
            }
            else
            {
                DebugLog(@"Blank & Array Count:%d %ld",blank, [cellArr count]);
                
                [self stopLoading];
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:@"Please Fill At Least One Field!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                
                [alert show];
                _postBtn.userInteractionEnabled = YES;
            }
            
        }
        else
        {
            [self stopLoading];
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:@"Please Select Tip Type!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            [alert show];
            _postBtn.userInteractionEnabled = YES;
        }
        
    }
    
    else{
        
        
        NSMutableDictionary *tipDict =[[NSMutableDictionary alloc]init];
        
        tipDict=[cellArr objectAtIndex:0];
        
        DebugLog(@"Tip TYPE:%@",[tipDict valueForKey:@"tip_type"]);
        
        //  http://www.esolz.co.in/lab3/tipright/index.php/dashboard_ios/insert_quicktip?tip_status=2&date_time=21:05:50&tip_type=1&price=100&user_id=123
        
        
        NSString *urlString=[NSString stringWithFormat:@"%@/%@/%@/insert_quicktip?tip_status=%@&date_time=%@&tip_type=%@&price=%@&user_id=%@",GLOBALAPI,INDEX,DASHBOARD,[tipDict valueForKey:@"tip_status"],[tipDict valueForKey:@"date_time"],[tipDict valueForKey:@"tip_type"],[tipDict valueForKey:@"price"],[[NSUserDefaults standardUserDefaults] valueForKey:UDUSERID]];
        
        DebugLog(@"post url in create group : %@",urlString);
        
        
        urlString=[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        
        
        AFHTTPClient *client= [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:urlString]];
        
        
        
        
        NSMutableURLRequest *request = [client multipartFormRequestWithMethod:@"POST" path:nil parameters:nil constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
            
            
            if ([imageData length]>0) {
                
                
                
                NSString *imagename = [NSString stringWithFormat:@"photo"];
                
                
                
                [formData appendPartWithFileData: imageData name:imagename fileName:@"temp.jpeg" mimeType:@"image/jpeg"];
                
                
            }
            
            
        }];
        
        
        
        
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        
        
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation * _operation, id responseObject) {
            
            
            
            //  NSString *response = [_operation responseString];
            
            
            
            // DebugLog(@"response: [%@]",response);
            
            
           // [loader stopAnimating];
                   [self stopLoading];
            
            
            NSError *error;
            jsonDict = [NSJSONSerialization JSONObjectWithData:responseObject
                        
                                                       options:kNilOptions
                        
                                                         error:&error];
            DebugLog(@"json returns: %@",jsonDict);
            
            if ([[jsonDict valueForKey:@"status"]isEqualToString:@"Success"]) {
                
                
                TRGlobalMethods *pushView = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"Home"];
                [[self navigationController ]pushViewController:pushView animated:YES];
                
                _postBtn.userInteractionEnabled = YES;
            }
            else{
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@",[jsonDict valueForKey:@"message"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                
                [alert show];
                _postBtn.userInteractionEnabled = YES;
                
            }
            
            
            
        } failure:^(AFHTTPRequestOperation * _operation, NSError *error) {
            
            
            
                   [self stopLoading];
            
            if([_operation.response statusCode] == 403){
                
                DebugLog(@"Upload Failed");
                
                return;
                
            }
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:@"Failed to upload!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            alert.tag=100;
            
            [alert show];
            
            _postBtn.userInteractionEnabled = YES;
            
        }];
        
        
        
        [operation start];
    }
    
    
    
}

-(NSString*) encodeToPercentEscapeString:(NSString *)string {
    return (NSString *)
    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                              (CFStringRef) string,
                                                              NULL,
                                                              (CFStringRef) @"!*'();:@&=+$,/?%#[]",
                                                              kCFStringEncodingUTF8));
}

- (IBAction)imagePickerClicked:(id)sender {
    
    if(pickerOpen)
    {
        [_imagePickerView setHidden:YES];
        pickerOpen=false;
          [_imagePicBtn setTitle:@"Choose Image" forState:UIControlStateNormal];
    }
    else{
      [_imagePickerView setHidden:NO];
        [_imagePicBtn setTitle:@"Cancel" forState:UIControlStateNormal];
        pickerOpen=true;
        
        
        
    }
    
  
    
}
- (IBAction)galleryClicked:(id)sender {
    
    
    ipc= [[UIImagePickerController alloc] init];
    ipc.delegate = self;
    ipc.allowsEditing=YES;
    ipc.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    
    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone)
        [self presentViewController:ipc animated:YES completion:nil];
    else
    {
        popover=[[UIPopoverController alloc]initWithContentViewController:ipc];
        [popover presentPopoverFromRect:self.view.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    
}

- (IBAction)cameraClicked:(id)sender {
    
    ipc = [[UIImagePickerController alloc] init];
    ipc.delegate = self;
    ipc.allowsEditing=YES;
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        ipc.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:ipc animated:YES completion:NULL];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No Camera Available." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        alert = nil;
    }
    
    
}

- (IBAction)cancelClicked:(id)sender {
    
    [_imagePickerView setHidden:YES];
}


#pragma mark - ImagePickerController Delegate

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    DebugLog(@"%@",info);
    
    
    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
        [picker dismissViewControllerAnimated:YES completion:nil];
    } else {
        [popover dismissPopoverAnimated:YES];
    }
    
    
    
    selectedImage=[info objectForKey:UIImagePickerControllerEditedImage];
    
    
    
    [_uploadImage setImage:selectedImage];
    [_uploadImage setHidden:NO];
    NSString *imageName;
    
    if([info[UIImagePickerControllerMediaType] isEqualToString:@"public.image"])
    {
    
        imageName =    [info objectForKey:@"UIImagePickerControllerMediaType"];

    }
    else{
    NSURL *imagePath = [info objectForKey:@"UIImagePickerControllerReferenceURL"];
    
    imageName = [imagePath lastPathComponent];
    }
  
    [_imagePicBtn setTitle:imageName forState:UIControlStateNormal];
    
  
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    [_imagePickerView setHidden:YES];
    
}




#pragma mark - picker view delegates
// The number of columns of data
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}


// The number of rows of data
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if([Ccell.timeField isFirstResponder])
        return timeArr.count;
    if([Ccell.typeField isFirstResponder])
        return typeArr.count;
    else
        return 1;
    
}


// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if([Ccell.timeField isFirstResponder])
        return [timeArr objectAtIndex:row];
    if([Ccell.typeField isFirstResponder])
        return [typeArr objectAtIndex:row];
    else
        return @"Select";
    
}


// Catpure the picker view selection
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    if([Ccell.timeField isFirstResponder])
    {
        Ccell.timeField.text=[NSString stringWithFormat:@"%@",[timeArr objectAtIndex:row]];
        [Ccell.timeField resignFirstResponder];
    }
    if([Ccell.typeField isFirstResponder])
    {
        Ccell.typeField.text=[NSString stringWithFormat:@"%@",[typeArr objectAtIndex:row]];
        [Ccell.typeField resignFirstResponder];
    }
    
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    
    Ccell = (TRTipTableCell*)[[[textField superview]superview]superview];
    
    
    
    
    
    return YES;
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    Ccell = (TRTipTableCell*)[[[textField superview]superview]superview];
    
  //  if([Ccell.priceField isFirstResponder])
  //  {
  //      [textField setText:@"$"];
   // }
    
   // else
   // {
        [textField setText:@""];
   // }
    
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    Ccell = (TRTipTableCell*)[[[textField superview]superview]superview];
    
//    if([Ccell.priceField isFirstResponder])
//    {
//        
//        // DebugLog(@"string is%@",string);
//        
//        
//        if(textField.text.length==1 && [string isEqualToString:@""])
//            return NO;
//        else
//        {
//            if(![string isEqualToString: @""])
//            {
//                NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
//                [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
//                NSNumber *number = [formatter numberFromString:string];
//                if ( number ==nil )
//                    return NO;
//                else
//                    return YES;
//            }
//            else
//                return YES;
//        }
//    }
    
    
    if (textField==Ccell.typeField || textField==Ccell.priceField) {
        
        if(textField.text.length<30)
        {
            return YES;
        }
        else{
            return NO;
            
        }
    }
    
    
    return YES;
    
    
}

-(void) scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    // DebugLog(@"scrolling");
    
    
    CGPoint currentOffset = scrollView.contentOffset;
    if (currentOffset.y > self.lastContentOffset.y)
    {
        [[NSUserDefaults standardUserDefaults]setValue:@"down" forKey:@"direction"];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults]setValue:@"up" forKey:@"direction"];
    }
    self.lastContentOffset = currentOffset;
}


- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
   
  Ccell = (TRTipTableCell*)[[[textView superview]superview]superview];
    
    Ccell.placeholder.hidden=YES;
    
    
    textView.text = @"";
    textView.textColor = [UIColor blackColor];
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    
    if([text isEqualToString:@"\n"])
        [textView resignFirstResponder];
    return YES;
    
}

-(void)textViewDidChange:(UITextView *)textView
{
    
    if(textView.text.length == 0){
        textView.textColor = [UIColor lightGrayColor];
        
        Ccell = (TRTipTableCell*)[[[textView superview]superview]superview];
        
        Ccell.placeholder.hidden=NO;
        
        [textView resignFirstResponder];
    }
    
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
[textField resignFirstResponder];
    
    return  YES;
    
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
