//
//  TRTipViewController.h
//  Tip Right
//
//  Created by santanu on 08/06/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import "TRGlobalMethods.h"

@interface TRTipViewController : TRGlobalMethods<UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UITextViewDelegate,UIScrollViewDelegate,NSURLConnectionDataDelegate,NSURLConnectionDelegate>

@property (nonatomic) int tipTypeSelected;


@end
