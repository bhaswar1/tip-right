//
//  TRGlobalMethods.m
//  Tip Right
//
//  Created by santanu on 29/05/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import "TRGlobalMethods.h"

#import "TRNotificationsViewController.h"
#import "TRHomeViewController.h"
#import "TRSearchViewController.h"
#import "TRTipViewController.h"
#import "TRLoginViewController.h"
#import "TROwnProfileViewController.h"
#import "TREdit_ProfileViewController.h"
#import "TRChangePasswordViewController.h"
#import "TRMessageListViewController.h"
#import "TRForgetPasswordViewController.h"
#import "TRSignUpViewController.h"
#import "TRChangePasswordViewController.h"
#import "TRTerms&ConditionViewController.h"

@interface TRGlobalMethods ()
{
    UIImageView *home, *search, *plus, *chat, *notification, *menu, *ball_img, *football_img, *horse_img, *gloves_img, *tip_img,*settingsImg, *notify_count;
    UIButton *plus_btn,*settingsBtn,*notificationBtn,*homeBtn,*searchBtn, *menuBtn, *signoutBtn, *profileBtn, *editProfile_btn, *password_btn,*messageBtn;
    
    UIButton *tipClick,*tip1Click,*tip2Click,*tip3Click,*tip4Click;
    NSDictionary *json;
    UIView *Self, *footerView, *blackView,*settings, *settingView;
    UILabel *titleLabel, *count;
    UIActivityIndicatorView *act;
    BOOL rotate, press;
    NSArray *menuArray;
    CGRect frame;
    UIAlertView *alert;
    UIActivityIndicatorView *activityIndicator;
    
}



@end

@implementation TRGlobalMethods
@synthesize msgCount,message_count;

-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeMsgCount) name:@"Received_MsgOutside" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeNotificationCount) name:@"Received_NotificationOutside" object:nil];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    menuArray = @[@"Profile",@"Sign out"];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




-(void)popToViewController:(BOOL)animation
{
    NSArray* arr = [[NSArray alloc] initWithArray:self.navigationController.viewControllers];
    NSInteger index=[arr count]-(1+1);
    CATransition *Transition=[CATransition animation];
    [Transition setDuration:0.4f];
    //[Transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
    [Transition setType:kCATransitionPush];
    [Transition setSubtype:kCATransitionFromLeft];
    [[[[self navigationController] view] layer] addAnimation:Transition forKey:@"transition"];
    [self.navigationController popToViewController:[arr objectAtIndex:index] animated:NO];
}



-(void)startLoading:(UIView *)loaderView
{
    


activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.alpha = 1.0;
    activityIndicator.center =loaderView.center;

[activityIndicator setColor:[UIColor orangeColor]];

    //[activityIndicator setColor:[UIColor colorWithRed:0.6 green:0.8 blue:1.0 alpha:1.0]];
    activityIndicator.layer.zPosition = 5;

    activityIndicator.hidesWhenStopped = YES;
    [loaderView addSubview:activityIndicator];
    [activityIndicator startAnimating];


}

-(void)stopLoading
{
    [activityIndicator stopAnimating];
}


-(void)Header:(UIView *)mainView
{
   
    UIView *headerView = [[UIView alloc] init];
    headerView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 60);
    headerView.backgroundColor = [UIColor blackColor];
    [mainView addSubview:headerView];
    
    

    
//
//    frame.size.width = [UIScreen mainScreen].bounds.size.width/2;
//    settingView.frame = frame;
//    settingView.backgroundColor = [UIColor blackColor];
//    [self.view addSubview:settingView];
    
    
    menu = [[UIImageView alloc] init];
    menu.frame = CGRectMake(15,30,25,15);
    
    [headerView addSubview:menu];
    
    menuBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 20, 80, 40)];
    menuBtn.userInteractionEnabled = YES;
    [menuBtn setBackgroundColor:[UIColor clearColor]];
    [menuBtn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [menuBtn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:(209.0f/255.0f) green:(209.0f/255.0f) blue:(209.0f/255.0f) alpha:0.5f]] forState:UIControlStateHighlighted];
    if (_tipView || [self isKindOfClass:[TRForgetPasswordViewController class]]) {
        menu.frame = CGRectMake(20, 30, 10, 15);
        menu.image = [UIImage imageNamed:@"back-arrow"];
        [menuBtn addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];    }
    else
    {
        menu.image = [UIImage imageNamed:@"menu"];
        [menuBtn addTarget:self action:@selector(settingsClicked) forControlEvents:UIControlEventTouchUpInside];
        
        
        frame.size.width = [UIScreen mainScreen].bounds.size.width/2;
        settingView = [[UIView alloc] init];
        settingView.frame = CGRectMake(-frame.size.width, 60, frame.size.width, [UIScreen mainScreen].bounds.size.height-116);
        //frame = settingView.frame;
        settingView.backgroundColor = [UIColor blackColor];
        [self.view addSubview:settingView];
        
        
        profileBtn = [[UIButton alloc] init];
        profileBtn.frame = CGRectMake(10, 0, 100, 70);
        [profileBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [profileBtn setTitle:@"Profile" forState:UIControlStateNormal];
        profileBtn.titleLabel.textAlignment = NSTextAlignmentLeft;
        [profileBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        
        [settingView addSubview:profileBtn];
        [profileBtn addTarget:self action:@selector(profile) forControlEvents:UIControlEventTouchUpInside];
        
        UILabel *profileBtn_div = [[UILabel alloc] init];
        profileBtn_div.frame = CGRectMake(0, 69.5, settingView.frame.size.width, 0.5f);
        profileBtn_div.backgroundColor = [UIColor grayColor];
        [settingView addSubview:profileBtn_div];
        
        
        editProfile_btn = [[UIButton alloc] init];
        editProfile_btn.frame = CGRectMake(10, 70, 100, 70);
        [editProfile_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [editProfile_btn setTitle:@"Edit Profile" forState:UIControlStateNormal];
        editProfile_btn.titleLabel.textAlignment = NSTextAlignmentLeft;
        [editProfile_btn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        
        
        
        [settingView addSubview:editProfile_btn];
        [editProfile_btn addTarget:self action:@selector(editProfile) forControlEvents:UIControlEventTouchUpInside];
        
        UILabel *editProfile_btn_div = [[UILabel alloc] init];
        editProfile_btn_div.frame = CGRectMake(0, 139.5, settingView.frame.size.width, 0.5f);
        editProfile_btn_div.backgroundColor = [UIColor grayColor];
        [settingView addSubview:editProfile_btn_div];
        
        password_btn = [[UIButton alloc] init];
        password_btn.frame = CGRectMake(10, 140, 170, 70);
        [password_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [password_btn setTitle:@"Change Password" forState:UIControlStateNormal];
        password_btn.titleLabel.textAlignment = NSTextAlignmentLeft;
        [password_btn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        
        [settingView addSubview:password_btn];
        [password_btn addTarget:self action:@selector(changePassword) forControlEvents:UIControlEventTouchUpInside];
        
        UILabel *password_btn_div = [[UILabel alloc] init];
        password_btn_div.frame = CGRectMake(0, 209.5, settingView.frame.size.width, 0.5f);
        password_btn_div.backgroundColor = [UIColor grayColor];
        [settingView addSubview:password_btn_div];
        
        
        
        signoutBtn = [[UIButton alloc] init];
        signoutBtn.frame = CGRectMake(10, 210, 100, 70);
        [signoutBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [signoutBtn setTitle:@"Sign out" forState:UIControlStateNormal];
        signoutBtn.titleLabel.textAlignment = NSTextAlignmentLeft;
        [signoutBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        
        [settingView addSubview:signoutBtn];
        
        UILabel *signout_div = [[UILabel alloc] init];
        signout_div.frame = CGRectMake(0, 279.5, settingView.frame.size.width, 0.5f);
        signout_div.backgroundColor = [UIColor grayColor];
        [settingView addSubview:signout_div];
        [signoutBtn addTarget:self action:@selector(sign_out) forControlEvents:UIControlEventTouchUpInside];

        
    }
    
    
    [headerView addSubview:menuBtn];
    
    
    // [headerView addSubview:settings];
    
    
    
    titleLabel =[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 180, 20)];
    titleLabel.center=headerView.center;
    
    
    CGRect labelFrame;
    labelFrame=titleLabel.frame;
    labelFrame.origin.y=labelFrame.origin.y+5;
    titleLabel.frame=labelFrame;
    
    
    [titleLabel setTextColor:[UIColor whiteColor]];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    
    
    if (IsIphone6plus) {
        titleLabel.font=[UIFont  boldSystemFontOfSize:20];
    }
    else if (IsIphone6) {
        titleLabel.font=[UIFont  boldSystemFontOfSize:18];
    }
    else  {
        titleLabel.font=[UIFont  boldSystemFontOfSize:16];
    }
    [headerView addSubview:titleLabel];
    
    
    
    
    if([self isKindOfClass:[TRSearchViewController class]])
        [titleLabel setText:@"Search"];
    
     if([self isKindOfClass:[TRMessageListViewController class]])
         [titleLabel setText:@"Recent Buddies"];
    
    if ([self isKindOfClass:[TRTerms_ConditionViewController class]]) {
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"Terms"]  isEqualToString:@"Terms"]) {
            [titleLabel setText:@"Terms & Conditions"];
        }
        else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"Terms"]  isEqualToString:@"Privacy"])
        {
            [titleLabel setText:@"Privacy"];
        }
        
    }
    
//    if([self isKindOfClass:[TRNotificationsViewController class]])
//    {
//        
//        settings=[[UIView alloc]initWithFrame:CGRectMake(headerView.frame.size.width-60, 0, 60, headerView.frame.size.height)];
//        
//        //        [settings setBackgroundColor:[UIColor greenColor]];
//        
//        
//        settingsImg = [[UIImageView alloc] init];
//        settingsImg.frame = CGRectMake(20,25,20,20);
//        settingsImg.image = [UIImage imageNamed:@"settings"];
//        [settings addSubview:settingsImg];
//        
//        
//        settingsBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 60, 60)];
//        
//        [settingsBtn setBackgroundColor:[UIColor clearColor]];
//        
//        [settingsBtn addTarget:self action:@selector(settingsClicked) forControlEvents:UIControlEventTouchUpInside];
//        
//        
//        [settings addSubview:settingsBtn];
//        
//        
//        [headerView addSubview:settings];
//        
//        
//    }
    
}

-(void)goBack{
    CATransition *Transition=[CATransition animation];
    [Transition setDuration:0.2f];
    //[Transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
    [Transition setType:kCATransitionPush];
    [Transition setSubtype:kCATransitionFromLeft];
    [[[[self navigationController] view] layer] addAnimation:Transition forKey:@"transition"];
    [self.navigationController popViewControllerAnimated:NO];

}

-(void)Footer:(UIView *)mainView
{
    float gap = ([UIScreen mainScreen].bounds.size.width-172)/6-35;
    
    footerView = [[UIView alloc] init];
    footerView.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height-56, [UIScreen mainScreen].bounds.size.width, 56);
    footerView.backgroundColor = [UIColor blackColor];
    [mainView addSubview:footerView];
    
    home = [[UIImageView alloc] init];
    home.frame = CGRectMake(gap+25, 14, 30, 28);
    home.image = [UIImage imageNamed:@"bottom-bar-icon1"];
    [footerView addSubview:home];
    
    homeBtn =[[UIButton alloc]initWithFrame:CGRectMake(home.frame.origin.x-10, home.frame.origin.y-10, home.frame.size.width+10, home.frame.size.height+10)];
    
    [homeBtn addTarget:self action:@selector(homeClicked) forControlEvents:UIControlEventTouchUpInside];
    
    if([self isKindOfClass:[TRHomeViewController class]])
        [homeBtn setUserInteractionEnabled:NO];
    
    [footerView addSubview:homeBtn];
    
    search = [[UIImageView alloc] init];
    search.frame = CGRectMake(home.frame.origin.x+home.frame.size.width+gap+40, 14, 30, 28);
    search.image = [UIImage imageNamed:@"bottom-bar-icon2"];
    [footerView addSubview:search];
    
    
    searchBtn =[[UIButton alloc]initWithFrame:CGRectMake(search.frame.origin.x-10, search.frame.origin.y-10, search.frame.size.width+10, search.frame.size.height+10)];
    
    [searchBtn addTarget:self action:@selector(searchClicked) forControlEvents:UIControlEventTouchUpInside];
    
    if([self isKindOfClass:[TRSearchViewController class]])
        [searchBtn setUserInteractionEnabled:NO];
    
    
    
    [footerView addSubview:searchBtn];
    
    
    plus = [[UIImageView alloc] init];
    plus.frame = CGRectMake(search.frame.origin.x+search.frame.size.width+gap+40, -5, 56, 56);
    plus.image = [UIImage imageNamed:@"bottom-bar-icon3"];
    [footerView addSubview:plus];
    
    DebugLog(@"PLUS FRAME:%lf",plus.frame.origin.x);
    
    plus_btn = [[UIButton alloc] init];
    plus_btn.frame = CGRectMake(plus.frame.origin.x, plus.frame.origin.y, plus.frame.size.width, plus.frame.size.height);
    [footerView addSubview:plus_btn];
    [plus_btn addTarget:self action:@selector(Rotate) forControlEvents:UIControlEventTouchUpInside];
    
    chat = [[UIImageView alloc] init];
    chat.frame = CGRectMake(plus.frame.origin.x+plus.frame.size.width+gap+40, 15.5, 32, 25);
    chat.image = [UIImage imageNamed:@"bottom-bar-icon4"];
    [footerView addSubview:chat];
    
    messageBtn =[[UIButton alloc]initWithFrame:CGRectMake(chat.frame.origin.x-10, chat.frame.origin.y-10, chat.frame.size.width+10, chat.frame.size.height+10)];
    
    [messageBtn addTarget:self action:@selector(messageClicked) forControlEvents:UIControlEventTouchUpInside];
    
    if([self isKindOfClass:[TRMessageListViewController class]])
        [messageBtn setUserInteractionEnabled:NO];
    
    
    
    [footerView addSubview:messageBtn];
    

  message_count = [[UIImageView alloc] init];
    message_count.frame = CGRectMake(chat.frame.origin.x+8, 5, 27, 27);
    message_count.image = [UIImage imageNamed:@"notification-bg"];
    
    
    
     [footerView addSubview:message_count];
        
    msgCount = [[UILabel alloc] init];
    
    msgCount.frame = CGRectMake(chat.frame.origin.x+8, 5, 27, 27);
    
    msgCount.font = [UIFont fontWithName:@"Helvetica Neue" size:12.0];
    msgCount.textAlignment = NSTextAlignmentCenter;
    msgCount.textColor = [UIColor whiteColor];
 msgCount.text=[[NSUserDefaults standardUserDefaults]valueForKey:@"msgCount"];
    
    [footerView addSubview:msgCount];
    
            DebugLog( @"count is:%@", [[NSUserDefaults standardUserDefaults]valueForKey:@"msgCount"]);
    
    if([[[NSUserDefaults standardUserDefaults]valueForKey:@"msgCount"] intValue]>0 )
    {
        [message_count setHidden:NO];
        [msgCount setHidden:NO];
    }
    else
    {
        [message_count setHidden:YES];
         [msgCount setHidden:YES];
    }
    
    notification = [[UIImageView alloc] init];
    notification.frame = CGRectMake(chat.frame.origin.x+chat.frame.size.width+gap+40, 14, 24, 28);
    notification.image = [UIImage imageNamed:@"bottom-bar-icon5"];
    [footerView addSubview:notification];
    
    notificationBtn =[[UIButton alloc]initWithFrame:CGRectMake(notification.frame.origin.x-10, notification.frame.origin.y-10, notification.frame.size.width+10, notification.frame.size.height+10)];
    
    [notificationBtn addTarget:self action:@selector(notifcationsClicked) forControlEvents:UIControlEventTouchUpInside];
    
    if([self isKindOfClass:[TRNotificationsViewController class]])
        [notificationBtn setUserInteractionEnabled:NO];
    
    
    
    [footerView addSubview:notificationBtn];
    
    [self NotifyCountView];
    
//    notify_count = [[UIImageView alloc] init];
//    notify_count.frame = CGRectMake(notification.frame.origin.x+8, 5, 27, 27);
//    notify_count.image = [UIImage imageNamed:@"notification-bg"];
//    [footerView addSubview:notify_count];
//    
//    count = [[UILabel alloc] init];
//    count.frame = CGRectMake(notification.frame.origin.x+8, 5, 27, 27);
//    count.text = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"notificationCount"]];
//    count.font = [UIFont fontWithName:@"Helvetica Neue" size:12.0];
//    count.textAlignment = NSTextAlignmentCenter;
//    count.textColor = [UIColor whiteColor];
//    [footerView addSubview:count];
//    
//    if([[[NSUserDefaults standardUserDefaults]valueForKey:@"notificationCount"] intValue]>0 )
//    {
//        [notify_count setHidden:NO];
//        [count setHidden:NO];
//    }
//    else
//    {
//        [notify_count setHidden:YES];
//        [count setHidden:YES];
//    }
    
    
}

-(void)NotifyCountView
{
    notify_count = [[UIImageView alloc] init];
    notify_count.frame = CGRectMake(notification.frame.origin.x+8, 5, 27, 27);
    notify_count.image = [UIImage imageNamed:@"notification-bg"];
    [footerView addSubview:notify_count];
    
    count = [[UILabel alloc] init];
    count.frame = CGRectMake(notification.frame.origin.x+8, 5, 27, 27);
    count.text = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"notificationCount"]];
    count.font = [UIFont fontWithName:@"Helvetica Neue" size:12.0];
    count.textAlignment = NSTextAlignmentCenter;
    count.textColor = [UIColor whiteColor];
    [footerView addSubview:count];
    
    if([[[NSUserDefaults standardUserDefaults]valueForKey:@"notificationCount"] intValue]>0 )
    {
        [notify_count setHidden:NO];
        [count setHidden:NO];
    }
    else
    {
        [notify_count setHidden:YES];
        [count setHidden:YES];
    }

}

-(void)changeMsgCount
{
    
    
    [message_count setHidden:NO];
    [msgCount setHidden:NO];
 msgCount.text=[[NSUserDefaults standardUserDefaults]valueForKey:@"msgCount"];

}

-(void)changeNotificationCount
{
    DebugLog(@"NOTI FY COUNT:%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"notificationCount"]);
    
    [notify_count setHidden:NO];
    [count setHidden:NO];
    
    count.text = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"notificationCount"]];
    [self NotifyCountView];
    
}


-(void)settingsClicked
{
    DebugLog(@"MENU");
    if (!press) {
        
        //settingView = [[UIView alloc] init];
        [UIView animateWithDuration:0.15f animations:^{
            
            frame = settingView.frame;
            frame.origin.x = 0.0f;
            settingView.frame = frame;
            //settingView.backgroundColor = [UIColor blackColor];
//            [self.view addSubview:settingView];
            press = TRUE;
            
            
//            profileBtn = [[UIButton alloc] init];
//            profileBtn.frame = CGRectMake(0, 0, 100, 70);
//            [profileBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//            [profileBtn setTitle:@"Profile" forState:UIControlStateNormal];
//            
//            [settingView addSubview:profileBtn];
//            [profileBtn addTarget:self action:@selector(profile) forControlEvents:UIControlEventTouchUpInside];
//            
//            UILabel *profileBtn_div = [[UILabel alloc] init];
//            profileBtn_div.frame = CGRectMake(0, 69.5, settingView.frame.size.width, 0.5f);
//            profileBtn_div.backgroundColor = [UIColor grayColor];
//            [profileBtn addSubview:profileBtn_div];
//            
//            
//            editProfile_btn = [[UIButton alloc] init];
//            editProfile_btn.frame = CGRectMake(0, 70, 100, 70);
//            [editProfile_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//            [editProfile_btn setTitle:@"Edit Profile" forState:UIControlStateNormal];
//            
//            [settingView addSubview:editProfile_btn];
//            [editProfile_btn addTarget:self action:@selector(editProfile) forControlEvents:UIControlEventTouchUpInside];
//            
//            UILabel *editProfile_btn_div = [[UILabel alloc] init];
//            editProfile_btn_div.frame = CGRectMake(0, 69.5, settingView.frame.size.width, 0.5f);
//            editProfile_btn_div.backgroundColor = [UIColor grayColor];
//            [editProfile_btn addSubview:editProfile_btn_div];
//            
//            password_btn = [[UIButton alloc] init];
//            password_btn.frame = CGRectMake(0, 140, 170, 70);
//            [password_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//            [password_btn setTitle:@"Change Password" forState:UIControlStateNormal];
//            
//            [settingView addSubview:password_btn];
//            [password_btn addTarget:self action:@selector(changePassword) forControlEvents:UIControlEventTouchUpInside];
//            
//            UILabel *password_btn_div = [[UILabel alloc] init];
//            password_btn_div.frame = CGRectMake(0, 69.5, settingView.frame.size.width, 0.5f);
//            password_btn_div.backgroundColor = [UIColor grayColor];
//            [password_btn addSubview:password_btn_div];
//
//            
//            
//            signoutBtn = [[UIButton alloc] init];
//            signoutBtn.frame = CGRectMake(0, 210, 100, 70);
//            [signoutBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//            [signoutBtn setTitle:@"Sign out" forState:UIControlStateNormal];
//            
//            [settingView addSubview:signoutBtn];
//            
//            UILabel *signout_div = [[UILabel alloc] init];
//            signout_div.frame = CGRectMake(0, 69.5, settingView.frame.size.width, 0.5f);
//            signout_div.backgroundColor = [UIColor grayColor];
//            [signoutBtn addSubview:signout_div];
//            [signoutBtn addTarget:self action:@selector(sign_out) forControlEvents:UIControlEventTouchUpInside];
            
        }];
        //        UITableView *menuTable = [[UITableView alloc] init];
        //        menuTable.frame = CGRectMake(0, 0, settingView.frame.size.width, settingView.frame.size.height);
        //        menuTable.backgroundColor = [UIColor greenColor];
        //        menuTable.delegate = self;
        //        menuTable.dataSource = self;
        //        [settingView addSubview:menuTable];
    }
    else{
        //settingView = [[UIView alloc] init];
        [UIView animateWithDuration:0.15f animations:^{
//            [profileBtn removeFromSuperview];
//            [signoutBtn removeFromSuperview];
//            [editProfile_btn removeFromSuperview];
//            [password_btn removeFromSuperview];
           // settingView.frame = CGRectMake(0, 60,0, 0);
            
            frame = settingView.frame;
            frame.origin.x = -frame.size.width;
            settingView.frame = frame;
            
            //        settingView.backgroundColor = [UIColor blackColor];
            //        [self.view addSubview:settingView];
            press = FALSE;
        }];
    }
    
}

-(void)profile
{
    
      if ([self isKindOfClass:[TROwnProfileViewController class]]) {
          
//          TROwnProfileViewController *own = [[TROwnProfileViewController alloc] init];

          DebugLog(@"Profile Page:%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"globalID"]);
        
          if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"globalID"] isEqualToString:[[NSUserDefaults standardUserDefaults] valueForKey:UDUSERID]]) {
              
              [UIView animateWithDuration:0.15f animations:^{
                  //            [profileBtn removeFromSuperview];
                  //            [signoutBtn removeFromSuperview];
                  //            [editProfile_btn removeFromSuperview];
                  //            [password_btn removeFromSuperview];
                  //settingView.frame = CGRectMake(0, 60,0, 0);
                  frame = settingView.frame;
                  frame.origin.x = -frame.size.width;
                  settingView.frame = frame;
                  
                  //        settingView.backgroundColor = [UIColor blackColor];
                  //        [self.view addSubview:settingView];
                  press = FALSE;
              }];
              
          }

          else{
            TROwnProfileViewController *pushView = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"profile"];
            pushView.userId=[[NSUserDefaults standardUserDefaults]valueForKey:UDUSERID];
            // pushView.isFromRequest=YES;
              
            
            [[self navigationController ]pushViewController:pushView animated:YES];
          }
          
      }
    else
    {
        TROwnProfileViewController *pushView = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"profile"];
        pushView.userId=[[NSUserDefaults standardUserDefaults]valueForKey:UDUSERID];
        // pushView.isFromRequest=YES;
        
        [[self navigationController ]pushViewController:pushView animated:YES];
    }
    
        
}


-(void)editProfile
{
    DebugLog(@"Profile Page");
    if([self isKindOfClass:[TREdit_ProfileViewController class]])
    {
        [UIView animateWithDuration:0.15f animations:^{
//            [profileBtn removeFromSuperview];
//            [signoutBtn removeFromSuperview];
//            [editProfile_btn removeFromSuperview];
//            [password_btn removeFromSuperview];
            //settingView.frame = CGRectMake(0, 60,0, 0);
            frame = settingView.frame;
            frame.origin.x = -frame.size.width;
            settingView.frame = frame;
            
            //        settingView.backgroundColor = [UIColor blackColor];
            //        [self.view addSubview:settingView];
            press = FALSE;
        }];
    }
    else
    {
    TREdit_ProfileViewController *pushView = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"edit"];
    [[self navigationController ]pushViewController:pushView animated:YES];
    }
}

-(void)changePassword
{
    DebugLog(@"Password Change Page");
    if([self isKindOfClass:[TRChangePasswordViewController class]])
    {
        [UIView animateWithDuration:0.15f animations:^{
//            [profileBtn removeFromSuperview];
//            [signoutBtn removeFromSuperview];
//            [editProfile_btn removeFromSuperview];
//            [password_btn removeFromSuperview];
            //settingView.frame = CGRectMake(0, 60,0, 0);
            
            frame = settingView.frame;
            frame.origin.x = -frame.size.width;
            settingView.frame = frame;
            //        settingView.backgroundColor = [UIColor blackColor];
            //        [self.view addSubview:settingView];
            press = FALSE;
        }];
    }
    else
    {
    TRChangePasswordViewController *pushView = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"password"];
    [[self navigationController ]pushViewController:pushView animated:YES];
    }
}

-(void)sign_out
{
    DebugLog(@"SIGN OUT");
    
    [self startLoading:self.view];
    
         
         NSString *urlString1 =[NSString stringWithFormat:@"http://esolz.co.in/lab3/tipright/index.php/login_ios/logout?id=%@",[[NSUserDefaults standardUserDefaults] valueForKey:UDUSERID]];
         
         DebugLog(@"requests url: %@",urlString1);
         
         NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
         
         NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
         NSString *error_msg;
         
         if (signeddataURL1 != nil)
         {
             NSError *error=nil;
             
             json = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                     
                                                    options:kNilOptions
                     
                                                      error:&error];
             DebugLog(@"json returns: %@",json);
             
             error_msg= [[json objectForKey:@"response"] objectForKey:@"status"];
             DebugLog(@"err  %@",[[json objectForKey:@"response"] objectForKey:@"status"]);
             if ([error_msg isEqualToString:@"Success"])
             {
                 [self stopLoading];
                 
                 
                 [[NSUserDefaults standardUserDefaults]setValue:[json valueForKey:@"userid"] forKey:UDUSERID];
                 
                         [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"keep"];
                 
                 [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"msgUsers"];
                 
                 
                 
                 UIViewController *Login = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"Login"];
                 [[self navigationController ]pushViewController:Login animated:YES];
                 
             }
             else
             {
                 [self stopLoading];
                 
                 alert = [[UIAlertView alloc] initWithTitle:error_msg
                                                    message:Nil
                                                   delegate:self
                                          cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                 [alert show];
             }
         }
         else
         {
             [self stopLoading];
             
             alert = [[UIAlertView alloc] initWithTitle:@"error in server connection!"
                                                message:Nil
                                               delegate:self
                                      cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
             [alert show];
         }
         
         
    
    
    //@"http://www.esolz.co.in/lab3/tipright/index.php/login_ios?email=pronoy.sarkar@esolzmail.com&password=123456"
    
    
    
    
    //http://esolz.co.in/lab3/tipright/index.php/login_ios/logout?id=123
    
}

-(void)notifcationsClicked
{
     [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"0"] forKey:@"notificationCount"];
    
    TRNotificationsViewController *pushView = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"TRnotification"];
    [[self navigationController ]pushViewController:pushView animated:YES];
    
}

-(void)homeClicked
{
    
    TRHomeViewController *pushView = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"Home"];
    [[self navigationController ]pushViewController:pushView animated:YES];
    
}

-(void)searchClicked
{
    
    TRSearchViewController *pushView = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"TRSearch"];
    [[self navigationController ]pushViewController:pushView animated:YES];
    
}
-(void)messageClicked
{
     [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"0"] forKey:@"msgCount"];
    
    
    TRMessageListViewController *pushView = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"TRmessageList"];
    [[self navigationController ]pushViewController:pushView animated:YES];
    
    
}




-(void)Rotate
{
    DebugLog(@"ROTATION:%f",self.view.frame.size.height);
    
    
    
    if (!rotate) {
        
        
        if([self isKindOfClass:[TROwnProfileViewController class]])
        {
            
            TROwnProfileViewController *trView=[[TROwnProfileViewController alloc]init];
            
            [trView.contentTable setUserInteractionEnabled:NO];
            
            
        }
        
        
        rotate = TRUE;
        [UIView animateWithDuration:.15 animations:^{
            plus.transform = CGAffineTransformMakeRotation(M_PI/4);
        }];
        
        blackView = [[UIView alloc] init];
        blackView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-56);
        blackView.backgroundColor = [UIColor blackColor];
        blackView.alpha = 0.75f;
       
        [self.view addSubview:blackView];
        [self.view bringSubviewToFront:footerView];
        
        ball_img = [[UIImageView alloc] init];
        ball_img.image = [UIImage imageNamed:@"icon3"];
        ball_img.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height-57, 0, 0);
        [ball_img setTag:3];
        [ball_img.layer setZPosition:99];
        
        [self.view addSubview:ball_img];
        
        UITapGestureRecognizer *ballRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tipClicked:)];
        [ballRecognizer setNumberOfTouchesRequired:1];
        [ball_img setUserInteractionEnabled:YES];
        [ball_img addGestureRecognizer:ballRecognizer];
        
        football_img = [[UIImageView alloc] init];
        football_img.image = [UIImage imageNamed:@"icon2"];
        football_img.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height-57, 0, 0);
            [football_img setTag:2];
          [football_img.layer setZPosition:99];
        [self.view addSubview:football_img];
        
        UITapGestureRecognizer *footballRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tipClicked:)];
        [footballRecognizer setNumberOfTouchesRequired:1];
        [football_img setUserInteractionEnabled:YES];
        [football_img addGestureRecognizer:footballRecognizer];
        
        horse_img = [[UIImageView alloc] init];
        horse_img.image = [UIImage imageNamed:@"icon1-1"];
        horse_img.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height-57, 0, 0);
               [horse_img setTag:1];
        [self.view addSubview:horse_img];
       [horse_img.layer setZPosition:99];
        
        UITapGestureRecognizer *horseRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tipClicked:)];
        [horseRecognizer setNumberOfTouchesRequired:1];
        [horse_img setUserInteractionEnabled:YES];
        [horse_img addGestureRecognizer:horseRecognizer];
        
        gloves_img = [[UIImageView alloc] init];
        gloves_img.image = [UIImage imageNamed:@"icon4"];
        gloves_img.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height-57, 0, 0);
               [gloves_img setTag:4];
          [gloves_img.layer setZPosition:99];
        [self.view addSubview:gloves_img];
        
        UITapGestureRecognizer *glovesRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tipClicked:)];
        [glovesRecognizer setNumberOfTouchesRequired:1];
        [gloves_img setUserInteractionEnabled:YES];
        [gloves_img addGestureRecognizer:glovesRecognizer];
        
        tip_img = [[UIImageView alloc] init];
        tip_img.image = [UIImage imageNamed:@"icon5"];
        tip_img.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height-57, 0, 0);
        [tip_img setTag:5];
        [self.view addSubview:tip_img];
         [tip_img.layer setZPosition:99];
        
        UITapGestureRecognizer *TapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tipClicked:)];
        [TapRecognizer setNumberOfTouchesRequired:1];
        [tip_img setUserInteractionEnabled:YES];
        [tip_img addGestureRecognizer:TapRecognizer];
        
        
        //
        //        tipClick=[[UIButton alloc ]initWithFrame:CGRectMake(tip_img.frame.origin.x-5, tip_img.frame.origin.y-5, tip_img.frame.size.width+10, tip_img.frame.size.height+10)];
        //        [tipClick addTarget:self action:@selector(tipClicked) forControlEvents:UIControlEventTouchUpInside];
        //        [self.view addSubview:tipClick];
        

        
        
        [UIView animateWithDuration:0.15f animations:^{
            
            ball_img.frame = CGRectMake(self.view.frame.size.width/2-15, self.view.frame.size.height-170, 45, 45);
            CABasicAnimation *theAnimation;
            theAnimation=[CABasicAnimation animationWithKeyPath:@"transform.translation.y"];///use transform
            theAnimation.duration=0.1;
            theAnimation.repeatCount=1;
            theAnimation.autoreverses=YES;
            theAnimation.fromValue=[NSNumber numberWithFloat:1.0];
            theAnimation.toValue=[NSNumber numberWithFloat:-7];
            [ball_img.layer addAnimation:theAnimation forKey:@"animateTranslation"];
            
            
            football_img.frame = CGRectMake(self.view.frame.size.width/2-76, self.view.frame.size.height-152, 45, 45);
            
            [football_img.layer addAnimation:theAnimation forKey:@"animateTranslation"];
            
            horse_img.frame = CGRectMake(self.view.frame.size.width/2-130, self.view.frame.size.height-107, 45, 45);
            [horse_img.layer addAnimation:theAnimation forKey:@"animateTranslation"];
            gloves_img.frame = CGRectMake(self.view.frame.size.width/2+46, self.view.frame.size.height-152, 45, 45);
            [gloves_img.layer addAnimation:theAnimation forKey:@"animateTranslation"];
            tip_img.frame = CGRectMake(self.view.frame.size.width/2+90, self.view.frame.size.height-107, 45, 45);
            [tip_img.layer addAnimation:theAnimation forKey:@"animateTranslation"];
            tipClick.frame = CGRectMake(self.view.frame.size.width/2+90, self.view.frame.size.height-107, 45, 45);
            [tip_img.layer addAnimation:theAnimation forKey:@"animateTranslation"];
            
            
        }];
        
        
    }
    else
    {
        rotate = FALSE;
        
        [UIView animateWithDuration:0.15 animations:^{
            plus.transform = CGAffineTransformMakeRotation(0);
        }];
        
        [UIView animateWithDuration:0.15f animations:^{
            
            ball_img.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height-57, 0, 0);
            football_img.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height-57, 0, 0);
            horse_img.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height-57, 0, 0);
            gloves_img.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height-57, 0, 0);
            tip_img.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height-57, 0, 0);
        }];
        
        [blackView removeFromSuperview];
        
        if([self isKindOfClass:[TROwnProfileViewController class]])
        {
            
            TROwnProfileViewController *trView=[[TROwnProfileViewController alloc]init];
            
            [trView.contentTable setUserInteractionEnabled:YES];
            
            
        }
        
    }
    
}


-(void)tipClicked:(UITapGestureRecognizer *)recognizer
{
    
    rotate = FALSE;
    
    [UIView animateWithDuration:0.15 animations:^{
        plus.transform = CGAffineTransformMakeRotation(0);
    }];
    
    [UIView animateWithDuration:0.15f animations:^{
        
        ball_img.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height-57, 0, 0);
        football_img.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height-57, 0, 0);
        horse_img.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height-57, 0, 0);
        gloves_img.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height-57, 0, 0);
        tip_img.frame = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height-57, 0, 0);
    }];
    
    [blackView removeFromSuperview];

    
    
    TRTipViewController *pushView = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"TRTip"];
    
    pushView.tipTypeSelected=(int)recognizer.view.tag;
 [[self navigationController ]pushViewController:pushView animated:YES];
    
    

}



-(UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
