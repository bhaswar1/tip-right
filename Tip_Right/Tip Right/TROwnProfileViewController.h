//
//  TROwnProfileViewController.h
//  Tip Right
//
//  Created by intel on 12/06/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import "TRGlobalMethods.h"
#import "TRProfileCellTableViewCell.h"

@interface TROwnProfileViewController : TRGlobalMethods
@property (strong, nonatomic) IBOutlet UIScrollView *mainScroll;
@property (strong, nonatomic) IBOutlet UIImageView *coverView;
@property (strong, nonatomic) IBOutlet UIImageView *profilePic;
@property (strong, nonatomic) IBOutlet UILabel *profileName;
@property (strong, nonatomic) IBOutlet UILabel *dummyText;
@property (strong, nonatomic) IBOutlet UILabel *bet_count;
@property (strong, nonatomic) IBOutlet UILabel *post_count;
@property (strong, nonatomic) IBOutlet UILabel *followers_count;
@property (strong, nonatomic) IBOutlet UILabel *following_count;
@property (strong, nonatomic) IBOutlet UILabel *betsLabel;
@property (strong, nonatomic) IBOutlet UILabel *postLabel;
@property (strong, nonatomic) IBOutlet UILabel *followLabel;
@property (strong, nonatomic) IBOutlet UILabel *followingLabel;
@property (strong, nonatomic) IBOutlet UILabel *bets;
@property (strong, nonatomic) IBOutlet UIButton *message_btn;
@property (strong, nonatomic) IBOutlet UIButton *follw_unfollw;

@property (strong, nonatomic) IBOutlet UITableView *contentTable;
@property (nonatomic, assign) BOOL search;
@property (nonatomic, assign) BOOL isfromOther;
@property (nonatomic)int privateCheck,FollowCheck;
@property(nonatomic,retain) NSString *userId, *follow_text;
@property(nonatomic)BOOL isFromRequest;


@end
