//
//  TREdit_ProfileViewController.h
//  Tip Right
//
//  Created by intel on 15/06/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import "TRGlobalMethods.h"

@interface TREdit_ProfileViewController : TRGlobalMethods<UIAlertViewDelegate>
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIImageView *coverPic;
@property (strong, nonatomic) IBOutlet UIImageView *profile_pic;

@end
