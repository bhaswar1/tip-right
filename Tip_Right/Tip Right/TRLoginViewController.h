//
//  TRLoginViewController.h
//  Tip Right
//
//  Created by santanu on 30/05/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import "TRGlobalMethods.h"

@interface TRLoginViewController : TRGlobalMethods
{
    
    IBOutlet UIScrollView *scrollView;
    __weak IBOutlet UITextField *passwordField;
    IBOutlet UITextField *emailField;
    
    IBOutlet UIImageView *keepMe;
    IBOutlet UIButton *keepMe_btn;
    IBOutlet UIButton *Sign_up;
    IBOutlet UIButton *LogIn_btn;
}

-(IBAction)keep;

@end
