//
//  AppDelegate.h
//  Tip Right
//
//  Created by intel on 29/05/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Pusher/Pusher.h>
#import "TRGlobalMethods.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate,PTPusherDelegate,UINavigationControllerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) NSString *userid;
@property (nonatomic, strong) PTPusher *pusherClient;
@property (nonatomic, strong) NSString *PUSHER_API_KEY, *PUSHER_CHANNEL, *PUSHER_EVENT;
@property (strong,nonatomic) UINavigationController *navigationController;
@property (nonatomic,retain) NSString *pushMsg;
-(void) callAfterSixtySecond;
@end

