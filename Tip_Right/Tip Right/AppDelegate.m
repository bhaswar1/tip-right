//
//  AppDelegate.m
//  Tip Right
//
//  Created by intel on 29/05/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import "AppDelegate.h"
#import "TRGlobalHeader.h"

@interface AppDelegate ()

@end

@implementation AppDelegate
@synthesize pushMsg;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    UIUserNotificationSettings *notificationSettings=[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    // Override point for customization after application launch.
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
           // [[NSUserDefaults standardUserDefaults]setValue:[NSString stringWithFormat:@"%d",0] forKey:@"msgCount"];
    
    
    _PUSHER_API_KEY = @"c64b27a7e666678cd26d";
    _PUSHER_CHANNEL = @"test_channel";
    _PUSHER_EVENT = @"my_event";
    
    
    _pusherClient = [PTPusher pusherWithKey:_PUSHER_API_KEY delegate:self encrypted:YES];
    
    // connect to pusher
    
    [self.pusherClient connect];
    
    // Declare the pusher channel to subscribe
    
    PTPusherChannel *channel   = [self.pusherClient subscribeToChannelNamed:_PUSHER_CHANNEL];
    
    // bind the pusher channel
    
    [channel bindToEventNamed:_PUSHER_EVENT handleWithBlock:^(PTPusherEvent *channelEvent) {
        
        DebugLog(@"CHANNEL EVENT:%@",channelEvent);
        
        
        NSDictionary*pushdict=(NSDictionary *)channelEvent.data;
        
      if([[pushdict objectForKey:@"notify_type"] isEqualToString:@"message"])
      {
      
      
          self.navigationController= (UINavigationController *)self.window.rootViewController;
          
          UIViewController* topMostController = self.navigationController.visibleViewController;
          
          NSString *class =[NSString stringWithFormat:@"%@",NSStringFromClass([topMostController class])];
          
          if ([class isEqualToString:@"TRMessageDetailsViewController"] ){

              
              DebugLog(@"in chat");
              
              
              
              [[NSUserDefaults standardUserDefaults]setObject:pushdict forKey:@"pushDict"];
              
             
              
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Received_Msg" object:nil];
          }else{
          
              
                  DebugLog(@"outside chat");
              
           //   TRGlobalMethods *gc=[[TRGlobalMethods alloc]init];
              
                if (![class isEqualToString:@"TRMessageListViewController"] ){
              
              
              int count;
              
              if ([[NSUserDefaults standardUserDefaults]valueForKey:@"msgCount"]) {
                
                 count=[[[NSUserDefaults standardUserDefaults]valueForKey:@"msgCount"] intValue];
                  
                  count++;
              }
              else{
                count=1;
                  
              }
              
              
              [[NSUserDefaults standardUserDefaults]setValue:[NSString stringWithFormat:@"%d",count] forKey:@"msgCount"];
            
                    
            DebugLog( @" msg count is:%@", [[NSUserDefaults standardUserDefaults]valueForKey:@"msgCount"]);
              
                }
              
              NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
              
              
              if([[NSUserDefaults standardUserDefaults]objectForKey:@"msgUsers"])
              {
                  
             dict=[[[NSUserDefaults standardUserDefaults]objectForKey:@"msgUsers"]mutableCopy];
                  
                  
                  BOOL found=false;
                  
                  NSArray* keys = [dict allKeys];
                  
                  for(NSString* key in keys) {
                      
                      if([key isEqualToString:[pushdict objectForKey:@"from_uid"]] )
                      {
                          
                          found=true;
                          
                          
                          int count=[[dict objectForKey:key]intValue];
                          
                          count++;
                          
                          [dict setObject:[NSString stringWithFormat:@"%d",count] forKey:key];
                          
                          
                          
                      }

                  }
                  
                  if (!found) {
                      
                      [dict setObject: [NSString stringWithFormat:@"1"]  forKey:[pushdict objectForKey:@"from_uid"] ];
                  }
                  
                  [[NSUserDefaults standardUserDefaults]setObject:dict forKey:@"msgUsers"];
                
         
              }else
              {
                
                  
                  [dict setObject: [NSString stringWithFormat:@"1"]  forKey:[pushdict objectForKey:@"from_uid"] ];
                  
                  [[NSUserDefaults standardUserDefaults]setObject:dict forKey:@"msgUsers"];
                  
              }
              
             DebugLog( @" msgUsers is:%@", [[NSUserDefaults standardUserDefaults]valueForKey:@"msgUsers"]);
             
           
              
                   if ([class isEqualToString:@"TRMessageListViewController"] ){
              
              
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Received_MsgTable" object:nil];
                   }
                   else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Received_MsgOutside" object:nil];
                   }
              
             // [gc.message_count setHidden:NO];
             // [gc.msgCount setHidden:NO];
              
             // gc.msgCount.text=[[NSUserDefaults standardUserDefaults]valueForKey:@"msgCount"];
              
              //[gc Footer:topMostController.view];
              
           
          
          }
          
      }
        else
            
            if(![[pushdict objectForKey:@"from_uid"]  isEqualToString:@"0"] && ![[pushdict objectForKey:@"from_uid"]  isEqualToString:@"<null>"] && [pushdict objectForKey:@"from_uid"] !=(id)[NSNull null] && ![[pushdict objectForKey:@"from_uid"] isKindOfClass:[NSNull class]] &&[pushdict objectForKey:@"from_uid"] !=nil)
                {
            
            if(![[pushdict objectForKey:@"from_uid"] isEqualToString:[[NSUserDefaults standardUserDefaults]valueForKey:UDUSERID]])
        {
            DebugLog(@"UserId:%@",[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:UDUSERID] ]);
            if([[pushdict objectForKey:@"notify_type"] isEqualToString:@"like"] || [[pushdict objectForKey:@"notify_type"] isEqualToString:@"comment"]||[[pushdict objectForKey:@"notify_type"] isEqualToString:@"follow_request_accept"] || [[pushdict objectForKey:@"notify_type"] isEqualToString:@"follow_request"] || [[pushdict objectForKey:@"notify_type"] isEqualToString:@"follow"])
        {
            self.navigationController= (UINavigationController *)self.window.rootViewController;
            
            UIViewController* topMostController = self.navigationController.visibleViewController;
            
            NSString *class =[NSString stringWithFormat:@"%@",NSStringFromClass([topMostController class])];
            
            DebugLog(@"CLASS:%@",class);
            
            if ([class isEqualToString:@"TRNotificationsViewController"] ){
                
                
              
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"Received_Notification" object:nil];
            }
            
           else
           {
            
            int notification_count;
            
            if ([[NSUserDefaults standardUserDefaults]valueForKey:@"notificationCount"]) {
                
                notification_count=[[[NSUserDefaults standardUserDefaults]valueForKey:@"notificationCount"] intValue];
                
                notification_count++;
            }
            else{
                notification_count = 1;
                
            }
            
            [[NSUserDefaults standardUserDefaults]setValue:[NSString stringWithFormat:@"%d",notification_count] forKey:@"notificationCount"];
            
            DebugLog( @" noti count is:%@", [[NSUserDefaults standardUserDefaults]valueForKey:@"notificationCount"]);
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Received_NotificationOutside" object:nil];
           }
            
        }
        
        }
        
                }
        
    }];
    
    
    return YES;
}

-(void) callAfterSixtySecond
{
    
    
    [[NSOperationQueue new] addOperationWithBlock:^{
        
        DebugLog(@"URL FIRED");

        
            NSString *urlString=[NSString stringWithFormat:@"%@/%@/profile_ios/view_user?user_id=%@&loggedin_id=%@",GLOBALAPI,INDEX,_userid,[[NSUserDefaults standardUserDefaults] valueForKey:UDUSERID]];
        
        DebugLog(@"profile url:%@",urlString);
        
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSDictionary *result=[[NSDictionary alloc]init];
        
        NSData *urlData1=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        if(urlData1==nil)
        {
            //[[[UIAlertView alloc]initWithTitle:nil message:@"Error in Server Connection" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
            
        }
        else{
            
            result=[NSJSONSerialization JSONObjectWithData:urlData1 options:kNilOptions error:nil];
            // DebugLog(@"Profile result is:%@",result);
            
            
            if([[result valueForKey:@"status"]isEqualToString:@"Error" ])
            {
                
               // [[[UIAlertView alloc]initWithTitle:@"Sorry!!!" message:@"ERROR" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                
            }
            else{
                
                NSDictionary *response = [result objectForKey:@"response"];
                
                [[NSUserDefaults standardUserDefaults] setObject:[response objectForKey:@"background_img"] forKey:@"cover_pic"];
                
                [[NSUserDefaults standardUserDefaults] setObject:[response objectForKey:@"bio"] forKey:@"bio"];
                
                [[NSUserDefaults standardUserDefaults] setObject:[response objectForKey:@"email"] forKey:@"email"];
                
                
                [[NSUserDefaults standardUserDefaults] setObject:[response objectForKey:@"first_name"] forKey:@"first_name"];
                [[NSUserDefaults standardUserDefaults] setObject:[response objectForKey:@"last_name"] forKey:@"last_name"];
                
                [[NSUserDefaults standardUserDefaults] setObject:[response objectForKey:@"profileimage"] forKey:@"profile_pic"];
                
                
                [[NSUserDefaults standardUserDefaults] setObject:[response objectForKey:@"sex"] forKey:@"sex"];
                
                
                [[NSUserDefaults standardUserDefaults] setObject:[response objectForKey:@"username"] forKey:@"username"];
                
                
                [[NSUserDefaults standardUserDefaults] setObject:[response objectForKey:@"website"] forKey:@"website"];
                
                  [[NSUserDefaults standardUserDefaults] setObject:[response objectForKey:@"private_status"] forKey:@"private"];
                
              //  [[NSUserDefaults standardUserDefaults]setValue:[response valueForKey:@"userid"] forKey:UDUSERID];
                
            }
            
        }
        
    }];
    
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:120.0f target:self
                                                           selector:@selector(callAfterSixtySecond)
                                                           userInfo:nil
                                                            repeats:NO];
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

//- (void)applicationDidEnterBackground:(UIApplication *)application {
//    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
//    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
//}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)devToken {
    NSString *deviceToken = [[[[devToken description]
                               stringByReplacingOccurrencesOfString:@"<"withString:@""]
                              stringByReplacingOccurrencesOfString:@">" withString:@""]
                             stringByReplacingOccurrencesOfString: @" " withString: @""];
    if ([deviceToken length] == 0 || [deviceToken isKindOfClass:[NSNull class]])
    {
        deviceToken = @"123456";
    }
    DebugLog(@"device token:%@",deviceToken);
    [[NSUserDefaults standardUserDefaults] setObject:deviceToken forKey:DEVICETOKEN];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err {
    NSString *str = [NSString stringWithFormat: @"Error: %@", err];
    DebugLog(@"%@",str);
     [[NSUserDefaults standardUserDefaults] setObject:@"1234568" forKey:DEVICETOKEN];

}


@end
