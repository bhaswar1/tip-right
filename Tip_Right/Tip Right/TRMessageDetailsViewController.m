//
//  TRMessageDetailsViewController.m
//  Tip Right
//
//  Created by santanu on 30/06/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import "TRMessageDetailsViewController.h"
#import "TRMessageDetailsTableViewCell.h"
#import "TRMessageDetailsTwoTableViewCell.h"
#import "AppDelegate.h"


@interface TRMessageDetailsViewController ()<UITableViewDelegate,UITableViewDataSource,UITextViewDelegate>
{

    __weak IBOutlet UITableView *messageTable;
    NSOperationQueue *operationQueue;
    NSDictionary *result;
    NSMutableArray *response;
    
    int page,dataCount,totalCount;
    NSMutableArray *messageArr;

    __weak IBOutlet UIView *containerView;
    __weak IBOutlet UITextView *messageField;
    
    CGRect fieldRect;
    CGRect containerRect;
    BOOL sent,isloaded;
}
@end

@implementation TRMessageDetailsViewController
@synthesize senderID;



-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appendAfterPush) name:@"Received_Msg" object:nil];
    
}

-(void)appendAfterPush
{
    
   // AppDelegate *mainDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

 
    NSDictionary *pushDict=[[NSUserDefaults standardUserDefaults]objectForKey:@"pushDict"];
  
    
        DebugLog(@"dict is%@", pushDict);
    if([[pushDict objectForKey:@"from_uid"]isEqualToString:senderID])
    {
    
        
        page=0;
        sent=YES;
        
        dataCount=0;
        
        messageArr=[[NSMutableArray alloc]init];
        
        [self loadData];
        
      
    }
    else
    {
    
        if(![[pushDict objectForKey:@"from_uid"]isEqualToString:[[NSUserDefaults standardUserDefaults] valueForKey:UDUSERID]])
        {
        
        int count=[[[NSUserDefaults standardUserDefaults]valueForKey:@"msgCount"] intValue];
        
        count++;
        
        [[NSUserDefaults standardUserDefaults]setValue:[NSString stringWithFormat:@"%d",count] forKey:@"msgCount"];
        
        DebugLog( @"count is:%@", [[NSUserDefaults standardUserDefaults]valueForKey:@"msgCount"]);
        
        [self Footer:self.view];
        
    
    }
    
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
   
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWillShow:)
     
                                                 name:UIKeyboardWillShowNotification
     
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWillHide:)
     
                                                 name:UIKeyboardWillHideNotification
     
                                               object:nil];
    
    page=0;
    sent=NO;
    isloaded=YES;
    dataCount=0;
    
        messageArr=[[NSMutableArray alloc]init];
    
    self.tipView = YES;
    [self Header:self.view];
    [self Footer:self.view];
    
    messageField.text = @"Type something";
    messageField.textColor = [UIColor lightGrayColor];

    
    [messageField setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    operationQueue=[[NSOperationQueue alloc]init];
    
     [self startLoading:self.view];
    
        [self loadData];
    
    // Do any additional setup after loading the view.
}


-(void) keyboardWillShow:(NSNotification *)note{
    
    DebugLog(@"key board jokhn show hobeeeee");
    
    fieldRect=messageTable.frame;
    containerRect=containerView.frame;
    
    
    messageTable.frame = CGRectMake(0, messageTable.frame.origin.y, [[UIScreen mainScreen] bounds].size.width, [UIScreen mainScreen].bounds.size.height-containerView.frame.size.height-300);
    
    [messageTable scrollRectToVisible:CGRectMake(0, messageTable.contentSize.height - messageTable.frame.size.height, messageTable.frame.size.width, messageTable.frame.size.height) animated:NO];
    
    CGRect keyboardBounds;
    
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    // Need to translate the bounds to account for rotation.
    
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    
    // get a rect for the textView frame
    
    CGRect containerFrame = containerView.frame;
    
    containerFrame.origin.y = self.view.bounds.size.height - (keyboardBounds.size.height + containerFrame.size.height);
    
    // animations settings
    
    [UIView beginAnimations:nil context:NULL];
    
    [UIView setAnimationBeginsFromCurrentState:YES];
    
    [UIView setAnimationDuration:[duration doubleValue]];
    
    [UIView setAnimationCurve:[curve intValue]];
    
    DebugLog(@"height:%f and ypos:%f",containerFrame.size.height,containerFrame.origin.y);
    
    // set views with new info
    
    containerView.frame = containerFrame;
    
    // commit animations
    
    [UIView commitAnimations];
    
}

-(void) keyboardWillHide:(NSNotification *)note{
    
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    
    // animations settings
    
    [UIView beginAnimations:nil context:NULL];
    
    [UIView setAnimationBeginsFromCurrentState:YES];
    
    [UIView setAnimationDuration:[duration doubleValue]];
    
    [UIView setAnimationCurve:[curve intValue]];
    
    // set views with new info
    
    containerView.frame = containerRect;
    
    //edited by santanu mondal
    
    messageTable.frame = fieldRect;
    
    
    // commit animations
    
    [UIView commitAnimations];
    
}

-(void)loadData

{
   // http://www.esolz.co.in/lab3/tipright/index.php/message_ios/conversation?sender=141&receiver=123&page=0&limit=20
    

    
    [operationQueue addOperationWithBlock:^{
        
        NSString *urlString=[NSString stringWithFormat:@"%@/%@/%@/conversation?sender=%@&receiver=%@&page=%d&limit=6",GLOBALAPI,INDEX,MESSAGELIST,[[NSUserDefaults standardUserDefaults]valueForKey:UDUSERID ],senderID,page];
        
        DebugLog(@"url:%@",urlString);
        
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        result=[[NSDictionary alloc]init];
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        if(urlData==nil)
        {
            
            [[[UIAlertView alloc]initWithTitle:@"Error!!!" message:@"error in internet Connection" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
            
        }
        else{
            
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                

                
                [self stopLoading];
                
                result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                DebugLog(@"message result is:%@",result);
                
                if([[result valueForKey:@"status"]isEqualToString:@"Error" ])
                {
                    
                  
                    
                }
                else{
                    
                    response = [result objectForKey:@"data"];
                    DebugLog(@"RESPONSE VALUE:%@",response);
                    DebugLog(@"%lu",(unsigned long)[response count]);
                    
                    NSArray* reversed = [[response reverseObjectEnumerator] allObjects];
                    
                    for(NSDictionary *dict in reversed)
                    {
                        
                        [messageArr insertObject:dict atIndex:0];
                        
                       // [messageArr addObject:dict];
                        
                        dataCount++;
                    
                    }
                    
                    totalCount=[[result valueForKey:@"total"]intValue ];
                    
                    
                    messageTable.delegate=self;
                    messageTable.dataSource=self;
                    [messageTable setUserInteractionEnabled:YES];
                    [messageTable setHidden:NO];
                    [containerView setHidden:NO];
        
                    
                    
                    
                    [messageTable reloadData];
                    
                    if(sent)
                    {[messageTable scrollRectToVisible:CGRectMake(0, messageTable.contentSize.height - messageTable.frame.size.height, messageTable.frame.size.width, messageTable.frame.size.height) animated:YES];
                        
                        sent=NO;
                        
                    }
                    
                    if(isloaded)
                    {[messageTable scrollRectToVisible:CGRectMake(0, messageTable.contentSize.height - messageTable.frame.size.height, messageTable.frame.size.width, messageTable.frame.size.height) animated:YES];
                        
                        isloaded=NO;
                        
                    }
                    
                    
                }
            }];
            
        }
        
    }];
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    
    //NSString *str = [[messageArr objectAtIndex:indexPath.row] valueForKey:@"message"];

    NSInteger MAX_HEIGHT = 2000;

    
    
    UILabel *txtLbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 200.0/320.0*self.view.frame.size.width, MAX_HEIGHT)];
    
    txtLbl.text= [[messageArr objectAtIndex:indexPath.row] valueForKey:@"message"];
    txtLbl.numberOfLines = 0;
    
    
    if(IsIphone6plus)
        txtLbl.font=[txtLbl.font fontWithSize:16];
    else if(IsIphone6)
        txtLbl.font=[txtLbl.font fontWithSize:13];
    else
        txtLbl.font=[txtLbl.font fontWithSize:10];
    
    
    
    //[cell2.userMsg sizeToFit];
    
    CGSize size = [ txtLbl.text sizeWithFont:txtLbl.font
                                  constrainedToSize:CGSizeMake(200.0/320.0*self.view.frame.size.width, MAXFLOAT)
                                      lineBreakMode:NSLineBreakByWordWrapping];
    CGRect  labelFrame =  txtLbl.frame;
    labelFrame.size.height = size.height;
    txtLbl.frame = labelFrame;
    
    
  
    
    if(txtLbl.frame.size.height>45.0/568.0*self.view.frame.size.height)
    {
        float extraSpace=txtLbl.frame.size.height/568.0*self.view.frame.size.height-(45.0/568.0*self.view.frame.size.height);
        
return extraSpace+(100.0/568.0*self.view.frame.size.height);
    }
    else{
     return 100.0/568.0*self.view.frame.size.height;
    }
       return 100.0/568.0*self.view.frame.size.height;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    DebugLog(@"RESPONSE:%ld",[messageArr count]);
    return [messageArr count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    
      NSString *  cellIdentifier1= @"OtherCell";
      NSString *  cellIdentifier2= @"UserCell";
    
        TRMessageDetailsTableViewCell *cell1=(TRMessageDetailsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier1];
    
        TRMessageDetailsTwoTableViewCell *cell2=(TRMessageDetailsTwoTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier2];
    
        if([[[messageArr objectAtIndex:indexPath.row] objectForKey:@"senderid"]intValue]==[[[NSUserDefaults standardUserDefaults]valueForKey:UDUSERID]intValue])
        {
            if (cell2 == nil)
            {
                DebugLog(@"CELL ALLOC wigth id %d",[[[messageArr objectAtIndex:indexPath.row] objectForKey:@"senderid"]intValue]);
                
                
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"extendedDesign" owner:self options:nil];
                
                
                cell2 = [nib objectAtIndex:2];
                
            }
            
            cell2.selectionStyle=UITableViewCellStyleDefault;
            [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
            
            
            UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 0.5)];/// change size as you need.
            separatorLineView.backgroundColor = [UIColor lightGrayColor];// you can also put image here
            [cell2.contentView addSubview:separatorLineView];
            
            UIImage *img=[UIImage imageNamed:@"noimages"];
            
            DebugLog(@"ami");
            
                [cell2.userImg sd_setImageWithURL:[NSURL URLWithString:[[messageArr objectAtIndex:indexPath.row] objectForKey:@"image"]] placeholderImage:img];
            
            
                cell2.userImg.layer.cornerRadius = cell2.userImg.frame.size.width/2;
                 cell2.userImg.clipsToBounds = YES;
            
//                CALayer *imageLayer = cell2.userImg.layer;
//                [imageLayer setCornerRadius:25/375.0*self.view.frame.size.width];
//                [imageLayer setMasksToBounds:YES];
//                [cell2.userImg.layer setMasksToBounds:YES];
            
            
                if(IsIphone6plus)
                    cell2.userName.font=[cell2.userName.font fontWithSize:13];
                else if(IsIphone6)
                    cell2.userName.font=[cell2.userName.font fontWithSize:10];
                else
                    cell2.userName.font=[cell2.userName.font fontWithSize:8];
            
            
                cell2.userName.text = [[messageArr objectAtIndex:indexPath.row] valueForKey:@"name"];
            
                    if(IsIphone6plus)
                        cell2.userMsg.font=[cell2.userMsg.font fontWithSize:16];
                    else if(IsIphone6)
                        cell2.userMsg.font=[cell2.userMsg.font fontWithSize:13];
                    else
                        cell2.userMsg.font=[cell2.userMsg.font fontWithSize:10];
                    
                    
                    cell2.userMsg.text = [[messageArr objectAtIndex:indexPath.row] valueForKey:@"message"];
            cell2.userMsg.numberOfLines = 0;
            
        
            
      //[cell2.userMsg sizeToFit];
        
            CGSize size = [ cell2.userMsg.text sizeWithFont:cell2.userMsg.font
                                 constrainedToSize:CGSizeMake(cell2.userMsg.frame.size.width, MAXFLOAT)
                                     lineBreakMode:NSLineBreakByWordWrapping];
          CGRect  labelFrame =  cell2.userMsg.frame;
            
            labelFrame.size.height = size.height;
            cell2.userMsg.frame = labelFrame;
            
            
            
            NSDateFormatter *formatter2 = [[NSDateFormatter alloc]init];
            [formatter2 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
             [formatter2 setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
         
            
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
            [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
           // [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT+5:30"]];
           [formatter setTimeZone:[NSTimeZone localTimeZone]];
            
            DebugLog(@"SERVER TIME:%@",[[messageArr objectAtIndex:indexPath.row] valueForKey:@"date"]);
            
            NSDate *date = [formatter2 dateFromString:[NSString stringWithFormat:@"%@",[[messageArr objectAtIndex:indexPath.row] valueForKey:@"date"]]];
            
            NSString *deviceTime=[formatter stringFromDate:date];
            
               DebugLog(@"Device TIME:%@",deviceTime);

            
            
            
            cell2.msgTime.text=deviceTime;
            
            
            DebugLog(@"cell height: %f",cell2.frame.size.height);
            
            
            
            return cell2;
            
 
       }
        else{
       
            
            if (cell1 == nil)
            {
                DebugLog(@"CELL ALLOC wigth id %d",[[[messageArr objectAtIndex:indexPath.row] objectForKey:@"senderid"]intValue]);
                
                
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"extendedDesign" owner:self options:nil];
                
               
                    cell1 = [nib objectAtIndex:1];
               
            }
            
            cell1.selectionStyle=UITableViewCellStyleDefault;
            [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
            
            
            UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 0.5)];/// change size as you need.
            separatorLineView.backgroundColor = [UIColor lightGrayColor];// you can also put image here
            [cell1.contentView addSubview:separatorLineView];
            
            UIImage *img=[UIImage imageNamed:@"noimages"];
            DebugLog(@"ami noi");
            
            [cell1.otherUserImg sd_setImageWithURL:[NSURL URLWithString:[[messageArr objectAtIndex:indexPath.row] objectForKey:@"image"]] placeholderImage:img];
            
            
            //cell.userImg.layer.cornerRadius = cell.userImg.frame.size.width/2;
            // cell.userImg.clipsToBounds = YES;
            
            CALayer *imageLayer = cell1.otherUserImg.layer;
            [imageLayer setCornerRadius:25/375.0*self.view.frame.size.width];
            [imageLayer setMasksToBounds:YES];
            [cell1.otherUserImg.layer setMasksToBounds:YES];
            
            
            if(IsIphone6plus)
                cell1.otherUserName.font=[cell1.otherUserName.font fontWithSize:13];
            else if(IsIphone6)
                cell1.otherUserName.font=[cell1.otherUserName.font fontWithSize:10];
            else
                cell1.otherUserName.font=[cell1.otherUserName.font fontWithSize:8];
            
            
            cell1.otherUserName.text = [[messageArr objectAtIndex:indexPath.row] valueForKey:@"name"];
            
            if(IsIphone6plus)
                cell1.otherUserMsg.font=[cell1.otherUserMsg.font fontWithSize:16];
            else if(IsIphone6)
                cell1.otherUserMsg.font=[cell1.otherUserMsg.font fontWithSize:13];
            else
                cell1.otherUserMsg.font=[cell1.otherUserMsg.font fontWithSize:10];
            
            
            cell1.otherUserMsg.text = [[messageArr objectAtIndex:indexPath.row] valueForKey:@"message"];
            cell1.otherUserMsg.numberOfLines = 0;
           // [cell1.otherUserMsg sizeToFit];
            
            CGSize size = [ cell1.otherUserMsg.text sizeWithFont:cell1.otherUserMsg.font
                                          constrainedToSize:CGSizeMake(cell1.otherUserMsg.frame.size.width, MAXFLOAT)
                                              lineBreakMode:NSLineBreakByWordWrapping];
            CGRect  labelFrame =  cell1.otherUserMsg.frame;
            labelFrame.size.height = size.height;
            cell1.otherUserMsg.frame = labelFrame;
            
            
            NSDateFormatter *formatter2 = [[NSDateFormatter alloc]init];
            [formatter2 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            [formatter2 setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
            [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        [formatter setTimeZone:[NSTimeZone localTimeZone]];
            
            DebugLog(@"SERVER TIME:%@",[[messageArr objectAtIndex:indexPath.row] valueForKey:@"date"]);
            
            NSDate *date = [formatter2 dateFromString:[NSString stringWithFormat:@"%@",[[messageArr objectAtIndex:indexPath.row] valueForKey:@"date"]]];
            
            NSString *deviceTime=[formatter stringFromDate:date];
            
            
            DebugLog(@"Device TIME:%@",deviceTime);
            
                       cell1.otherMsgTime.text=deviceTime;
            
            return cell1;
            
        }
    
    return cell1;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    textView.text = @"";
    textView.textColor = [UIColor blackColor];
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
   
    if([text isEqualToString:@"\n"])
    {    [textView resignFirstResponder];
    
        
        textView.text = [textView.text stringByTrimmingCharactersInSet:
                         [NSCharacterSet whitespaceCharacterSet]];
        
        if(textView.text.length>0)
        {
        [self sendMsg:textView.text];
        }
    }
    
   return YES;
    
}

-(void)textViewDidChange:(UITextView *)textView
{
    if(textView.text.length == 0){
        textView.textColor = [UIColor lightGrayColor];
        textView.text = @"Type something";
        [textView resignFirstResponder];
    }
    
}


-(void)sendMsg:(NSString *)str
{

    [messageField setText:@""];
    
    DebugLog(@"text is %@",str);
   // http://www.esolz.co.in/lab3/tipright/index.php/message_ios/insertmessage?messageto=123&messagefrom=140&message=testing%20message

    [self startLoading:self.view];
    [messageTable setUserInteractionEnabled:NO];
    
    
    [operationQueue addOperationWithBlock:^{
        
        NSString *urlString=[NSString stringWithFormat:@"%@/%@/%@/insertmessage?messageto=%@&messagefrom=%@&message=%@",GLOBALAPI,INDEX,MESSAGELIST,senderID,[[NSUserDefaults standardUserDefaults]valueForKey:UDUSERID ],str];
        
        DebugLog(@"url:%@",urlString);
        
        urlString =[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        result=[[NSDictionary alloc]init];
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        if(urlData==nil)
        {
            
            [[[UIAlertView alloc]initWithTitle:@"Error!!!" message:@"error in internet Connection" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
            
        }
        else{
            
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                
              
                
                result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                DebugLog(@"message result is:%@",result);
                
                if([[result valueForKey:@"status"]isEqualToString:@"Success" ])
                {
                    
                      // [self startLoading:messageTable];
                  
                    page=0;
                    sent=YES;
                    
                    dataCount=0;
                    
                    messageArr=[[NSMutableArray alloc]init];
                    
                    [self loadData];
                    
                    
                 
                    
                }
                else if([[result valueForKey:@"status"]isEqualToString:@"Error" ]){
                    
                    
                    [[[UIAlertView alloc]initWithTitle:@"Sorry!!!" message:[result valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                    
                }
           
            }];
             }
        }];

}


#pragma UIScrollViewDelegate

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{

    float scrollOffset = scrollView.contentOffset.y;
    
    DebugLog(@"scrollPos%f",scrollOffset);
    
    if (scrollOffset <0)
    {
        DebugLog(@"then we are at the top");

        
      DebugLog(@"total count is%d and current count is %d",totalCount,dataCount);
        
        if (dataCount<totalCount)
        {
            [messageTable setUserInteractionEnabled:NO];
            [self startLoading:self.view];
            
            page++;
            
            [self performSelector:@selector(loadData) withObject:nil];
        }
    }
    
}

-(void)goBack
{
    CATransition *Transition=[CATransition animation];
    [Transition setDuration:0.4f];
    //[Transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
    [Transition setType:kCATransitionPush];
    [Transition setSubtype:kCATransitionFromLeft];
    [[[[self navigationController] view] layer] addAnimation:Transition forKey:@"transition"];
    [self.navigationController popViewControllerAnimated:NO];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
