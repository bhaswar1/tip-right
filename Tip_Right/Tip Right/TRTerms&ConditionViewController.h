//
//  TRTerms&ConditionViewController.h
//  Tip Right
//
//  Created by intel on 17/07/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import "TRGlobalMethods.h"

@interface TRTerms_ConditionViewController : TRGlobalMethods

@property(nonatomic, assign)BOOL terms;
@property(nonatomic, assign)BOOL privacy;
@end
