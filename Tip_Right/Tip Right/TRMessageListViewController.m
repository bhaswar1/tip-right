//
//  TRMessageListViewController.m
//  Tip Right
//
//  Created by santanu on 30/06/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import "TRMessageListViewController.h"
#import "TRMessageLIstTableViewCell.h"
#import "TRMessageDetailsViewController.h"
@interface TRMessageListViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    NSOperationQueue *operationQueue;
    NSDictionary *result;
    NSMutableArray *response;

}
@property (weak, nonatomic) IBOutlet UITableView *UserTable;
@property (weak, nonatomic) IBOutlet UILabel *noMsgLbl;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScroll;


@end

@implementation TRMessageListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self Header:self.view];
    [self Footer:self.view];
    
    self.view.backgroundColor = [UIColor colorWithRed:(248.0f/255.0f) green:(247.0f/255.0f) blue:(243.0f/255.0f) alpha:1.0f];
    
    [_UserTable setDelegate:self];
    [_UserTable setDataSource:self];
   
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated 
{
   

        operationQueue=[[NSOperationQueue alloc]init];
    [self startLoading:self.view];
    
    [self loadData];
   
      [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeMsgCount) name:@"Received_MsgTable" object:nil];
   

}


-(void)changeMsgCount
{

    [self startLoading:self.view];
    
    [self loadData];

}


-(void)loadData

{
  // http://www.esolz.co.in/lab3/tipright/index.php/message_ios/friendlist?userid=123
  
    [operationQueue addOperationWithBlock:^{
    
    NSString *urlString=[NSString stringWithFormat:@"%@/%@/%@/friendlist?userid=%@",GLOBALAPI,INDEX,MESSAGELIST,[[NSUserDefaults standardUserDefaults]valueForKey:UDUSERID ]];
    
    DebugLog(@"url:%@",urlString);
    
    urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
    //urlString =[urlString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    result=[[NSDictionary alloc]init];
    
    NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
    
    if(urlData==nil)
    {
        
        [[[UIAlertView alloc]initWithTitle:@"Error!!!" message:@"error in internet Connection" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
        
    }
    else{
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
        
        [self stopLoading];
        
        result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
        DebugLog(@"Notification result is:%@",result);
        
        if([[result valueForKey:@"status"]isEqualToString:@"Error" ])
        {
            _noMsgLbl.text = [result valueForKey:@"message"];
            [_noMsgLbl setHidden:NO];
            
            
        }
        else{
            
            response = [result objectForKey:@"data"];
            DebugLog(@"RESPONSE VALUE:%@",response);
            DebugLog(@"Respnse Count:%lu",(unsigned long)[response count]);
            
            [_UserTable setUserInteractionEnabled:YES];
            [_UserTable setHidden:NO];
       

         
            [_UserTable setScrollEnabled:NO];
            

            CGRect tableRect=[_UserTable frame];
            
            tableRect.size.height=((72.0/568.0*self.view.frame.size.height)*[response count]);
            
            _UserTable.frame=tableRect;
            
            
            [_mainScroll setContentSize:CGSizeMake(_UserTable.frame.size.width,((72.0/568.0*self.view.frame.size.height)*[response count]))];
            
            
            [_UserTable reloadData];
            
        }
        }];
        
    }
    
    }];
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70.0/568.0*self.view.frame.size.height;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    DebugLog(@"RESPONSE:%ld",[response count]);
    return [response count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier=@"CellIdentifier";
    TRMessageLIstTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        DebugLog(@"CELL ALLOC");
        cell = [[TRMessageLIstTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
    }
    
    cell.selectionStyle=UITableViewCellStyleDefault;
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    
 
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"msgUsers"])
    {

     NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
             dict=[[NSUserDefaults standardUserDefaults]objectForKey:@"msgUsers"];
        
    
        NSArray* keys = [dict allKeys];
        
        for(NSString* key in keys) {
            
            if([key isEqualToString:[[response objectAtIndex:indexPath.row] objectForKey:@"senderid"]] )
            {
            
                [cell.messageLbl setText:[dict valueForKey:key]];
               
                
               [cell.countImage setHidden:NO];
                
              
                [cell setHighlighted:YES];
                
                
                
                
            }
           
           
        }
        
    }
    
    UILabel *separatorLine = [[UILabel alloc] init];
    separatorLine.frame = CGRectMake(0, 0, cell.frame.size.width, 1.0f);
    separatorLine.backgroundColor = [UIColor colorWithRed:(248.0f/255.0f) green:(247.0f/255.0f) blue:(243.0f/255.0f) alpha:1.0f];
    [cell.contentView addSubview:separatorLine];
    
    UIImage *img=[UIImage imageNamed:@"noimages"];
    
    [cell.userImg sd_setImageWithURL:[NSURL URLWithString:[[response objectAtIndex:indexPath.row] objectForKey:@"Profile-image"]] placeholderImage:img];
    
    
    cell.userImg.layer.cornerRadius = cell.userImg.frame.size.width/2;
    cell.userImg.clipsToBounds = YES;
    
//    CALayer *imageLayer = cell.userImg.layer;
//    [imageLayer setCornerRadius:25/375.0*self.view.frame.size.width];
//    [imageLayer setBorderWidth:0.1];
//    [imageLayer setMasksToBounds:YES];
//    [cell.userImg.layer setMasksToBounds:YES];

    
    if(IsIphone6plus)
        cell.userName.font=[cell.userName.font fontWithSize:17];
    else if(IsIphone6)
        cell.userName.font=[cell.userName.font fontWithSize:15];
    else
        cell.userName.font=[cell.userName.font fontWithSize:13];
    
    
    cell.userName.text = [[response objectAtIndex:indexPath.row] valueForKey:@"Name"];
    
    
 
//    
//    UILabel *separatorLine = [[UILabel alloc] init];
//    separatorLine.frame = CGRectMake(0, cell.frame.size.height-0.5f, cell.frame.size.width, 0.5f);
//    separatorLine.backgroundColor = [UIColor colorWithRed:(248.0f/255.0f) green:(247.0f/255.0f) blue:(243.0f/255.0f) alpha:1.0f];
//    [cell.contentView addSubview:separatorLine];
    
    
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    TRMessageDetailsViewController *pushView = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"TRmessageDetails"];
    
    //pushView.userId=[[NSUserDefaults standardUserDefaults]valueForKey:UDUSERID];
    
    
    DebugLog(@"MSG Id:%@",[response objectAtIndex:indexPath.row]);
   
    NSMutableDictionary *dict= [[[NSUserDefaults standardUserDefaults]objectForKey:@"msgUsers"]mutableCopy];
    
    NSArray* keys = [dict allKeys];
    
    for(NSString* key in keys) {
        
        if([key isEqualToString:[[response objectAtIndex:indexPath.row] objectForKey:@"senderid"]] )
        {
            
             [dict removeObjectForKey:[[response objectAtIndex:indexPath.row] objectForKey:@"senderid"]];
            
            
            TRMessageLIstTableViewCell *cell = (TRMessageLIstTableViewCell *)[_UserTable cellForRowAtIndexPath:indexPath];
            
            [cell.messageLbl setText:@""];
            
            
            [cell.countImage setHidden:YES];
            
    }
        
    }
    
    

    
    [[NSUserDefaults standardUserDefaults]setObject:dict forKey:@"msgUsers"];
    
    
    pushView.senderID=[NSString stringWithFormat:@"%@",[[response objectAtIndex:indexPath.row] objectForKey:@"senderid"]];
    [[self navigationController ]pushViewController:pushView animated:YES];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
