//
//  TRHomeTableViewCell.h
//  Tip Right
//
//  Created by santanu on 15/07/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TRHomeTableViewCell : UITableViewCell


@property(nonatomic) int followedBy;


@end
