//
//  TRFollowingListViewController.m
//  Tip Right
//
//  Created by intel on 10/07/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import "TRFollowingListViewController.h"
#import "TRGlobalHeader.h"
#import "UIImageView+WebCache.h"
#import "TROwnProfileViewController.h"

@interface TRFollowingListViewController ()<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>
{
    NSDictionary *result,*resultDict;
    NSOperationQueue *operationQueue;
    NSMutableArray *response,*response2;
    
    int page,dataCount,totalCount;
    NSMutableArray *followingArr;
    
    NSMutableArray *searchDisplayArr;
    NSDictionary *searchDict;
    NSString *searchText;
    
    NSArray *searchResult;
    BOOL search;
  
}
@property (weak, nonatomic) IBOutlet UILabel *noFollowLbl;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;

@end

@implementation TRFollowingListViewController
@synthesize userId;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    

    self.tipView = YES;
    [self Header:self.view];
    [self Footer:self.view];
    
    page=0;
    
    dataCount=0;
    
    followingArr=[[NSMutableArray alloc]init];
    
    operationQueue=[[NSOperationQueue alloc]init];
    
    _searchBar.delegate = self;
    _searchBar.showsCancelButton = YES;

    
}

-(void)viewDidAppear:(BOOL)animated
{
    response = [[NSMutableArray alloc] init];
     [self loadData];
}

-(void)loadData
{
    [self startLoading:self.view];
    
     [operationQueue addOperationWithBlock:^{
    NSString *urlString=[NSString stringWithFormat:@"%@/%@/follow_ios/userfollow?userid=%@&loggedinid=%@&page=%d&limit=4&searchparam=",GLOBALAPI,INDEX,userId,[[NSUserDefaults standardUserDefaults]valueForKey:UDUSERID ],page];
    
    
    urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
    DebugLog(@"url:%@",urlString);
    result=[[NSDictionary alloc]init];
    
    NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
    
    [[NSOperationQueue mainQueue]addOperationWithBlock:^{
        
        [self stopLoading];
        
    if(urlData == nil)
    {
        
        [[[UIAlertView alloc]initWithTitle:@"Error!!!" message:@"error in internet Connection" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
        
    }
    else{
        
        result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
        DebugLog(@"result is:%@",result);
        
        [_followingTable setUserInteractionEnabled:YES];
        
        if([[result valueForKey:@"status"]isEqualToString:@"Success"])
        {
            
            response=[result valueForKey:@"response"];
            
            DebugLog(@"Response:%@",response);
            
            dataCount+=[response count];
            
            totalCount=[[result valueForKey:@"total_count"]intValue];
            
            for(NSDictionary *rowDict in response)
            {
                [followingArr addObject:rowDict];
                
            }
            
            
            _followingTable.dataSource = self;
            _followingTable.delegate = self;
            _followingTable.hidden = NO;
            [_followingTable reloadData];
            
        }
        else{
            
    
            [_noFollowLbl setHidden:NO];
            
        }
        
    }
    
     }];
    
    }];
    
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 235.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (search) {
         return [searchDisplayArr count];
    }
    else
    {
    return [followingArr count];
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"cellIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    DebugLog(@"CELL ALLOC");
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor whiteColor];
    
    if (search) {
        
        DebugLog(@"SEARCH DATA IN CELL:%@",searchDisplayArr);
        
        UIImageView *profilePic = [[UIImageView alloc] init];
        profilePic.frame = CGRectMake(10, 10, 60, 60);
        profilePic.layer.cornerRadius = profilePic.frame.size.width/2;
        profilePic.clipsToBounds = YES;
       DebugLog(@"PROFILE IMAGE:%@",[[searchDisplayArr objectAtIndex:indexPath.row]objectForKey:@"profileimage"]);
        [profilePic sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[searchDisplayArr objectAtIndex:indexPath.row]objectForKey:@"profileimage"] ]] placeholderImage:[UIImage imageNamed:@"profile-pic-2"]];
        // profilePic.image = [UIImage imageNamed:@"own-profile"];
        
        [cell addSubview:profilePic];
        
        UILabel *profileName = [[UILabel alloc] init];
        profileName.frame = CGRectMake(profilePic.frame.origin.x+profilePic.frame.size.width+10, 10, 150, 20);
        profileName.backgroundColor = [UIColor clearColor];
        profileName.textColor = [UIColor blackColor];
        profileName.textAlignment = NSTextAlignmentLeft;
        profileName.font = [UIFont fontWithName:@"Helvetica Neue" size:15.0];
        profileName.text = [[searchDisplayArr objectAtIndex:indexPath.row] objectForKey:@"name"];
        [cell addSubview:profileName];
        
        UILabel *userName = [[UILabel alloc] init];
        userName.frame = CGRectMake(profilePic.frame.origin.x+profilePic.frame.size.width+10, 35, 150, 20);
        userName.backgroundColor = [UIColor clearColor];
        userName.textColor = [UIColor blackColor];
        userName.textAlignment = NSTextAlignmentLeft;
        userName.font = [UIFont fontWithName:@"Helvetica Neue" size:15.0];
        userName.text = [[searchDisplayArr objectAtIndex:indexPath.row] objectForKey:@"username"];
        [cell addSubview:userName];
        
        UIImageView *coverPic = [[UIImageView alloc] init];
        coverPic.frame = CGRectMake(10, profilePic.frame.origin.y+profilePic.frame.size.height+10, [UIScreen mainScreen].bounds.size.width-20, 150);
        coverPic.clipsToBounds = YES;
        [coverPic sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[searchDisplayArr objectAtIndex:indexPath.row]objectForKey:@"coverimage"] ]] placeholderImage:[UIImage imageNamed:@"loading-image"]];
        //coverPic.image = [UIImage imageNamed:@"banner"];
        [cell addSubview:coverPic];
        
        if(![[[searchDisplayArr objectAtIndex:indexPath.row] objectForKey:@"userid"] isEqualToString:[[NSUserDefaults standardUserDefaults]valueForKey:UDUSERID]])
        {
            UIButton *following_btn = [[UIButton alloc] init];
            following_btn.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-100, 10, 95, 40);
            following_btn.layer.cornerRadius = following_btn.frame.size.width/8;
            [following_btn setBackgroundColor:[UIColor orangeColor]];
            following_btn.titleLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:15.0];
            following_btn.tag = [[searchDisplayArr objectAtIndex:indexPath.row] objectForKey:@"userid"];
            
            if([[[searchDisplayArr objectAtIndex:indexPath.row] objectForKey:@"followed_by"]intValue]==1)
            {
                
                [following_btn setTitle:@"Unfollow" forState:UIControlStateNormal];
            }
            else    if([[[searchDisplayArr objectAtIndex:indexPath.row] objectForKey:@"followed_by"]intValue]==0)
            {
                
                [following_btn setTitle:@"Follow" forState:UIControlStateNormal];
            }
            else{
                
                [following_btn setTitle:@"Withdraw" forState:UIControlStateNormal];
                
                
            }
            
            
            [following_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [following_btn addTarget:self action:@selector(followClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            [cell addSubview:following_btn];
            
        }

    }
    else
    {
    UIImageView *profilePic = [[UIImageView alloc] init];
    profilePic.frame = CGRectMake(10, 10, 60, 60);
    profilePic.layer.cornerRadius = profilePic.frame.size.width/2;
    profilePic.clipsToBounds = YES;
    DebugLog(@"PROFILE IMAGE:%@",[[followingArr objectAtIndex:indexPath.row]objectForKey:@"profileimage"]);
    [profilePic sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[followingArr objectAtIndex:indexPath.row]objectForKey:@"profileimage"] ]] placeholderImage:[UIImage imageNamed:@"profile-pic-2"]];
    // profilePic.image = [UIImage imageNamed:@"own-profile"];
    
    [cell addSubview:profilePic];
    
    UILabel *profileName = [[UILabel alloc] init];
    profileName.frame = CGRectMake(profilePic.frame.origin.x+profilePic.frame.size.width+10, 10, 150, 20);
    profileName.backgroundColor = [UIColor clearColor];
    profileName.textColor = [UIColor blackColor];
    profileName.textAlignment = NSTextAlignmentLeft;
    profileName.font = [UIFont fontWithName:@"Helvetica Neue" size:15.0];
    profileName.text = [[followingArr objectAtIndex:indexPath.row] objectForKey:@"name"];
    [cell addSubview:profileName];
    
    UILabel *userName = [[UILabel alloc] init];
    userName.frame = CGRectMake(profilePic.frame.origin.x+profilePic.frame.size.width+10, 35, 150, 20);
    userName.backgroundColor = [UIColor clearColor];
    userName.textColor = [UIColor blackColor];
    userName.textAlignment = NSTextAlignmentLeft;
    userName.font = [UIFont fontWithName:@"Helvetica Neue" size:15.0];
    userName.text = [[followingArr objectAtIndex:indexPath.row] objectForKey:@"username"];
    [cell addSubview:userName];
    
    UIImageView *coverPic = [[UIImageView alloc] init];
    coverPic.frame = CGRectMake(10, profilePic.frame.origin.y+profilePic.frame.size.height+10, [UIScreen mainScreen].bounds.size.width-20, 150);
    coverPic.clipsToBounds = YES;
    [coverPic sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[followingArr objectAtIndex:indexPath.row]objectForKey:@"coverimage"] ]] placeholderImage:[UIImage imageNamed:@"loading-image"]];
    //coverPic.image = [UIImage imageNamed:@"banner"];
    [cell addSubview:coverPic];
    
    if(![[[followingArr objectAtIndex:indexPath.row] objectForKey:@"userid"] isEqualToString:[[NSUserDefaults standardUserDefaults]valueForKey:UDUSERID]])
    {
     UIButton *following_btn = [[UIButton alloc] init];
    following_btn.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-100, 10, 95, 40);
    following_btn.layer.cornerRadius = following_btn.frame.size.width/8;
    [following_btn setBackgroundColor:[UIColor orangeColor]];
    following_btn.titleLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:15.0];
 
    
    if([[[followingArr objectAtIndex:indexPath.row] objectForKey:@"followed_by"]intValue]==1)
    {
        
        [following_btn setTitle:@"Unfollow" forState:UIControlStateNormal];
    }
    else    if([[[followingArr objectAtIndex:indexPath.row] objectForKey:@"followed_by"]intValue]==0)
    {
        
        [following_btn setTitle:@"Follow" forState:UIControlStateNormal];
    }
    else{
        
        [following_btn setTitle:@"Withdraw" forState:UIControlStateNormal];
        
        
    }
    
    
    [following_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [following_btn addTarget:self action:@selector(followClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell addSubview:following_btn];
    
    }
        
    }
    
    UILabel *cellSep = [[UILabel alloc] init];
    cellSep.frame = CGRectMake(0, 234.0f, [UIScreen mainScreen].bounds.size.width, 1.0f);
    cellSep.backgroundColor = [UIColor lightGrayColor];
    [cell addSubview:cellSep];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (search) {
        TROwnProfileViewController *pushView = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"profile"];
        pushView.userId=[[searchDisplayArr objectAtIndex:indexPath.row]objectForKey:@"userid" ] ;
        
        
        if(![[[searchDisplayArr objectAtIndex:indexPath.row] objectForKey:@"userid"] isEqualToString:[[NSUserDefaults standardUserDefaults]valueForKey:UDUSERID]])
        {
            pushView.isfromOther = TRUE;
        }
        
        pushView.FollowCheck=[[[searchDisplayArr objectAtIndex:indexPath.row]objectForKey:@"followed_by" ]intValue] ;
        
        if([[[searchDisplayArr objectAtIndex:indexPath.row]objectForKey:@"followed_by" ]intValue]==1)
        {
            
            pushView.follow_text=@"Unfollow";
            
        }
        else    if([[[searchDisplayArr objectAtIndex:indexPath.row]objectForKey:@"followed_by" ]intValue]==0)
        {
            
            pushView.follow_text=@"Follow";
            
            
        }
        else{
            pushView.follow_text=@"Withdraw";
            
        }
        
        
        
        
        //pushView.follow_text = @"Unfollow";
        //setting to unfollow because there cant be any other option in following page
        
        
        [[self navigationController ]pushViewController:pushView animated:YES];

    }
    else
    {
    TROwnProfileViewController *pushView = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"profile"];
    pushView.userId=[[followingArr objectAtIndex:indexPath.row]objectForKey:@"userid" ] ;
    
    
    if(![[[followingArr objectAtIndex:indexPath.row] objectForKey:@"userid"] isEqualToString:[[NSUserDefaults standardUserDefaults]valueForKey:UDUSERID]])
    {
    pushView.isfromOther = TRUE;
    }
    
    pushView.FollowCheck=[[[followingArr objectAtIndex:indexPath.row]objectForKey:@"followed_by" ]intValue] ;
    
    if([[[followingArr objectAtIndex:indexPath.row]objectForKey:@"followed_by" ]intValue]==1)
    {
        
        pushView.follow_text=@"Unfollow";
        
    }
    else    if([[[followingArr objectAtIndex:indexPath.row]objectForKey:@"followed_by" ]intValue]==0)
    {
        
        pushView.follow_text=@"Follow";
        
        
    }
    else{
        pushView.follow_text=@"Withdraw";
        
    }

    
    
    
    //pushView.follow_text = @"Unfollow";
    //setting to unfollow because there cant be any other option in following page
    
    
    [[self navigationController ]pushViewController:pushView animated:YES];
    }
    
}


-(void)followClicked:(UIButton *)btn
{
    DebugLog(@"Btn Tag:%ld",btn.tag);
    
    UITableViewCell *cCell= (UITableViewCell *)btn.superview;
    
    NSIndexPath *indexPath = [_followingTable indexPathForCell:cCell];
    
    [self startLoading:self.view];
    
    
      [cCell setUserInteractionEnabled:NO];
    [btn setUserInteractionEnabled:NO];

    
    [operationQueue addOperationWithBlock:^{
        NSString *urlString;
        
        if (search) {
            urlString=[NSString stringWithFormat:@"%@/%@/%@/index?userid=%@&following_id=%@",GLOBALAPI,INDEX,FOLLOW,[[NSUserDefaults standardUserDefaults]valueForKey:UDUSERID ],[[searchDisplayArr objectAtIndex:indexPath.row]objectForKey:@"userid"] ];
        }
        
        else
        {
            urlString=[NSString stringWithFormat:@"%@/%@/%@/index?userid=%@&following_id=%@",GLOBALAPI,INDEX,FOLLOW,[[NSUserDefaults standardUserDefaults]valueForKey:UDUSERID ],[[followingArr objectAtIndex:indexPath.row]objectForKey:@"userid"] ];
        }
        
        
        
        
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        //urlString =[urlString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        DebugLog(@"url:%@",urlString);
        resultDict=[[NSDictionary alloc]init];
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            [self stopLoading];
            
            
            if(urlData==nil)
            {
                
                [cCell setUserInteractionEnabled:YES];
                [btn setUserInteractionEnabled:YES];
                
                [[[UIAlertView alloc]initWithTitle:@"Error!!!" message:@"error in internet Connection" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                
                
                
                
            }
            else{
                
                resultDict=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                DebugLog(@"result is:%@",resultDict);
                
                response2=[resultDict valueForKey:@"response"];
                
                
                if([[response2 valueForKey:@"status"]isEqualToString:@"Success" ])
                {
                    
                    if (!search) {
                        
                        [cCell setUserInteractionEnabled:YES];
                        [btn setUserInteractionEnabled:YES];
                        page=0;
                        dataCount=0;
                        [followingArr removeAllObjects];
                        [self loadData];
                        //[_followerTable reloadData];
                    }
                    else
                    {
                        DebugLog(@"Reload After Search:%@",response2);
                        
                        [cCell setUserInteractionEnabled:YES];
                        [btn setUserInteractionEnabled:YES];
                        
                        [self searchAgain];
                        
                        //                        NSMutableArray *tempArray = [[NSMutableArray alloc] init];
                        //
                        //                        if ([[response2  valueForKey:@"request_status"] isEqualToString:@"0"]) {
                        //
                        //                            for(NSMutableDictionary *dict in searchDisplayArr){
                        //                                DebugLog(@"Initial Dict:%@",dict);
                        //
                        //                                for( NSString *aKey in [dict allKeys])
                        //                                {
                        //
                        //                                    if ([aKey isEqualToString:@"name"]) {
                        //                                        [dict setValue:[dict objectForKey:aKey] forKey:@"fullname"];
                        //                                        [dict removeObjectForKey:aKey];
                        //                                    }
                        //                                }
                        //
                        //                                if([[dict objectForKey:@"userid"] isEqualToString:[NSString stringWithFormat:@"%ld",(long)btn.tag]]){
                        //                                    [dict setObject:@"0" forKey:@"followed_by"];
                        //                                    DebugLog(@"Dict:%@",dict);
                        //
                        //                                }
                        //
                        //                                [tempArray addObject:dict];
                        //
                        //                            }
                        //
                        //                            DebugLog(@"TEMP ARRAY:%@",tempArray);
                        //                            [searchDisplayArr removeAllObjects];
                        //                            searchDisplayArr = [tempArray mutableCopy];
                        //
                        //                        }
                        
                        //                        page=0;
                        //                        dataCount=0;
                        //                        [searchDisplayArr removeAllObjects];
                        //                        [self loadData];
                    }
                    
                    
                    
                    
                }
                else{
                    [cCell setUserInteractionEnabled:YES];
                    [btn setUserInteractionEnabled:YES];
                    [[[UIAlertView alloc]initWithTitle:@"Sorry!!!" message:[response valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                    
                }
            }
        }];
    }];
    
    
    
    
    
}

-(void)goBack
{
    CATransition *Transition=[CATransition animation];
    [Transition setDuration:0.4f];
    //[Transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
    [Transition setType:kCATransitionPush];
    [Transition setSubtype:kCATransitionFromLeft];
    [[[[self navigationController] view] layer] addAnimation:Transition forKey:@"transition"];
    [self.navigationController popViewControllerAnimated:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma UIScrollViewDelegate

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGPoint offset = scrollView.contentOffset;
    CGRect bounds = scrollView.bounds;
    CGSize size = scrollView.contentSize;
    UIEdgeInsets inset = scrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    
    float reload_distance = -60.0f;
    if(y > h + reload_distance)
    {
        
        DebugLog(@"total count is%d and current count is %d",totalCount,dataCount);
        
        if (dataCount<totalCount)
        {
            [_followingTable setUserInteractionEnabled:NO];
           
            
            page++;
            
            [self performSelector:@selector(loadData) withObject:nil];
        }
    }
    
}


- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    
    return YES;
    
}// return NO to not become first responder
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    
}// called when text starts editing
- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    return YES;
    
}// return NO to not resign first responder
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    
}// called when text ends editing
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    
    if ([searchText length]==0) {
        [searchBar resignFirstResponder];
        search = FALSE;
        if([_followingTable isHidden])
        {
            _followingTable.hidden = NO;
            _followingTable.userInteractionEnabled = YES;
            _followingTable.scrollEnabled = YES;
            
        }
        [_followingTable reloadData];
    }

    
}// called when text changes (including clear)
- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text NS_AVAILABLE_IOS(3_0)
{
    
    return YES;
    
}// called before text changes

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    searchBar.text=@"";
    [searchBar resignFirstResponder];
    search = FALSE;
    if([_followingTable isHidden])
    {
        _followingTable.hidden = NO;
        _followingTable.userInteractionEnabled = YES;
        _followingTable.scrollEnabled = YES;

    }
    [_followingTable reloadData];
    
}



- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    
    page=0;
    dataCount=0;
    [searchDisplayArr removeAllObjects];
    
    [searchBar resignFirstResponder];
    
    
    searchText=searchBar.text;
    
    [operationQueue addOperationWithBlock:^{
        NSString *urlString=[NSString stringWithFormat:@"%@/%@/follow_ios/userfollow?userid=%@&loggedinid=%@&page=%d&limit=4&searchparam=%@",GLOBALAPI,INDEX,userId,[[NSUserDefaults standardUserDefaults]valueForKey:UDUSERID],page,searchText];
        
        
        
        
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        //urlString =[urlString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        DebugLog(@"url:%@",urlString);
        searchDict=[[NSDictionary alloc]init];
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            if(urlData==nil)
            {
                
                
                [[[UIAlertView alloc]initWithTitle:@"Error!!!" message:@"error in internet Connection" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                
                
                
            }
            else{
                
                searchDict=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                DebugLog(@"result is:%@",searchDict);
                
                
                if([[searchDict valueForKey:@"status"]isEqualToString:@"Error" ])
                {
                    
                    NSDictionary   *responseS=[searchDict valueForKey:@"response"];
                    
                    [_followingTable setUserInteractionEnabled:NO];
                    [_followingTable setHidden:YES];
                    
                    [_noFollowLbl setHidden:NO];
                    [_noFollowLbl setText:[responseS valueForKey:@"message"]];
                    
                    // [[[UIAlertView alloc]initWithTitle:@"Sorry!!!" message:[response valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                    
                }
                else{
                   
                    search = TRUE;
                    searchDisplayArr = [[NSMutableArray alloc] init];
                    
                    searchResult=[searchDict valueForKey:@"response"];
                    
                    DebugLog(@"%lu",(unsigned long)[searchResult count]);
                    
                    dataCount+=[searchResult count];
                    
                    totalCount=[[searchDict valueForKey:@"$totalcount_search"]intValue];
                    
                    
                    for(NSDictionary *rowDict in searchResult)
                    {
                        
                        [searchDisplayArr addObject:rowDict];
                        
                        
                    }
                    
                    DebugLog(@"Search Data:%@",searchDisplayArr);
                    
                    [_followingTable setUserInteractionEnabled:YES];
                    [_followingTable setHidden:NO];
                    [_noFollowLbl setHidden:YES];
                    
                    [_followingTable reloadData];
                    
                    
                }
                
            }
            
        }];
        
    }];
    
}// called when keyboard search button pressed

-(void)searchAgain
{
    //    _followerTable.hidden = YES;
    
    page=0;
    dataCount=0;
    [searchDisplayArr removeAllObjects];
    
    //[searchBar resignFirstResponder];
    
    
    //searchText=searchBar.text;
    
    [operationQueue addOperationWithBlock:^{
        NSString *urlString=[NSString stringWithFormat:@"%@/%@/follow_ios/userfollowing?userid=%@&loggedinid=%@&page=%d&limit=4&searchparam=%@",GLOBALAPI,INDEX,userId,[[NSUserDefaults standardUserDefaults]valueForKey:UDUSERID],page,searchText];
        
        
        
        
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        //urlString =[urlString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        DebugLog(@"url:%@",urlString);
        searchDict=[[NSDictionary alloc]init];
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            if(urlData==nil)
            {
                
                
                [[[UIAlertView alloc]initWithTitle:@"Error!!!" message:@"error in internet Connection" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                
                
                
            }
            else{
                
                searchDict=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                DebugLog(@"Search result is:%@",searchDict);
                
                
                if([[searchDict valueForKey:@"status"]isEqualToString:@"Error" ])
                {
                    
                    NSDictionary *responseS=[searchDict valueForKey:@"response"];
                    
                    [_followingTable setUserInteractionEnabled:NO];
                    [_followingTable setHidden:YES];
                    
                    [_noFollowLbl setHidden:NO];
                    [_noFollowLbl setText:[responseS valueForKey:@"message"]];
                    
                    // [[[UIAlertView alloc]initWithTitle:@"Sorry!!!" message:[response valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                    
                }
                else{
                    search = TRUE;
                    
                    searchDisplayArr = [[NSMutableArray alloc] init];
                    
                    searchResult=[searchDict valueForKey:@"response"];
                    
                    DebugLog(@"Search Count------>: %lu",(unsigned long)[searchResult count]);
                    
                    dataCount+=[searchResult count];
                    
                    totalCount=[[searchDict valueForKey:@"$totalcount_search"]intValue];
                    
                    
                    for(NSDictionary *rowDict in searchResult)
                    {
                        
                        [searchDisplayArr addObject:rowDict];
                        
                        
                    }
                    
                    DebugLog(@"Search Data:%@",searchDisplayArr);
                    
                    [_followingTable setUserInteractionEnabled:YES];
                    [_followingTable setHidden:NO];
                    [_noFollowLbl setHidden:YES];
                    
                    
                    [_followingTable reloadData];
                    
                    [followingArr removeAllObjects];
                    [self performSelectorOnMainThread:@selector(loadData) withObject:nil waitUntilDone:NO];
                    
                }
                
            }
            
        }];
        
    }];
    
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
