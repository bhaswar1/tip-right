//
//  splashViewController.m
//  Tip Right
//
//  Created by santanu on 14/07/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import "splashViewController.h"
#import "TRLoginViewController.h"
#import "UIImageView+WebCache.h"

@interface splashViewController ()
{
    NSOperationQueue *opQueue;
    NSDictionary *json;
    __weak IBOutlet UIImageView *backImage;
}
@end

@implementation splashViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    opQueue=[[NSOperationQueue alloc]init];
    
    
    [self loadBackImage];
    
    [self performSelector:@selector(loadLogin) withObject:nil afterDelay:5.0];
    
    
    
    
    // Do any additional setup after loading the view.
}

-(void)loadBackImage
{
    //http://esolz.co.in/lab3/tipright/index.php/app_ios/appbackgroundimage
    
    
    //[self startLoading:self.view];
    
    
    [opQueue addOperationWithBlock:^{
        
        NSString *urlString =[NSString stringWithFormat:@"http://www.esolz.co.in/lab3/tipright/index.php/app_ios/appbackgroundimage"]; //&thumb=true
        
        DebugLog(@"requests url: %@",urlString);
        
        NSString *newString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSData *dataURL =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            
            // [self stopLoading];
            
            
            NSString *error_msg;
            
            if (dataURL != nil)
            {
                NSError *error=nil;
                
                json = [NSJSONSerialization JSONObjectWithData:dataURL //1
                        
                                                       options:kNilOptions
                        
                                                         error:&error];
                DebugLog(@"json returns: %@",json);
                
                
                error_msg= [json objectForKey:@"status"];
                DebugLog(@"err  %@",[json objectForKey:@"status"]);
                if ([error_msg isEqualToString:@"Success"])
                {
                    
                    NSDictionary *dict=[json objectForKey:@"data"];
                    
                    DebugLog(@"Dynamic Background:%@",[dict objectForKey:@"appimage"]);
                    
                  //  backImage.image = [UIImage imageNamed:@"banner"];
                    
                    
                   // [backImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[dict objectForKey:@"appimage"]]]];
                    
                    [backImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[dict objectForKey:@"appimage"]]] placeholderImage:[UIImage imageNamed:@"bg"]];
                    
                    
                }
                else
                {
                    
                    
                }
            }
            else
            {
                
                
            }
            
        }];
        
    }];
    
    
    
}

-(void)loadLogin
{
    if([[NSUserDefaults standardUserDefaults]boolForKey:@"keep"])
    {
        
        UIViewController *afterLogin = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"Home"];
        [[self navigationController ]pushViewController:afterLogin animated:YES];
    }
    else
    {
    
    UIViewController *Login = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"Login"];
    [[self navigationController ]pushViewController:Login animated:YES];
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
