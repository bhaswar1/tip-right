//
//  TREdit_ProfileViewController.m
//  Tip Right
//
//  Created by intel on 15/06/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import "TREdit_ProfileViewController.h"
#import "AFHTTPClient.h"
#import "AFHTTPRequestOperation.h"
#import "AppDelegate.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <AssetsLibrary/AssetsLibrary.h>


@interface TREdit_ProfileViewController ()<UIActionSheetDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIGestureRecognizerDelegate,UITextFieldDelegate,UIScrollViewDelegate,UITextViewDelegate, UIActionSheetDelegate>
{
    UIActivityIndicatorView *act;
    NSDictionary *json, *result, *response;
    UIAlertView *alert;
    NSArray *pickerSex;
    UIToolbar *providerToolbar;
    UIBarButtonItem *done;
    UIPickerView *picker;
    UIPopoverController *popover;
    UIImagePickerController *ipc,*ipc2;
    UIImage *selectedImage;
    NSDictionary *jsonDict;
    AppDelegate *del;
    CGPoint svos;
    NSOperationQueue *opQueue;
    UITextField *firstResponder;
    NSString *oldValue;
    CGRect scrollRect;
    BOOL image_type;
    
    __weak IBOutlet UIImageView *profileCamera;
    BOOL privateCheck;
    __weak IBOutlet UIImageView *checkImg;
    __weak IBOutlet UIButton *checkBtn;
}
@property (weak, nonatomic) IBOutlet UIView *coverClick;
@property (strong, nonatomic) IBOutlet UIImageView *firstName_img;
@property (strong, nonatomic) IBOutlet UIImageView *lastName_img;
@property (strong, nonatomic) IBOutlet UIImageView *userName_img;
@property (strong, nonatomic) IBOutlet UIImageView *email_img;
@property (strong, nonatomic) IBOutlet UIImageView *sex_img;
@property (strong, nonatomic) IBOutlet UIImageView *bio_img;
@property (strong, nonatomic) IBOutlet UIImageView *web_img;
@property (strong, nonatomic) IBOutlet UITextField *firstName_field;
@property (strong, nonatomic) IBOutlet UITextField *lastName_field;
@property (strong, nonatomic) IBOutlet UITextField *userName_field;
@property (strong, nonatomic) IBOutlet UITextField *email_field;
@property (strong, nonatomic) IBOutlet UITextView *bio_field;
@property (strong, nonatomic) IBOutlet UITextField *web_field;
@property (weak, nonatomic) IBOutlet UILabel *bioLbl;

@property (strong, nonatomic) IBOutlet UIButton *submit_btn;
@property (weak, nonatomic) IBOutlet UIButton *coverChangeBtn;
@property (weak, nonatomic) IBOutlet UITextField *sexField;

@end

@implementation TREdit_ProfileViewController



-(void) keyboardWillShow:(NSNotification *)note{
    
    
    [_scrollView scrollRectToVisible:CGRectMake(0, _scrollView.contentSize.height - _scrollView.frame.size.height, _scrollView.frame.size.width, _scrollView.frame.size.height) animated:NO];
    
    
}

-(void) keyboardWillHide:(NSNotification *)note{
    
    [_scrollView scrollRectToVisible:CGRectMake(0, 0, _scrollView.frame.size.width, _scrollView.frame.size.height) animated:NO];
    
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self Header:self.view];
    [self Footer:self.view];
    opQueue=[[NSOperationQueue alloc]init];
    privateCheck=NO;
    
    
    del = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    self.view.backgroundColor = [UIColor colorWithRed:(248.0f/255.0f) green:(247.0f/255.0f) blue:(243.0f/255.0f) alpha:1.0f];
    
    
    
    
    //_scrollView.delegate = self;
    
    _profile_pic.layer.cornerRadius = _profile_pic.frame.size.width/2;
    _profile_pic.clipsToBounds = YES;
    
    
    
    
    
//    UITapGestureRecognizer *tapPic=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changeProfilePic)];
//    
//    [tapPic setNumberOfTouchesRequired:1];
//    
//    [_profile_pic setUserInteractionEnabled:YES];
//    [_profile_pic addGestureRecognizer:tapPic];
    
    
    [_firstName_field setValue:[UIColor blackColor]
                    forKeyPath:@"_placeholderLabel.textColor"];
    [_lastName_field setValue:[UIColor blackColor]
                   forKeyPath:@"_placeholderLabel.textColor"];
    [_userName_field setValue:[UIColor blackColor]
                   forKeyPath:@"_placeholderLabel.textColor"];
    [_email_field setValue:[UIColor blackColor]
                forKeyPath:@"_placeholderLabel.textColor"];
    
    [_web_field setValue:[UIColor blackColor]
              forKeyPath:@"_placeholderLabel.textColor"];
    
    pickerSex = @[@"Male",@"Female"];
    
    _firstName_field.delegate = self;
    _lastName_field.delegate = self;
    _email_field.delegate = self;
    _userName_field.delegate = self;
    _bio_field.delegate = self;
    _web_field.delegate = self;
    
    
    [_bio_field.layer setBorderColor:[UIColor grayColor].CGColor];
    [_bio_field.layer setBorderWidth:1];
    
    CALayer *imageLayer = _bio_field.layer;
    
    [imageLayer setCornerRadius:10/375.0*self.view.frame.size.width];
    
    [imageLayer setBorderWidth:1];
    
    [imageLayer setMasksToBounds:YES];
    
    [_bio_field.layer setMasksToBounds:YES];
    
    
    
    [self startLoading:self.view];
    
    //  [self performSelectorOnMainThread:@selector(loadData) withObject:nil waitUntilDone:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWillShow:)
     
                                                 name:UIKeyboardWillShowNotification
     
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWillHide:)
     
                                                 name:UIKeyboardWillHideNotification
     
                                               object:nil];
    
    [self displaydata];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    //_scrollView.delegate = self;
    _scrollView.scrollEnabled = YES;
    _scrollView.userInteractionEnabled = YES;
    //[_mainScroll setContentSize:CGSizeMake(_mainScroll.frame.size.width,_mainScroll.frame.size.height)];
    _scrollView.contentSize = CGSizeMake([UIScreen mainScreen].bounds.size.width, 800.0/568.0*self.view.frame.size.height);
    _scrollView.showsVerticalScrollIndicator=NO;
   // _scrollView.contentOffset=CGPointMake(0, 0);
    
    
   // [self loadData];
    
    
}

-(void)loadData
{
    
    
    [opQueue addOperationWithBlock:^{
        
        // http://esolz.co.in/lab3/tipright/index.php/profile_ios/view_user?user_id=141
        

        
       NSString *urlString=[NSString stringWithFormat:@"%@/%@/profile_ios/view_user?user_id=%@&loggedin_id=%@",GLOBALAPI,INDEX,[[NSUserDefaults standardUserDefaults] valueForKey:UDUSERID],[[NSUserDefaults standardUserDefaults] valueForKey:UDUSERID]];
        
        
        
        
        DebugLog(@"profile url:%@",urlString);
        
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        result=[[NSDictionary alloc]init];
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        if(urlData==nil)
        {
            
            [[[UIAlertView alloc]initWithTitle:@"Error!!!" message:@"error in internet Connection" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
            
        }
        else{
            
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                
                result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                DebugLog(@"Profile result is:%@",result);
                
                
                if([[result valueForKey:@"status"]isEqualToString:@"Error" ])
                {
                    
                    [[[UIAlertView alloc]initWithTitle:@"Sorry!!!" message:@"ERROR" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                    
                }
                else{
                    
                    
                    response = [result objectForKey:@"response"];
                    
                    [[NSUserDefaults standardUserDefaults] setObject:[response objectForKey:@"background_img"] forKey:@"cover_pic"];
                    
                    [[NSUserDefaults standardUserDefaults] setObject:[response objectForKey:@"bio"] forKey:@"bio"];
                    
                    
                    if(![[[NSUserDefaults standardUserDefaults] objectForKey:@"email"] isEqualToString: _email_field.text])
                        
                    {
                        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"keep" ];
                    }
                    
                    [[NSUserDefaults standardUserDefaults] setObject:[response objectForKey:@"email"] forKey:@"email"];
                    
                    
                    [[NSUserDefaults standardUserDefaults] setObject:[response objectForKey:@"first_name"] forKey:@"first_name"];
                    [[NSUserDefaults standardUserDefaults] setObject:[response objectForKey:@"last_name"] forKey:@"last_name"];
                    
                    [[NSUserDefaults standardUserDefaults] setObject:[response objectForKey:@"profileimage"] forKey:@"profile_pic"];
                    
                    
                    [[NSUserDefaults standardUserDefaults] setObject:[response objectForKey:@"sex"] forKey:@"sex"];
                    
                    
                    [[NSUserDefaults standardUserDefaults] setObject:[response objectForKey:@"username"] forKey:@"username"];
                    
                    
                    [[NSUserDefaults standardUserDefaults] setObject:[response objectForKey:@"website"] forKey:@"website"];
                    
                    [[NSUserDefaults standardUserDefaults] setObject:[response objectForKey:@"private_status"] forKey:@"private"];
                }
                
                
                [self displaydata];
                
            }];
        }
        
    }];
    
    
}

-(void)displaydata
{
        _coverPic.backgroundColor = [UIColor blackColor];
     _coverPic.contentMode = UIViewContentModeScaleAspectFit;
    
    [_coverPic sd_setImageWithURL:[NSURL URLWithString:[[NSUserDefaults standardUserDefaults] objectForKey:@"cover_pic"]]  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [_profile_pic sd_setImageWithURL:[NSURL URLWithString:[[NSUserDefaults standardUserDefaults] objectForKey:@"profile_pic"]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [self stopLoading];
        }];
    }];
    
    
    _firstName_field.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"first_name"];
    _lastName_field.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"last_name"];
    _userName_field.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"username"];
    _email_field.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"email"];
    _sexField.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"sex"];
    
    
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"bio"]length]>0)
        _bio_field.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"bio"];
    else
        [_bioLbl setHidden:NO];
    
    
    
    
    
    _web_field.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"website"];
    
    privateCheck=[[[NSUserDefaults standardUserDefaults] objectForKey:@"private"]boolValue];
    
    if(privateCheck)
    {
        [checkImg setImage:[UIImage imageNamed:@"checkboxTick"]];
        
    }
    else{
        [checkImg setImage:[UIImage imageNamed:@"checkBoxUntick"]];
        
    }
    
    
    
    //
    //        [_coverPic sd_setImageWithURL:[NSURL URLWithString:[response objectForKey:@"background_img"]]];
    //        [_profile_pic sd_setImageWithURL:[NSURL URLWithString:[response objectForKey:@"profileimage"]]];
    //
    //        _firstName_field.text = [response objectForKey:@"first_name"];
    //        _lastName_field.text = [response objectForKey:@"last_name"];
    //        _userName_field.text = [response objectForKey:@"username"];
    //        _email_field.text = [response objectForKey:@"email"];
    //        _sexLabel.text = [response objectForKey:@"sex"];
    //        _bio_field.text = [response objectForKey:@"bio"];
    //        _web_field.text = [response objectForKey:@"website"];
    
    //[self stopLoading];
    
}

- (IBAction)submitFunc:(id)sender
{
    [_firstName_field resignFirstResponder];
    [_lastName_field resignFirstResponder];
    [_userName_field resignFirstResponder];
    [_email_field resignFirstResponder];
    [_bio_field resignFirstResponder];
    [_web_field resignFirstResponder];
    
    [_scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    
    
    
    NSString *gender;
    if ([ [_sexField.text lowercaseString] isEqualToString:@"male"]) {
        gender = @"0";
    }
    else if([[_sexField.text lowercaseString] isEqualToString:@"female"])
    {
        gender = @"1";
    }
    
    
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    
    if ([[_firstName_field.text stringByTrimmingCharactersInSet:whitespace] length]==0) {
        
        
        alert = [[UIAlertView alloc] initWithTitle:@"Please Enter First Name!"
                                           message:Nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        
        [alert show];
    }
    else if ([[_lastName_field.text stringByTrimmingCharactersInSet:whitespace] length]==0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Last Name!"
                                           message:Nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        
        [alert show];
    }
    else if ([[_userName_field.text stringByTrimmingCharactersInSet:whitespace] length]==0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Username!"
                                           message:Nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        
        [alert show];
    }
    
    else if ([[_email_field.text stringByTrimmingCharactersInSet:whitespace] length]==0) {
        
        alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Email!"
                                           message:Nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        
        [alert show];
    }
    
    
    else if ([_email_field.text isEqualToString:@""])
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Email"
                                           message:Nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
    }
    
    else if ([[_email_field.text stringByTrimmingCharactersInSet:whitespace] length] > 0 && ![self NSStringIsValidEmail:_email_field.text])
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Please Enter a Valid Email"
                                           message:Nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        
        [alert show];
        
    }
    
    else if ([[_web_field.text stringByTrimmingCharactersInSet:whitespace] length] > 0 && ![self validateUrl:_web_field.text])
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Please Enter a valid Website"
                                           message:Nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
        
    }
    
    
    else
    {
        
        [self startLoading:self.view];
        
        NSString *urlString1 =[NSString stringWithFormat:@"%@/%@/%@/editprofile?user_id=%@&first_name=%@&last_name=%@&email=%@&sex=%@&username=%@&bio=%@&website=%@&private=%d",GLOBALAPI,INDEX,EDITPROFILE,[[NSUserDefaults standardUserDefaults]valueForKey:UDUSERID],_firstName_field.text,_lastName_field.text,_email_field.text,gender,_userName_field.text,_bio_field.text,_web_field.text,privateCheck];
        
        
        DebugLog(@"requests url: %@",urlString1);
        
        //  NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSString *newString1 = [urlString1 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding ] ;
        
        NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
        NSString *error_msg;
        
        if (signeddataURL1 != nil)
        {
            NSError *error=nil;
            
            json = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                    
                                                   options:kNilOptions
                    
                                                     error:&error];
            DebugLog(@"json returns: %@",json);
            
            error_msg= [[json objectForKey:@"response"] objectForKey:@"status"];
            DebugLog(@"err  %@",[[json objectForKey:@"response"] objectForKey:@"status"]);
            if ([error_msg isEqualToString:@"Success"])
            {
                [self stopLoading];
                
                alert = [[UIAlertView alloc] initWithTitle:nil
                                                   message:@"Data successfully updated"
                                                  delegate:self
                                         cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                [alert show];
                
                [self loadData];
//                [self performSelectorOnMainThread:@selector(loadData) withObject:nil waitUntilDone:YES];
                
            }
            else
            {
                [self stopLoading];
                alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%@",[[json objectForKey:@"response"] objectForKey:@"message"]]
                                                   message:Nil
                                                  delegate:self
                                         cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                [alert show];
            }
        }
        else
        {
            [self stopLoading];
            alert = [[UIAlertView alloc] initWithTitle:@"error in server connection!"
                                               message:Nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
        }
        
        
    }
    
}




- (IBAction)privateChecking:(id)sender {
    
    
    if(privateCheck)
    {
        [checkImg setImage:[UIImage imageNamed:@"checkBoxUntick"]];
        
        privateCheck=NO;
    }
    else{
        [checkImg setImage:[UIImage imageNamed:@"checkboxTick"]];
        privateCheck=YES;
    }
    
    
}
- (IBAction)deleteAccount:(id)sender {
    
    
    
    
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"Do you want to Delete your Account?"
                                                       delegate:self
                                              cancelButtonTitle:@"No"
                                              otherButtonTitles:@"Yes", nil];
    [alertView show];
    
    
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    switch(buttonIndex) {
        case 0: //"No" pressed
            //do something?
            break;
        case 1:
            
            [self startLoading:self.view];
            
            [opQueue addOperationWithBlock:^{
                
                //http://www.esolz.co.in/lab3/tipright/index.php/editprofile_ios/del_acc?userid=142
                
                NSString *urlString=[NSString stringWithFormat:@"%@/%@/editprofile_ios/del_acc?userid=%@",GLOBALAPI,INDEX,[[NSUserDefaults standardUserDefaults]valueForKey:UDUSERID]];
                
                
                DebugLog(@"firing url:%@",urlString);
                
                urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
                
                result=[[NSDictionary alloc]init];
                
                NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
                
                if(urlData==nil)
                {
                    
                    [[[UIAlertView alloc]initWithTitle:@"Error!!!" message:@"error in internet Connection" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                    
                }
                else{
                    
                    [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                        
                        result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                        DebugLog(@"Profile result is:%@",result);
                        
                        response = [result objectForKey:@"response"];
                        
                        
                        if([[response valueForKey:@"status"]isEqualToString:@"Error" ])
                        {
                            
                            [[[UIAlertView alloc]initWithTitle:@"Sorry!!!" message:[response valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                            
                        }
                        else{
                            
                            [self stopLoading];
                            
                            UIViewController *Login = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"Login"];
                            [[self navigationController ]pushViewController:Login animated:YES];
                            
                        }
                    }];
                }
            }];
            
            
            
            break;
    }
}


// The number of columns of data
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// returns the # of rows in each component..
// The number of rows of data
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return pickerSex.count;
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return pickerSex[row];
}

// Catpure the picker view selection
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    _sexField.text = [pickerSex objectAtIndex:row];
    
}



-(void)changeProfilePic
{
    
    [self ImageChoose];
    
}

-(void)openGallery
{
    ipc2= [[UIImagePickerController alloc] init];
    
    ipc2.delegate = self;
    
    ipc2.allowsEditing=YES;
    
    //ipc2.view.tag=2;
    
    ipc2.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    
    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone)
        
        [self presentViewController:ipc2 animated:YES completion:nil];
    
    else
        
    {
        
        popover=[[UIPopoverController alloc]initWithContentViewController:ipc2];
        
        [popover presentPopoverFromRect:self.view.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
    }
    
}

- (IBAction)coverChange:(int)sender {
    
    _coverChangeBtn.userInteractionEnabled = NO;
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select a option"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Change Profile Picture", @"Change Cover Picture", nil];
    
    actionSheet.tag = 900;
    [actionSheet showInView:_scrollView];
    
    
}

-(void)ImageChoose
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select a option"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Open Gallery", @"Open Camera", nil];
    
    actionSheet.tag = 800;
    [actionSheet showInView:_scrollView];
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 900) {
        if (buttonIndex==0) {
            image_type = 0;
            [self changeProfilePic];
        }
        else if(buttonIndex==1){
            image_type = 1;
            [self changeCoverPic];
        }
        else
        {
            [actionSheet dismissWithClickedButtonIndex:2 animated:YES];
            _coverChangeBtn.userInteractionEnabled = YES;
        }
    }
    else if (actionSheet.tag == 800)
    {
        if (buttonIndex==0) {
            [self openGallery];
        }
        else if(buttonIndex==1){
            [self camera_func];
        }
        else
        {
            [actionSheet dismissWithClickedButtonIndex:2 animated:YES];
            _coverChangeBtn.userInteractionEnabled = YES;
        }
    }
}

-(void)changeCoverPic
{
    [self ImageChoose];
    //    ipc= [[UIImagePickerController alloc] init];
    //
    //    ipc.delegate = self;
    //
    //    ipc.allowsEditing=YES;
    //
    //    ipc.view.tag=1;
    //
    //    ipc.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    //
    //    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone)
    //
    //        [self presentViewController:ipc animated:YES completion:nil];
    //
    //    else
    //
    //    {
    //        popover=[[UIPopoverController alloc]initWithContentViewController:ipc];
    //
    //        [popover presentPopoverFromRect:self.view.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    //    }
    
}


-(void)camera_func
{
    ipc = [[UIImagePickerController alloc] init];
    ipc.delegate = self;
    ipc.allowsEditing=YES;
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        ipc.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:ipc animated:YES completion:NULL];
    }
    else
    {
        alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No Camera Available." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        alert = nil;
    }
    
    
    
    //    DebugLog(@"camera func");
    //    if ([self isCameraAvailable] && [self doesCameraSupportTakingPhotos]) {
    //        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
    //        controller.sourceType = UIImagePickerControllerSourceTypeCamera;
    //        if ([self isFrontCameraAvailable]) {
    //            controller.cameraDevice = UIImagePickerControllerCameraDeviceFront;
    //        }
    //        NSMutableArray *mediaTypes = [[NSMutableArray alloc] init];
    //        [mediaTypes addObject:(__bridge NSString *)kUTTypeImage];
    //        controller.mediaTypes = mediaTypes;
    //        controller.delegate = self;
    //        [self presentViewController:controller
    //                           animated:YES
    //                         completion:^(void){
    //                             DebugLog(@"Picker View Controller is presented");
    //                         }];
    //    }
}


//#pragma mark - OpenCamera Delegate

//- (BOOL) isCameraAvailable{
//    return [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
//}
//
//- (BOOL) isRearCameraAvailable{
//    return [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear];
//}
//
//- (BOOL) isFrontCameraAvailable {
//    return [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront];
//}
//
//- (BOOL) doesCameraSupportTakingPhotos {
//    return [self cameraSupportsMedia:(__bridge NSString *)kUTTypeImage sourceType:UIImagePickerControllerSourceTypeCamera];
//}
//
//- (BOOL) isPhotoLibraryAvailable{
//    return [UIImagePickerController isSourceTypeAvailable:
//            UIImagePickerControllerSourceTypePhotoLibrary];
//}
//- (BOOL) canUserPickVideosFromPhotoLibrary{
//    return [self
//            cameraSupportsMedia:(__bridge NSString *)kUTTypeMovie sourceType:UIImagePickerControllerSourceTypePhotoLibrary];
//}
//- (BOOL) canUserPickPhotosFromPhotoLibrary{
//    return [self
//            cameraSupportsMedia:(__bridge NSString *)kUTTypeImage sourceType:UIImagePickerControllerSourceTypePhotoLibrary];
//}
//
//- (BOOL) cameraSupportsMedia:(NSString *)paramMediaType sourceType:(UIImagePickerControllerSourceType)paramSourceType{
//    __block BOOL result = NO;
//    if ([paramMediaType length] == 0) {
//        return NO;
//    }
//    NSArray *availableMediaTypes = [UIImagePickerController availableMediaTypesForSourceType:paramSourceType];
//    [availableMediaTypes enumerateObjectsUsingBlock: ^(id obj, NSUInteger idx, BOOL *stop) {
//        NSString *mediaType = (NSString *)obj;
//        if ([mediaType isEqualToString:paramMediaType]){
//            result = YES;
//            *stop= YES;
//        }
//    }];
//    return result;
//}
//


#pragma mark - ImagePickerController Delegate

-(void)imagePickerController:(UIImagePickerController *)Ipicker didFinishPickingMediaWithInfo:(NSDictionary *)info

{
    
    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
        
        [Ipicker dismissViewControllerAnimated:YES completion:nil];
        
    } else {
        
        [popover dismissPopoverAnimated:YES];
        
    }
    
    selectedImage=[info objectForKey:UIImagePickerControllerEditedImage];
    
    //[_coverPic setImage:selectedImage];
    
    [self uploadImage:selectedImage];
    
    [Ipicker dismissViewControllerAnimated:YES completion:NULL];
    
}


-(void)uploadImage:(UIImage *)image
{
    
    NSData *imageData = [NSData dataWithData:UIImagePNGRepresentation(image)];
    
    NSString *urlString;
    
    if(image_type==1)
    {
        DebugLog(@"coveruplaod");
        
        urlString =[NSString stringWithFormat:@"%@/%@/%@/edit_coverpic?user_id=%@",GLOBALAPI,INDEX,EDITPROFILE,[[NSUserDefaults standardUserDefaults] valueForKey:UDUSERID]];
        
        [self startLoading:_coverPic];
        
    }
    else{
        
        DebugLog(@"profileupload");
        urlString=[NSString stringWithFormat:@"%@/%@/%@/edit_profile_pic?user_id=%@",GLOBALAPI,INDEX,EDITPROFILE,[[NSUserDefaults standardUserDefaults] valueForKey:UDUSERID]];
        
        [self startLoading:_coverPic];
    }
    
    DebugLog(@"post url  : %@",urlString);
    
    
    
    AFHTTPClient *client= [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:urlString]];
    
    //NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:encodedText, @"details", nil];
    
    NSMutableURLRequest *request = [client multipartFormRequestWithMethod:@"POST" path:nil parameters:nil constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
        
        if ([imageData length]>0) {
            
            NSString *imagename;
            if(image_type==1)
            {
                imagename = [NSString stringWithFormat:@"coverimage"];
            }
            else{
                imagename = [NSString stringWithFormat:@"profileimage"];
                
            }
            
            [formData appendPartWithFileData: imageData name:imagename fileName:@"temp.jpeg" mimeType:@"image/jpeg"];
            
            
        }
        
    }];
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation * _operation, id responseObject) {
        
        
        NSString *responseStr = [_operation responseString];
        
        DebugLog(@"response: [%@]",responseStr);
        
        NSError *error;
        jsonDict = [NSJSONSerialization JSONObjectWithData:responseObject //1
                    
                                                   options:kNilOptions
                    
                                                     error:&error];
        DebugLog(@"json returns: %@",jsonDict);
        
        NSDictionary *responseDict=[jsonDict valueForKey:@"response"];
        
        if ([[responseDict valueForKey:@"status"]isEqualToString:@"Success"]) {
            
            [self stopLoading];
            
            if (image_type == 1) {
                
                
                [_coverPic sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[responseDict objectForKey:@"image"]]]];
                
                   [[NSUserDefaults standardUserDefaults] setObject:[responseDict objectForKey:@"image"] forKey:@"cover_pic"];
                
                alert=[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@",[responseDict valueForKey:@"message"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                
               
                
                
                [alert show];
                
                _coverChangeBtn.userInteractionEnabled = YES;
            }
            else
            {
            [_profile_pic sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[responseDict objectForKey:@"image"]]]];
               
                     [[NSUserDefaults standardUserDefaults] setObject:[responseDict objectForKey:@"image"] forKey:@"profile_pic"];
                
                alert=[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@",[responseDict valueForKey:@"message"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                
                
                [alert show];
                _coverChangeBtn.userInteractionEnabled = YES;
                
            }
            
            //[self performSelectorOnMainThread:@selector(loadData) withObject:nil waitUntilDone:YES];
            
        }
        else{
            
            [self stopLoading];
            alert=[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@",[responseDict valueForKey:@"message"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            
            [alert show];
            _coverChangeBtn.userInteractionEnabled = YES;
            
        }
        
        
    } failure:^(AFHTTPRequestOperation * _operation, NSError *error) {
        
        
        if([_operation.response statusCode] == 403){
            
            DebugLog(@"Upload Failed");
            
            return;
        }
        [self stopLoading];
        alert=[[UIAlertView alloc]initWithTitle:nil message:@"Failed to upload!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        _coverChangeBtn.userInteractionEnabled = YES;
        
    }];
    
    [operation start];
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    if(textField==_sexField)
    {
        
        [self pickerShow:textField];
    }
    
}


-(void)pickerShow:(UITextField *)sender
{
    
    firstResponder=sender;
    picker=[[UIPickerView alloc]initWithFrame:CGRectMake(0.0f, 0.0f, FULLWIDTH, 150.0f)];
    picker.delegate=self;
    picker.dataSource=self;
    [picker setShowsSelectionIndicator:YES];
    [sender setInputView:picker];
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView setTranslucent:YES];
    [keyboardDoneButtonView setTintColor:Nil];
    [keyboardDoneButtonView sizeToFit];
    
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem* doneButton=[[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(pickerDoneClicked)];
    UIBarButtonItem* cancelButton=[[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(pickerCancelClicked)];
    [keyboardDoneButtonView setItems:@[flex,cancelButton,doneButton]];
    [sender setInputAccessoryView:keyboardDoneButtonView];
    oldValue = [[NSUserDefaults standardUserDefaults] objectForKey:@"sex"];
    firstResponder.text=[pickerSex objectAtIndex:0];
    
}
-(void)pickerDoneClicked
{
    
    
    oldValue=[firstResponder text];
    [firstResponder resignFirstResponder];
}

-(void)pickerCancelClicked
{
    
    [firstResponder setText:oldValue];
    [firstResponder resignFirstResponder];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    
    
    return YES;
}

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    
    _bioLbl.hidden=YES;
    
    textView.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"bio"];
    
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"])
        
    { [textView resignFirstResponder];
        
        if(textView.text.length == 0){
            _bioLbl.hidden=NO;
        }
        
    }
    
    return YES;
    
}

-(void)textViewDidChange:(UITextView *)textView

{
    if(textView.text.length == 0){
        _bioLbl.hidden=NO;
        
    }
    else
    {
        _bioLbl.hidden=YES;
    }
    
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    checkString = [checkString lowercaseString];
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:checkString];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL) validateUrl: (NSString *) candidate {
    NSString *urlRegEx =
    @"(http|https)://((\\w\\w\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
    
    NSString *urlRegEx1 =  @"((\\w\\w\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
    
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    
    if ([urlTest evaluateWithObject:candidate]) {
        
        return [urlTest evaluateWithObject:candidate];
    }
    else
    {
        NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx1];
        if ([urlTest evaluateWithObject:candidate]) {
            return [urlTest evaluateWithObject:candidate];
        }
        else
        {
            return false;
        }
        
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
