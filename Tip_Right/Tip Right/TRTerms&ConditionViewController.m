//
//  TRTerms&ConditionViewController.m
//  Tip Right
//
//  Created by intel on 17/07/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import "TRTerms&ConditionViewController.h"

@interface TRTerms_ConditionViewController ()<UIScrollViewDelegate>
{
    UIScrollView *scrollView;
    NSOperationQueue *opQueue;
    NSString *urlString;
    NSDictionary *json;
    UIAlertView *alert;
    UILabel *termsLabel;
}

@end

@implementation TRTerms_ConditionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tipView = YES;
    [self Header:self.view];
    
  
    self.view.backgroundColor = [UIColor colorWithRed:(248.0f/255.0f) green:(247.0f/255.0f) blue:(243.0f/255.0f) alpha:1.0f];
}

-(void)viewDidAppear:(BOOL)animated
{
    scrollView = [[UIScrollView alloc] init];
    scrollView.frame = CGRectMake(0, 60, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    [self.view addSubview:scrollView];
    
    scrollView.delegate = self;
    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width,scrollView.frame.size.height+100)];
    scrollView.contentOffset=CGPointMake(0, 0);
    
    termsLabel = [[UILabel alloc] init];
    termsLabel.frame = CGRectMake(10, 20, [UIScreen mainScreen].bounds.size.width-20, scrollView.frame.size.height);
    termsLabel.lineBreakMode = UILineBreakModeCharacterWrap;
    termsLabel.numberOfLines = 0;
    [termsLabel setFont:[UIFont fontWithName:@"Helvetica Neue" size:15.0]];
    [scrollView addSubview:termsLabel];
    
    opQueue = [[NSOperationQueue alloc] init];
    
    if (_terms) {
        urlString = @"http://www.esolz.co.in/lab3/tipright/index.php/notification_ios/get_termss";
    }
    else if (_privacy)
    {
        urlString = @"http://www.esolz.co.in/lab3/tipright/index.php/notification_ios/get_privacyy";
    }
    
    [self loadData];
    
}

-(void)loadData
{
    [opQueue addOperationWithBlock:^{
        
        
        DebugLog(@"requests url: %@",urlString);
        
        NSString *newString1 = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            NSString *error_msg;
            
            if (signeddataURL1 != nil)
            {
                NSError *error=nil;
                
                json = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                        
                                                       options:kNilOptions
                        
                                                         error:&error];
                DebugLog(@"json returns: %@",json);
                
                
                NSDictionary *response=[json objectForKey:@"response"];
                
                error_msg= [response objectForKey:@"status"];
                DebugLog(@"err  %@",[response objectForKey:@"status"]);
                if ([error_msg isEqualToString:@"success"])
                {
                    [self stopLoading];
                    
                    termsLabel.text = [[json objectForKey:@"response"] objectForKey:@"subject_body"];
                    
                }
                else
                {
                    [self stopLoading];
                }
            }
            else
            {
                [self stopLoading];
                
                alert = [[UIAlertView alloc] initWithTitle:@"error in server connection!"
                                                   message:Nil
                                                  delegate:self
                                         cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                [alert show];
            }
            
        }];
        
    }];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Terms"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
