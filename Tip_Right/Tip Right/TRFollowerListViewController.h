//
//  TRFollowerListViewController.h
//  Tip Right
//
//  Created by intel on 11/07/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import "TRGlobalMethods.h"

@interface TRFollowerListViewController : TRGlobalMethods
@property (strong, nonatomic) IBOutlet UITableView *followerTable;
@property (nonatomic,retain)NSString *userId;
@end
