 //
//  TRGlobalHeader.h
//  Tip Right
//
//  Created by santanu on 29/05/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#ifndef Tip_Right_TRGlobalHeader_h
#define Tip_Right_TRGlobalHeader_h


#define  IsIphone5 (([[UIScreen mainScreen] bounds].size.height)==568)?true:false
#define  IsIphone6 (([[UIScreen mainScreen] bounds].size.height)==667)?true:false
#define  IsIphone6plus (([[UIScreen mainScreen] bounds].size.height)==736)?true:false
#define  IsIphone4 (([[UIScreen mainScreen] bounds].size.height)==480)?true:false

#define  FULLHEIGHT (float)[[UIScreen mainScreen] bounds].size.height
#define  FULLWIDTH  (float)[[UIScreen mainScreen] bounds].size.width


//Color
#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

//Font
#define PantonBLACK @"Panton-BlackCaps"
#define PantonBLACKItalic @"Panton-BlackitalicCaps"
#define PantonLIGHT @"Panton-LightCaps"
#define PantonLIGHTItalic @"Panton-LightitalicCaps"
#define OPENSANS @"OpenSans-Regular"
#define OPENSANSLIGHT @"OpenSans-Light_1"
#define HELVETICAPro @"HelveticaNeueLTPro-Bd_0"
#define HELVETICAProLight @"HelveticaNeueLTPro-Lt"


//key
#define UDUSERID @"userid"
#define UDUSERNAME @"userName"
#define DEVICETOKEN @"deviceToken"
#define ISLOGGED @"isLogged"

//size
#define LandingHeaderHeight 100.0f
#define OtherHeaderHeight 45.0f
#define StatusBarHeight (float)[[UIApplication sharedApplication]statusBarFrame].size.height

//url API

#define GLOBALAPI @"http://www.esolz.co.in/lab3/tipright"

#define INDEX @"index.php"
#define DASHBOARD @"dashboard_ios"
#define USERSEARCH @"usersearch_ios"
#define FOLLOW @"follow_ios"
#define EDITPROFILE @"editprofile_ios"
#define PROFILE @"profile_ios"
#define NOTIFICATION @"notification_ios"
#define MESSAGELIST @"message_ios"

//Log
#ifdef DEBUG

#define DebugLog(...) NSLog(__VA_ARGS__)

#else

#define DebugLog(...) while(0)

#endif

//Degree To Radian
#define DEGREES_TO_RADIANS(angle) ((angle) / 180.0 * M_PI)

//Extra key
#define ANIMATION_DURATION				0.3f

#endif
//Custom Blocks



