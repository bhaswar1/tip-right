//
//  TRChangePasswordViewController.m
//  Tip Right
//
//  Created by intel on 25/06/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import "TRChangePasswordViewController.h"

@interface TRChangePasswordViewController ()
{
    UIAlertView *alert;
}

@end

@implementation TRChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self Header:self.view];
    [self Footer:self.view];
    self.view.backgroundColor = [UIColor colorWithRed:(248.0f/255.0f) green:(247.0f/255.0f) blue:(243.0f/255.0f) alpha:1.0f];
}

-(void)viewDidAppear:(BOOL)animated
{
    _save_btn.layer.cornerRadius = 7;
    _oldPass_Field.layer.cornerRadius = 5;
    _NewPass_Field.layer.cornerRadius = 5;
    _confirmPass_Field.layer.cornerRadius = 5;
}

-(NSString*) encodeToPercentEscapeString:(NSString *)string {
    return (NSString *)
    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                              (CFStringRef) string,
                                                              NULL,
                                                              (CFStringRef) @"!*'();:@&=+$,/?%#[]",
                                                              kCFStringEncodingUTF8));
}

- (IBAction)save:(id)sender {
    
    [_oldPass_Field resignFirstResponder];
    [_NewPass_Field resignFirstResponder];
    [_confirmPass_Field resignFirstResponder];
    

    
         NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
         NSString *oldpass = [self encodeToPercentEscapeString:_oldPass_Field.text];
         NSString *newpass = [self encodeToPercentEscapeString:_NewPass_Field.text];
         
         
         if ([[_oldPass_Field.text stringByTrimmingCharactersInSet:whitespace] length] == 0)
         {
             [self stopLoading];
             alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Your Old Password!"
                                                message:Nil
                                               delegate:self
                                      cancelButtonTitle:@"OK" otherButtonTitles:Nil, nil];
             [alert show];
             
         }
         else if ([[_oldPass_Field.text stringByTrimmingCharactersInSet:whitespace] length] < 6 ||[[_oldPass_Field.text stringByTrimmingCharactersInSet:whitespace] length] > 20 )
         {
             alert = [[UIAlertView alloc] initWithTitle:@"Enter password within a range of 6 to 20 characters"
                                                message:Nil
                                               delegate:self
                                      cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
             [alert show];
         }
         
         
         else if ([[_NewPass_Field.text stringByTrimmingCharactersInSet:whitespace] length] == 0)
         {
             [self stopLoading];
             alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Your New Password!"
                                                message:Nil
                                               delegate:self
                                      cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
             [alert show];
             
         }
         
         else if ([[_NewPass_Field.text stringByTrimmingCharactersInSet:whitespace] length] < 6 ||[[_NewPass_Field.text stringByTrimmingCharactersInSet:whitespace] length] > 20 )
         {
             alert = [[UIAlertView alloc] initWithTitle:@"Enter password within a range of 6 to 20 characters"
                                                message:Nil
                                               delegate:self
                                      cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
             [alert show];
         }
     
         
         else if ([[_confirmPass_Field.text stringByTrimmingCharactersInSet:whitespace] length] == 0)
         {
             [self stopLoading];
             alert = [[UIAlertView alloc] initWithTitle:@"Please Confirm Your New Password"
                                                message:Nil
                                               delegate:self
                                      cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
             [alert show];
             
         }
         else if ([[_confirmPass_Field.text stringByTrimmingCharactersInSet:whitespace] length] > 0 && [[_confirmPass_Field.text stringByTrimmingCharactersInSet:whitespace] length] < 6)
         {
             [self stopLoading];
             alert = [[UIAlertView alloc] initWithTitle:@"Confirm Password field should have minimum 6 characters"
                                                message:Nil
                                               delegate:self
                                      cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
             [alert show];
             
         }
         
         else if(![_confirmPass_Field.text isEqualToString:_NewPass_Field.text])
         {
             [self stopLoading];
             alert =[[UIAlertView alloc]initWithTitle:@"Password & Confirm Password Did Not Match!" message:Nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
             
             [alert show];
             
             return;
         }
         else
         {
             
                  [self startLoading:self.view];
             
             NSString *urlString1 =[NSString stringWithFormat:@"%@/%@/changepassword_ios/change_password?user_id=%@&old_password=%@&new_password=%@",GLOBALAPI,INDEX,[[NSUserDefaults standardUserDefaults]valueForKey:UDUSERID ],oldpass,newpass];
             
             DebugLog(@"deny url: %@",urlString1);
             NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
             
             NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
             if (signeddataURL1 != nil)
             {
                 
                 [self stopLoading];
                 
                 
                 NSError *error=nil;
                 NSDictionary *json_deny = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                                                                           options:kNilOptions
                                                                             error:&error];
                 DebugLog(@"deny json returns: %@",json_deny);
                 NSString *errornumber_reqcount= [NSString stringWithFormat:@"%@",[json_deny objectForKey:@"errorno"]];
                 if (![errornumber_reqcount isEqualToString:@"0"])
                 {
                     NSString *err_str_reqcount = [[json_deny objectForKey:@"response"] objectForKey:@"message"];
                     [self stopLoading];
                     alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:err_str_reqcount
                                                       delegate:self
                                              cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                     [alert show];
                     
                 }
                 
                 if ([[json_deny objectForKey:@"success"]intValue] == 1)
                 {
                     [self stopLoading];
                     alert = [[UIAlertView alloc] initWithTitle:@"Changes have been Successfully Saved!"
                                                        message:nil
                                                       delegate:self
                                              cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                     [alert show];
                     
                         [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"keep"];
                     
                     CATransition *transition = [CATransition animation];
                     
                     transition.duration = 0.4f;
                     
                     transition.type = kCATransitionFade;
                     
                     [[self navigationController].view.layer addAnimation:transition forKey:nil];
                     [self.navigationController popViewControllerAnimated:YES];
                 }
             }
         }
         
 
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
