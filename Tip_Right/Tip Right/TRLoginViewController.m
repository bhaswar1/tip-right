//
//  TRLoginViewController.m
//  Tip Right
//
//  Created by santanu on 30/05/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import "TRLoginViewController.h"
#import "AppDelegate.h"
#import "TRGlobalHeader.h"
#import "UIImageView+WebCache.h"
#import "TRForgetPasswordViewController.h"



@interface TRLoginViewController ()<UITextFieldDelegate>
{
    BOOL keep;
    UIAlertView *alert;
    IBOutlet UIView *mainView;
    NSDictionary *json;
    UIActivityIndicatorView *act;
    NSOperationQueue *opQueue;
    AppDelegate *del;
}
@property (weak, nonatomic) IBOutlet UIImageView *backImage;
@property (weak, nonatomic) IBOutlet UIButton *forgetPass;
@end

@implementation TRLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
  
    
//    if([[NSUserDefaults standardUserDefaults]boolForKey:@"keep"])
//    {
//    
//        UIViewController *afterLogin = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"Home"];
//        [[self navigationController ]pushViewController:afterLogin animated:YES];
//    }
    
    
   del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    // Do any additional setup after loading the view.
    opQueue=[[NSOperationQueue alloc]init];
    
    [_backImage.layer setZPosition:-999];
  
    
    [self loadBackImage];
    
    
    
    
}

-(void)loadBackImage
{
//http://esolz.co.in/lab3/tipright/index.php/app_ios/appbackgroundimage
    
    
   // [self startLoading:self.view];
    
    
    [opQueue addOperationWithBlock:^{
        
        NSString *urlString =[NSString stringWithFormat:@"http://www.esolz.co.in/lab3/tipright/index.php/app_ios/appbackgroundimage"]; //&thumb=true
        
        DebugLog(@"requests url: %@",urlString);
        
        NSString *newString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSData *dataURL =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            
            //[self stopLoading];
            
            
            NSString *error_msg;
      
            if (dataURL != nil)
            {
              
                NSError *error=nil;
                
                json = [NSJSONSerialization JSONObjectWithData:dataURL //1
                        
                                                       options:kNilOptions
                        
                                                         error:&error];
                DebugLog(@"json returns: %@",json);
                
                
               
                
                error_msg= [json objectForKey:@"status"];
                DebugLog(@"err  %@",[json objectForKey:@"status"]);
                if ([error_msg isEqualToString:@"Success"])
                {
                
                    NSDictionary *dict=[json objectForKey:@"data"];
                    
                    
                    [_backImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[dict objectForKey:@"appimage"]]] placeholderImage:[UIImage imageNamed:@"bg"]];
                     
                    
                    
             
                    
                }
                else
                {
                   
//                    alert = [[UIAlertView alloc] initWithTitle:@""
//                                                       message:[json objectForKey:@"message"]
//                                                      delegate:self
//                                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
//                    [alert show];
                }
            }
            else
            {
                
               // alert = [[UIAlertView alloc] initWithTitle:@"error in server connection!"
               //                                    message:Nil
                 //                                 delegate:self
                   //                      cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                //[alert show];
            }
            
        }];
        
    }];



}

- (IBAction)forgetClicked:(id)sender {
    
    UIViewController *signUpVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"TRForget"];
    [[self navigationController ]pushViewController:signUpVC animated:YES];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    
    
    
    
   
    
    
    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width,scrollView.frame.size.height+100)];
    scrollView.contentSize = CGSizeMake(1, scrollView.frame.size.height+100);
    scrollView.contentOffset=CGPointMake(0, 0);
    
    
    
    emailField.delegate = self;
    passwordField.delegate = self;
    
    emailField.autocorrectionType = UITextAutocorrectionTypeNo;
    passwordField.autocorrectionType = UITextAutocorrectionTypeNo;
  
    [emailField setValue:[UIColor whiteColor]
                    forKeyPath:@"_placeholderLabel.textColor"];
    [passwordField setValue:[UIColor whiteColor]
              forKeyPath:@"_placeholderLabel.textColor"];
    

//    [emailField setText:@"pronoy.sarkar@esolzmail.com"];
//    [passwordField setText:@"1234567"];
//
    UIFont *font2,*font1;
    
    if(IsIphone6||IsIphone6plus)
    {
        font2 = [UIFont fontWithName:@"HelveticaNeue-Medium" size:18.0f];
        font1 = [UIFont fontWithName:@"HelveticaNeue-Light"  size:15.0f];
    }
    else{
       font2 = [UIFont fontWithName:@"HelveticaNeue-Medium" size:15.0f];
       font1 = [UIFont fontWithName:@"HelveticaNeue-Light"  size:12.0f];
    }
   
    NSDictionary *dict1 = @{NSUnderlineStyleAttributeName:@(NSUnderlineStyleNone),NSFontAttributeName:font1}; // Added line
    NSDictionary *dict2 = @{NSUnderlineStyleAttributeName:@(NSUnderlineStyleNone),NSFontAttributeName:font2}; // Added line
    
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] init];
    [attString appendAttributedString:[[NSAttributedString alloc] initWithString:@"Don't have an account?"    attributes:dict1]];
    [attString appendAttributedString:[[NSAttributedString alloc] initWithString:@" Sign Up"      attributes:dict2]];
    [Sign_up setAttributedTitle:attString forState:UIControlStateNormal];
    
    
    passwordField.secureTextEntry = YES;
    
    
}
- (IBAction)signUpClick:(id)sender {
    

    UIViewController *signUpVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"signUp"];
  [[self navigationController ]pushViewController:signUpVC animated:YES];
}

-(IBAction)keep
{
    if (!keep) {
        keepMe.image = [UIImage imageNamed:@"check-box1"];
        keep = TRUE;
        
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"keep"];
    }
    else
    {
        keepMe.image = [UIImage imageNamed:@"check-box"];
        keep = FALSE;
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"keep"];
    }
    
}

-(BOOL)isNumeric:(NSString*)inputString{
    BOOL isValid = NO;
    NSCharacterSet *alphaNumbersSet = [NSCharacterSet decimalDigitCharacterSet];
    NSCharacterSet *stringSet = [NSCharacterSet characterSetWithCharactersInString:inputString];
    isValid = [alphaNumbersSet isSupersetOfSet:stringSet];
    return isValid;
}

- (IBAction)Login:(id)sender {

    [emailField resignFirstResponder];
    [passwordField resignFirstResponder];
    mainView.frame = CGRectOffset( mainView.frame, 0, 0 );
    
     NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    
    if ([emailField.text isEqualToString:@""]) {
        
        alert = [[UIAlertView alloc] initWithTitle:@"Please Enter email!"
                                           message:Nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        
        [alert show];
    }
    
//    else
//    {
//        
//    if ( [self isNumeric:emailField.text]) {
//        
//        if ([[emailField.text stringByTrimmingCharactersInSet:whitespace] length] <10 || [[emailField.text stringByTrimmingCharactersInSet:whitespace] length] > 10 || !([emailField.text rangeOfCharacterFromSet:notDigits].location == NSNotFound))
//        {
//            alert = [[UIAlertView alloc] initWithTitle:@"Please Enter a Valid Mobile Number"
//                                               message:Nil
//                                              delegate:self
//                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
//            [alert show];
//        }
//        else if ([[passwordField.text stringByTrimmingCharactersInSet:whitespace] length] == 0)
//        {
//            alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Your Password!"
//                                               message:Nil
//                                              delegate:self
//                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
//            [alert show];
//        }
//        else
//        {
//            alert = [[UIAlertView alloc] initWithTitle:@"Successfull Login!"
//                                               message:Nil
//                                              delegate:self
//                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
//            [alert show];
//        }
//
//        
//        
//        }
    
    else  if ([[emailField.text stringByTrimmingCharactersInSet:whitespace] length] > 0 && ![self NSStringIsValidEmail:emailField.text])
        {
        alert = [[UIAlertView alloc] initWithTitle:@"Please Enter a Valid Email!"
                                           message:Nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        
        [alert show];
        
        }
    else if ([[passwordField.text stringByTrimmingCharactersInSet:whitespace] length] == 0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Your Password!"
                                           message:Nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
    }
    else
    {
        [self startLoading:self.view];
        
        [opQueue addOperationWithBlock:^{
        
             NSString *urlString1 =[NSString stringWithFormat:@"http://www.esolz.co.in/lab3/tipright/index.php/login_ios?email=%@&password=%@&device_token=%@",emailField.text,passwordField.text, [[NSUserDefaults standardUserDefaults]valueForKey:DEVICETOKEN]]; //&thumb=true
            
             DebugLog(@"requests url: %@",urlString1);
             
             NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
             
             NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
           
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                
              NSString *error_msg;
                
             if (signeddataURL1 != nil)
             {
                 NSError *error=nil;
                 
                 json = [NSJSONSerialization JSONObjectWithData:signeddataURL1 //1
                         
                                                        options:kNilOptions
                         
                                                          error:&error];
                 DebugLog(@"json returns: %@",json);
                 
                 
                 NSDictionary *response=[json objectForKey:@"response"];
                 
                 error_msg= [response objectForKey:@"status"];
                 DebugLog(@"err  %@",[response objectForKey:@"status"]);
                 if ([error_msg isEqualToString:@"Success"])
                 {
                      [self stopLoading];
                     
                     
                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"keep"];
                     
                     [[NSUserDefaults standardUserDefaults] setObject:[response objectForKey:@"background_img"] forKey:@"cover_pic"];
                     
                     [[NSUserDefaults standardUserDefaults] setObject:[response objectForKey:@"bio"] forKey:@"bio"];
                     
                     [[NSUserDefaults standardUserDefaults] setObject:[response objectForKey:@"email"] forKey:@"email"];
                    
                     
                     [[NSUserDefaults standardUserDefaults] setObject:[response objectForKey:@"first_name"] forKey:@"first_name"];
                      [[NSUserDefaults standardUserDefaults] setObject:[response objectForKey:@"last_name"] forKey:@"last_name"];
                     
                     [[NSUserDefaults standardUserDefaults] setObject:[response objectForKey:@"profileimage"] forKey:@"profile_pic"];
                     
                     
                     [[NSUserDefaults standardUserDefaults] setObject:[response objectForKey:@"sex"] forKey:@"sex"];
                     
                     [[NSUserDefaults standardUserDefaults] setObject:[response objectForKey:@"username"] forKey:@"username"];
                     
                     [[NSUserDefaults standardUserDefaults] setObject:[response objectForKey:@"website"] forKey:@"website"];
                     
                     
                     [[NSUserDefaults standardUserDefaults]setValue:[response valueForKey:@"userid"] forKey:UDUSERID];
                     
                     
                 
                     
                     del.userid = [response objectForKey:@"userid"];
                     [del callAfterSixtySecond];
                     
                     if([[response objectForKey:@"login"]boolValue])
                     {
                     UIViewController *afterLogin = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"Home"];
                     [[self navigationController ]pushViewController:afterLogin animated:YES];
                     }
                     else{
                         UIViewController *afterLogin = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"TRSearch"];
                         [[self navigationController ]pushViewController:afterLogin animated:YES];
                     
                     }
                     
                 }
                 else
                 {
                     [act removeFromSuperview];
                     [self stopLoading];
                     alert = [[UIAlertView alloc] initWithTitle:@"User Not Found!"
                                                        message:Nil
                                                       delegate:self
                                              cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                     [alert show];
                 }
             }
             else
             {
                 [self stopLoading];
                 
                 alert = [[UIAlertView alloc] initWithTitle:@"error in server connection!"
                                                    message:Nil
                                                   delegate:self
                                          cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                 [alert show];
             }
             
            }];
            
        }];
        
    }
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [UIView animateWithDuration:0.5 animations:^{
        
        mainView.frame = CGRectOffset( mainView.frame, 0, -70 );
        
    }];
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{

    [UIView animateWithDuration:0.5 animations:^{
        
        mainView.frame = CGRectOffset( mainView.frame, 0, 70 );
        
    }];

}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return  YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [emailField resignFirstResponder];
    [passwordField resignFirstResponder];
    
    return YES;
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    checkString = [checkString lowercaseString];
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:checkString];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
