//
//  TRHomeViewController.m
//  Tip Right
//
//  Created by intel on 02/06/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import "TRHomeViewController.h"
#import "TRGlobalHeader.h"
#import "TROwnProfileViewController.h"
#import "TRHomeTableViewCell.h"


@interface TRHomeViewController ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
{
    
    NSOperationQueue *operationQueue,*innerOperationQueue;
    NSDictionary *result,*result2,*result3;
    NSMutableArray *response,*response2,*response3,*DisplayResponse,*commentArr;
    NSArray *tipImageArray;
    int page,dataCount,totalCount,commentPage,commentCount,totalCommentCount;
    BOOL isReloadComplete;

    
    UIView *popupView,*blackView;
    UITableView *CommentTable;
    
    UILabel *timeLabel,*tipType,*currency;
    UIImageView *logo,*time_div,*tipType_div,*priceDiv;
    NSString *dateTime;
 
    BOOL isTime,isType,isPrice;
    
    UIRefreshControl *refreshControl;
    int cross_index;
    
    
}

@property (strong, nonatomic) IBOutlet UILabel *NoFeed_Label;
@property (strong, nonatomic) IBOutlet UITableView *contentTable;

@end

@implementation TRHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self Header:self.view];
    [self Footer:self.view];
    
    [_contentTable setHidden:YES];
    
    DisplayResponse=[[NSMutableArray alloc]init];
    
    dataCount=0;
    totalCount=0;
    
    page=0;
    
    tipImageArray=@[@"horse",@"ball",@"hand",@"football"];
    
    operationQueue =[[NSOperationQueue alloc]init];
    innerOperationQueue =[[NSOperationQueue alloc]init];
    
    
   
    
    UITableViewController *refreshCon=[[UITableViewController alloc]init];
    
    [refreshCon setTableView:_contentTable];
    
    
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    refreshControl.backgroundColor = [UIColor orangeColor];
    refreshControl.tintColor = [UIColor whiteColor];
    [refreshCon setRefreshControl:refreshControl];
    
    

    
}

-(void)viewDidAppear:(BOOL)animated
{
    [self loadData];
    
}


-(void)loadData
{
    [self startLoading:self.view];
    
    operationQueue = [[NSOperationQueue alloc]init];
    
    [operationQueue addOperationWithBlock:^{
        NSString *urlString=[NSString stringWithFormat:@"%@/%@/%@/load_feeds?id=%@&page=%d&limit=4",GLOBALAPI,INDEX, DASHBOARD,[[NSUserDefaults standardUserDefaults]valueForKey:UDUSERID ],page];
        
        
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        DebugLog(@"url:%@",urlString);
        result=[[NSDictionary alloc]init];
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
      
            if(urlData == nil)
                
             
                
            {
                [self stopLoading];
                
                [[[UIAlertView alloc]initWithTitle:@"Error!!!" message:@"error in internet Connection" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                
            }
            else{
                
                [self stopLoading];
                
                result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                DebugLog(@"result is:%@",result);
                
            
                
                if([[result valueForKey:@"status"]isEqualToString:@"Error" ])
                {
                    response=[result valueForKey:@"response"];
                    
                    _contentTable.hidden = YES;
                    _NoFeed_Label.hidden = NO;
                    _NoFeed_Label.text = [response valueForKey:@"message"];
                    
                }
                else{
                    
                    response=[[result valueForKey:@"response"]valueForKeyPath:@"data"];
                    
                    DebugLog(@"%lu",(unsigned long)[response count]);
                    
                    totalCount=[[result valueForKey:@"total_feeds"]intValue];
                    
                    dataCount+= (int)[response count];
                    
                    for(NSDictionary *rowDict in response)
                    {
                        [DisplayResponse addObject:rowDict];
                        
                    }
                    
                    DebugLog(@"Display DATA:%@",DisplayResponse);
                    
                    _contentTable.delegate = self;
                    _contentTable.dataSource = self;
                    
                    _contentTable.hidden = NO;
                    
                    [_contentTable reloadData];
                    
                    [_contentTable setUserInteractionEnabled:YES];

                    
                    
                    
                }
                
            }
            
        }];
        
    }];
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag==22)
    {
        return 70;
    }
    
    NSArray *tipArr=[[NSArray alloc]init];
    
    tipArr=[[DisplayResponse objectAtIndex:indexPath.row]objectForKey:@"tip"];
    
    
    int height;
    height=0;
    float extraSpace=0;
    
    for (NSDictionary *innerDict in tipArr)
    {
        
        NSInteger MAX_HEIGHT = 2000;
        
        isTime=NO;
        isType=NO;
        isPrice=NO;
        
  
   
        
        UILabel *txtLbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-50.0/375.0*self.view.frame.size.width, MAX_HEIGHT)];
        
        txtLbl.text= [innerDict valueForKey:@"description"];
        txtLbl.numberOfLines = 0;
        
        
        txtLbl.font = [UIFont fontWithName:@"Helvetica Neue" size:12.0];
        
        
        //[cell2.userMsg sizeToFit];
        
        CGSize size = [ txtLbl.text sizeWithFont:txtLbl.font
                               constrainedToSize:CGSizeMake(self.view.frame.size.width-50.0/375.0*self.view.frame.size.width, MAXFLOAT)
                                   lineBreakMode:NSLineBreakByWordWrapping];
        CGRect  labelFrame =  txtLbl.frame;
        labelFrame.size.height = size.height;
        txtLbl.frame = labelFrame;
        
        
        extraSpace+=size.height;
        
        dateTime = [[innerDict valueForKey:@"date_time"]stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        if(![dateTime isEqualToString:@""])
        {
            isTime=YES;
            height+=35;
            
            
        }
        if([[[innerDict valueForKey:@"tip_type"]stringByReplacingOccurrencesOfString:@" " withString:@""]  length]>0 )
        {
            isTime=YES;
             height+=35;
            
        }
        
        if([[[innerDict valueForKey:@"price"]stringByReplacingOccurrencesOfString:@" " withString:@""]  length]>0 )
        {
            isPrice=YES;
             height+=35;
            
        }
        
        if(isType || isTime || isPrice)
        {
          
            
        }
        else{
            
             height+=35;
            
        }
        
        
        height+=50;
    }
    
    
    if ([[[DisplayResponse objectAtIndex:indexPath.row]objectForKey:@"tip_image"] length]>0) {
        
       return 430.0/568.0*self.view.frame.size.height+height+extraSpace;
    }
    else
    {
       return 225.0/568.0*self.view.frame.size.height+height+extraSpace;
    }
    

    
    return 430.0/568.0*self.view.frame.size.height+height+extraSpace;
    
    //return 490+((int)tipArr.count*50)+extraSpace;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if(tableView.tag==22)
    {
        
        return [commentArr count];
        
    }
    
    return [DisplayResponse count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [NSString stringWithFormat:@"cellIdentifier%ld%ld",(long)indexPath.section,(long)indexPath.row];
    
    TRHomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    DebugLog(@"CELL ALLOC");
    cell = [[TRHomeTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor whiteColor];
    
    
    if(tableView.tag==22)
    {
        UIView *commentView=[[UIView alloc]initWithFrame:CGRectMake(0, 0,tableView.frame.size.width, tableView.frame.size.height-5)];
        
        UIImageView *otherProfile_img = [[UIImageView alloc] init];
        otherProfile_img.frame = CGRectMake(10, 10, 40, 40);
        
        [otherProfile_img sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[commentArr objectAtIndex:indexPath.row]objectForKey:@"profileimage"] ]] placeholderImage:nil];
        
        otherProfile_img.layer.cornerRadius = otherProfile_img.frame.size.width/2;
        otherProfile_img.clipsToBounds = YES;
        
//        CALayer *imageLayer = otherProfile_img.layer;
//        [imageLayer setCornerRadius:20.5];
//        [imageLayer setBorderWidth:0.1];
//        [imageLayer setMasksToBounds:YES];
        
        
        [commentView addSubview:otherProfile_img];
        
        UILabel *otherName = [[UILabel alloc] init];
        otherName.frame = CGRectMake(otherProfile_img.frame.origin.x+otherProfile_img.frame.size.width+10, 19, 150, 15);
        otherName.text = [NSString stringWithFormat:@"%@ : ",[[commentArr objectAtIndex:indexPath.row]objectForKey:@"name"]];
        otherName.textColor = [UIColor blackColor];
        otherName.font = [UIFont boldSystemFontOfSize:13.0];
        [commentView addSubview:otherName];
        [otherName sizeToFit];
        
        
        UILabel *commentContent=[[UILabel alloc]initWithFrame:CGRectMake( otherName.frame.origin.x+otherName.frame.size.width+5, 5, [UIScreen mainScreen].bounds.size.width-(otherName.frame.origin.x+otherName.frame.size.width+60),  45)];
        
        [commentContent setText:[NSString stringWithFormat:@"%@",[[commentArr objectAtIndex:indexPath.row]objectForKey:@"comment"]]];
        
        commentContent.textColor = [UIColor blackColor];
        
        commentContent.numberOfLines = 3;
        commentContent.lineBreakMode = YES;
        commentContent.font = [UIFont fontWithName:@"Helvetica Neue" size:10.0];
        
        [commentView addSubview:commentContent];
        
        [cell addSubview:commentView];
        
    }
    else{
        
        DebugLog(@"in row %ld",(long)indexPath.row);
        
        
        UILabel *cellSeparator = [[UILabel alloc] init];
        cellSeparator.frame = CGRectMake(0, 0, self.view.frame.size.width-10, 2);
        cellSeparator.backgroundColor = [UIColor colorWithRed:(248.0f/255.0f) green:(247.0f/255.0f) blue:(243.0f/255.0f) alpha:1.0];
        [cell addSubview:cellSeparator];
        
        
        
        UIButton *profileBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 170, 160)];
        
        [profileBtn addTarget:self action:@selector(viewUser:) forControlEvents:UIControlEventTouchUpInside];
        
        
        [cell addSubview:profileBtn];
        
        
        
        
        
        UIImageView *profile_img = [[UIImageView alloc] init];
        profile_img.frame = CGRectMake(5, 10, 45, 45);
        
        
        [profile_img sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[DisplayResponse objectAtIndex:indexPath.row]objectForKey:@"profileimage"] ]] placeholderImage:nil];
        
        [[NSUserDefaults standardUserDefaults] setObject:[[DisplayResponse objectAtIndex:indexPath.row]objectForKey:@"profileimage"] forKey:@"profilepic"];
       
        profile_img.layer.cornerRadius = profile_img.frame.size.width/2;
        profile_img.clipsToBounds = YES;
        
//        CALayer *imageLayer = profile_img.layer;
//        [imageLayer setCornerRadius:23.5/375.0*self.view.frame.size.width];
//       // [imageLayer setBorderWidth:0.1];
//        [imageLayer setMasksToBounds:YES];
        
        
        [cell addSubview:profile_img];
        
        DebugLog(@"name is:%@",[[DisplayResponse objectAtIndex:indexPath.row]objectForKey:@"user_name"]);
        DebugLog(@"x pos is:%f",profile_img.frame.origin.x+profile_img.frame.size.width+10);
        
        
        UILabel *name = [[UILabel alloc] init];
        name.frame = CGRectMake(profile_img.frame.origin.x+profile_img.frame.size.width+10, 12, 150, 25);
        
        
        name.text = [NSString stringWithFormat:@"%@",[[DisplayResponse objectAtIndex:indexPath.row]objectForKey:@"user_name"]];
        name.textColor = [UIColor blackColor];
        name.font = [UIFont boldSystemFontOfSize:15.0];
        [cell addSubview:name];
        
        
          UIButton *copyBet = [[UIButton alloc] init];
        copyBet.frame = CGRectMake(self.view.frame.size.width-120, 10, 70, 33);
        // [copyBet setBackgroundImage:[UIImage imageNamed:@"copy-ber"] forState:UIControlStateNormal];
        
        [copyBet setBackgroundColor:[UIColor colorWithRed:(255.0f/255.0f) green:(127.0f/255.0f) blue:(0.0f/255.0f) alpha:1]];
        [copyBet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [copyBet.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:12.0f]];
        [copyBet setTitle:@"COPY BET" forState:UIControlStateNormal];
        
        
        CALayer *imageLayer2 = copyBet.layer;
        [imageLayer2 setCornerRadius:15.0/375.0*self.view.frame.size.width];
        
        [imageLayer2 setMasksToBounds:YES];
        
        [copyBet addTarget:self action:@selector(copyBetClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        
        [cell addSubview:copyBet];
        
        
        UIImageView *delete_image = [[UIImageView alloc] init];
        delete_image.frame = CGRectMake(self.view.frame.size.width-35, 17, 20, 20);
        delete_image.image = [UIImage imageNamed:@"close-if"];
        delete_image.layer.cornerRadius = delete_image.frame.size.width/2;
        [cell addSubview:delete_image];
        
        UIButton *delete_cross = [[UIButton alloc] init];
        delete_cross.frame = CGRectMake(self.view.frame.size.width-50, 3, 50, 50);
        
        
      // [delete_cross setBackgroundColor:[UIColor redColor]];
        
        delete_cross.tag = indexPath.row;
        
        [delete_cross addTarget:self action:@selector(tipDelete:) forControlEvents:UIControlEventTouchUpInside];
        
        
        [cell addSubview:delete_cross];
        
        if ([[[DisplayResponse objectAtIndex:indexPath.row] objectForKey:@"user_id"] isEqualToString:[[NSUserDefaults standardUserDefaults]valueForKey:UDUSERID ]]) {
            delete_image.hidden = NO;
            delete_cross.hidden = NO;
        }
        else
        {
            delete_image.hidden = YES;
            delete_cross.hidden = YES;
        }
        
        UIImageView *heart_img = [[UIImageView alloc] init];
        heart_img.frame = CGRectMake(name.frame.origin.x, name.frame.origin.y+name.frame.size.height+5, 16, 15);
        
        DebugLog(@"Like Status:%@",[DisplayResponse objectAtIndex:indexPath.row]);
        
        if([[[DisplayResponse objectAtIndex:indexPath.row]objectForKey:@"like_status"] boolValue])
            heart_img.image = [UIImage imageNamed:@"heart"];
        else
            heart_img.image = [UIImage imageNamed:@"heart-1"];
        
        [cell addSubview:heart_img];
        
        
        UIButton *likeBtn=[[UIButton alloc]initWithFrame:CGRectMake(heart_img.frame.origin.x-5, heart_img.frame.origin.y-7, heart_img.frame.size.width+50, heart_img.frame.size.height+10)];
        
       // [likeBtn setBackgroundColor:[UIColor redColor]];
        
        [likeBtn addTarget:self action:@selector(likeClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        
        [cell addSubview:likeBtn];
        
        
        UILabel *like_label = [[UILabel alloc] init];
        like_label.frame = CGRectMake(heart_img.frame.origin.x+heart_img.frame.size.width+5, heart_img.frame.origin.y+2, 150, 15);
        like_label.font = [UIFont fontWithName:@"Helvetica Neue LT Pro" size:11.0];
        like_label.text = [NSString stringWithFormat:@" %@ people liked",[[DisplayResponse objectAtIndex:indexPath.row]objectForKey:@"like"] ];
        like_label.textColor = [UIColor blackColor];
        [cell addSubview:like_label];
        
        
        NSArray *tipArray=[[NSArray alloc]init];
        
        tipArray=[[DisplayResponse objectAtIndex:indexPath.row]objectForKey:@"tip"];
        
        UILabel *timeAgo = [[UILabel alloc] init];
        timeAgo.frame = CGRectMake(copyBet.frame.origin.x-10, heart_img.frame.origin.y, 90, 20);
        timeAgo.font = [UIFont fontWithName:@"Helvetica Neue" size:13.0];
        [timeAgo setTextAlignment:NSTextAlignmentCenter];
        
        
#pragma mark- time difference calculation
        
        NSDate *now=[NSDate date];
        
        
        NSDateFormatter *formatter2 = [[NSDateFormatter alloc]init];
        [formatter2 setDateFormat:@"yyyy-MM-dd HH:mm"];
        [formatter2 setTimeZone:[NSTimeZone timeZoneWithName:@"Utc"]];
        NSString *nowS=[formatter2 stringFromDate:now];
        
        
        
        NSDate *dateJ = [formatter2 dateFromString:nowS];
        
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
        [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
        
        DebugLog(@"SERVER TIME:%@",[[tipArray objectAtIndex:0]objectForKey:@"added_date"]);
        
        NSDate *date = [formatter dateFromString:[NSString stringWithFormat:@"%@",[[tipArray objectAtIndex:0]objectForKey:@"added_date"]]];
        
        
        
        NSTimeInterval interval = [dateJ timeIntervalSinceDate:date];
        
        int hours = (int)interval / 3600;
        // integer division to get the hours part
        
        int days=hours/24;
        int minutes = (interval - (hours*3600)) / 60; // interval minus hours part (in seconds) divided by 60 yields minutes
        
        
        //        if (days>=365) {
        //
        //            int year = days/365;
        //            [timeAgo setText:[NSString stringWithFormat:@"%d %@",year,@"Years ago"]];
        //
        //        }
        //
        //        else if ((days/30)>0) {
        //
        //            [timeAgo setText:[NSString stringWithFormat:@"%d Months ago",(int)days/30]];
        //        }
        //
        //        else
        
        if ((days/7)>0) {
            
            timeAgo.text = [NSString stringWithFormat:@"%@",[[[tipArray objectAtIndex:0]objectForKey:@"added_date"]  substringWithRange:NSMakeRange(0, 11)]];
            
        }
        
        else  if (hours>0) {
            
            
            
            [timeAgo setText:[NSString stringWithFormat:@"%d %@",hours,@"Hours ago"]];
        }
        else if (minutes>0) {
            DebugLog(@"Minutes:%d",minutes);
            
            [timeAgo setText:[NSString stringWithFormat:@"%d %@",minutes,@"Minutes ago"]];
            
        }
        
        else
        {
            [timeAgo setText:@"Just Now"];
        }
        
        
        timeAgo.textColor = [UIColor grayColor];
        [cell addSubview:timeAgo];
        
        
        
        
        
        
        
      
        
        
        UIImageView *cover = [[UIImageView alloc] init];
        
        if ([[[DisplayResponse objectAtIndex:indexPath.row]objectForKey:@"tip_image"] length]>0) {
               cover.frame = CGRectMake(0, heart_img.frame.origin.y+heart_img.frame.size.height+20, self.view.frame.size.width-10, 205.0/568.0*self.view.frame.size.height);
            
                    [cover sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[DisplayResponse objectAtIndex:indexPath.row]objectForKey:@"tip_image"] ]] placeholderImage:[UIImage imageNamed:@"loading-image"]];
        }
        else
        {
                   cover.frame = CGRectMake(0, heart_img.frame.origin.y+heart_img.frame.size.height+20, self.view.frame.size.width-10,0);
        }
        
     
        

        
        
        [cell addSubview:cover];
        
        int height=26;
        int yPos =cover.frame.origin.y+cover.frame.size.height+20;
      
        
        
        for(NSDictionary * innerDict in tipArray)
        {
            
            int yposInit=yPos;
            
            isTime=NO;
            isType=NO;
            isPrice=NO;
            
            
            
            
            
            timeLabel = [[UILabel alloc] init];
            time_div = [[UIImageView alloc] init];
            tipType = [[UILabel alloc] init];
            tipType_div = [[UIImageView alloc] init];
            currency = [[UILabel alloc] init];
            logo = [[UIImageView alloc] init];
            priceDiv=[[UIImageView alloc]init];
            
            
            
            dateTime = [[innerDict valueForKey:@"date_time"]stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            if(![dateTime isEqualToString:@""])
            {
                
                timeLabel.frame = CGRectMake(25.0/375.0*self.view.frame.size.width+10, yPos+2,self.view.frame.size.width-50.0/375.0*self.view.frame.size.width, height);
                time_div.frame = CGRectMake(25.0/375.0*self.view.frame.size.width+10, timeLabel.frame.origin.y+timeLabel.frame.size.height, timeLabel.frame.size.width,0.5);
                [cell addSubview:timeLabel];
                [cell addSubview:time_div];
                
                
                
                
                isTime=YES;
                
                yPos+=height+5;
                
                
            }
            
            if([[[innerDict valueForKey:@"tip_type"]stringByReplacingOccurrencesOfString:@" " withString:@""]  length]>0 )
            {
                
                
                
                tipType.frame = CGRectMake(25.0/375.0*self.view.frame.size.width+10, yPos+2,self.view.frame.size.width-50.0/375.0*self.view.frame.size.width, height);
                
                tipType_div.frame = CGRectMake(25.0/375.0*self.view.frame.size.width+10, tipType.frame.origin.y+tipType.frame.size.height, tipType.frame.size.width,0.5);
                
                [cell addSubview:tipType];
                [cell addSubview:tipType_div];
                isType=YES;
                
                yPos+=height+5;
                
            }
            
            
            
            if([[[innerDict valueForKey:@"price"]stringByReplacingOccurrencesOfString:@" " withString:@""]  length]>0 )
            {
                
                
                currency.frame = CGRectMake(25.0/375.0*self.view.frame.size.width+10, yPos+2,self.view.frame.size.width-50.0/375.0*self.view.frame.size.width, height);
                
                
                
                priceDiv.frame = CGRectMake(25.0/375.0*self.view.frame.size.width+10, currency.frame.origin.y+currency.frame.size.height, currency.frame.size.width,0.5 );
                
                [cell addSubview:currency];
                [cell addSubview:priceDiv];
                
                isPrice=YES;
                
                yPos+=height+5;
                
            }
            
            
  
            
            
            if(isType || isTime || isPrice)
            {
                logo.frame = CGRectMake(0/375.0*self.view.frame.size.width+5, yposInit+((yPos-yposInit)/2)-14.0/375.0*self.view.frame.size.width, 25.0/375.0*self.view.frame.size.width, 28.0/667.0*self.view.frame.size.height);
                
                
                [cell addSubview:logo];
            }
            else{
                
                logo.frame = CGRectMake(self.view.frame.size.width/2-13.0/375.0*self.view.frame.size.width, yPos-5.0/667.0*self.view.frame.size.height, 25.0/375.0*self.view.frame.size.width, 28.0/667.0*self.view.frame.size.height);
                
                
                [cell addSubview:logo];
                
                UIImageView *logoDiv;
                
                logoDiv=[[UIImageView alloc]init];
                
                logoDiv.frame = CGRectMake(0, logo.frame.origin.y+logo.frame.size.height+2, self.view.frame.size.width,0.5 );
                
                //logoDiv.image = [UIImage imageNamed:@"divider"];
                [logoDiv setBackgroundColor:[UIColor lightGrayColor]];
                
                
                [cell addSubview:logoDiv];
                
                yPos+=height+5;
            }
            
            
            UILabel *descriptionLabel=[[UILabel alloc]initWithFrame:CGRectMake(25.0/375.0*self.view.frame.size.width+10, yPos+5, self.view.frame.size.width-50.0/375.0*self.view.frame.size.width, 0)];
            
            descriptionLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:11.0];
            descriptionLabel.text = [NSString stringWithFormat:@"%@",[innerDict valueForKey:@"description"] ];
            descriptionLabel.textAlignment = NSTextAlignmentCenter;
            descriptionLabel.textColor = [UIColor blackColor];
    
            
            
            descriptionLabel.numberOfLines = 0;
            
            
            
            //[cell2.userMsg sizeToFit];
            
            CGSize size = [ descriptionLabel.text sizeWithFont:descriptionLabel.font
                                             constrainedToSize:CGSizeMake(descriptionLabel.frame.size.width, MAXFLOAT)
                                                 lineBreakMode:NSLineBreakByWordWrapping];
            CGRect  labelFrame =  descriptionLabel.frame;
            
            labelFrame.size.height = size.height;
            descriptionLabel.frame = labelFrame;
            
            [cell addSubview:descriptionLabel];
            
            
            timeLabel.font = [UIFont fontWithName:@"Helvetica Neue LT Pro" size:13.0];
            
            dateTime = [[innerDict valueForKey:@"date_time"]stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            if(![dateTime isEqualToString:@""])
            {
                DebugLog(@"DATE TIME:%@",dateTime);
                timeLabel.text = [NSString stringWithFormat:@"%@",[innerDict valueForKey:@"date_time"]];
            }
            timeLabel.textAlignment = NSTextAlignmentCenter;
            timeLabel.textColor = [UIColor blackColor];
            
            
            
            
            // time_div.image = [UIImage imageNamed:@"divider"];
            [time_div setBackgroundColor:[UIColor lightGrayColor]];
            
            
            
            tipType.font = [UIFont fontWithName:@"Helvetica Neue LT Pro" size:13.0];
            
            
            tipType.text =[innerDict valueForKey:@"tip_type"] ;
            
            tipType.textAlignment = NSTextAlignmentCenter;
            tipType.textColor = [UIColor blackColor];
            
            
            
            //tipType_div.image = [UIImage imageNamed:@"divider"];
            [tipType_div setBackgroundColor:[UIColor lightGrayColor]];
            
            
            
            currency.font = [UIFont fontWithName:@"Helvetica Neue LT Pro" size:13.0];
            currency.text = [NSString stringWithFormat:@"%@",[innerDict valueForKey:@"price"] ];
            currency.textAlignment = NSTextAlignmentCenter;
            currency.textColor = [UIColor blackColor];
            
            // priceDiv.image = [UIImage imageNamed:@"divider"];
            [priceDiv setBackgroundColor:[UIColor lightGrayColor]];
            
            
            if(![[innerDict valueForKey:@"tip_status"] isEqualToString:@""])
            {
                logo.image = [UIImage imageNamed:[tipImageArray objectAtIndex:[[innerDict valueForKey:@"tip_status"] integerValue]-1]];
                //[cell addSubview:logo];
            }
            
            
            
            
            
            
            
            
            
            
            yPos=yPos+size.height+50;
            
            // yPos=yPos+35;
        }
        
        UILabel *comment_div = [[UILabel alloc] init];
        comment_div.frame = CGRectMake(0, yPos, self.view.frame.size.width-10, 0.5f);
        comment_div.backgroundColor = [UIColor grayColor];
        comment_div.alpha = 0.2f;
        [cell addSubview:comment_div];
        
        UITextField *commentField = [[UITextField alloc] init];
        commentField.frame = CGRectMake(0, yPos+1, self.view.frame.size.width-60, 37);
        commentField.backgroundColor = [UIColor whiteColor];
        commentField.delegate = self;
        commentField.autocorrectionType=UITextAutocorrectionTypeNo;
        
        commentField.font = [UIFont fontWithName:@"Helvetica Neue LT Pro" size:15.0];
        [cell addSubview:commentField];
        
        commentField.placeholder = @"Comment";
        [commentField setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
        commentField.layer.sublayerTransform = CATransform3DMakeTranslation(20.f, 4.0f, 0.0f);
        
      commentField.tag=indexPath.row;
        
        UIImageView *send_img = [[UIImageView alloc] init];
        send_img.frame = CGRectMake(commentField.frame.origin.x+commentField.frame.size.width+20, commentField.frame.origin.y+10, 18, 19);
        send_img.image = [UIImage imageNamed:@"icon1"];
        [cell addSubview:send_img];
        
        UIButton *send_btn = [[UIButton alloc] init];
        send_btn.frame = CGRectMake(commentField.frame.origin.x+commentField.frame.size.width+20, commentField.frame.origin.y+10, 18, 19);
        
       send_btn.tag=indexPath.row;
        
        
        DebugLog(@"SendTag:%ld, CommentTag:%ld",(long)send_btn.tag,(long)commentField.tag);
        
        
        
        [send_btn addTarget:self action:@selector(sendClick:) forControlEvents:UIControlEventTouchUpInside];
        
        [send_btn setUserInteractionEnabled:NO];
      
        
        
        
        [cell addSubview:send_btn];
        
        UILabel *cell_div = [[UILabel alloc] init];
        cell_div.frame = CGRectMake(0, commentField.frame.origin.y+commentField.frame.size.height+5, self.view.frame.size.width-10, 2);
        cell_div.backgroundColor = [UIColor colorWithRed:(248.0f/255.0f) green:(247.0f/255.0f) blue:(243.0f/255.0f) alpha:1.0];
        [cell addSubview:cell_div];
        
        
        [innerOperationQueue addOperationWithBlock:^{
            NSString *urlString2=[NSString stringWithFormat:@"%@/%@/%@/load_comments?tipid=%@&page=%d&limit=4&loggedinid=%@",GLOBALAPI,INDEX, DASHBOARD,[[DisplayResponse objectAtIndex:indexPath.row]objectForKey:@"count_tip_id"] ,0,[[NSUserDefaults standardUserDefaults] valueForKey:UDUSERID]];
            
            DebugLog(@"url:%@",urlString2);
            
            urlString2 = [urlString2 stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            result2=[[NSDictionary alloc]init];
            
            NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString2]];
            
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                if(urlData==nil)
                {
                    
                    [[[UIAlertView alloc]initWithTitle:@"Error!!!" message:@"error in internet Connection" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                    
                }
                else{
                    
                    result2=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                    DebugLog(@"result is:%@",result2);
                    
                    
                    if([[result2 valueForKey:@"status"]isEqualToString:@"Error" ])
                    {
                        
                        response2=[result2 valueForKey:@"response"];
                        
                        UILabel *commentView=[[UILabel alloc]initWithFrame:CGRectMake(0, commentField.frame.origin.y+commentField.frame.size.height+5,_contentTable.frame.size.width, 100)];
                        
                        [commentView setTextAlignment:NSTextAlignmentCenter];
                        
                        [commentView setText:@""];
                        
                        commentView.textColor = [UIColor blackColor];
                        commentView.font = [UIFont fontWithName:HELVETICAPro size:18.0];
                        [cell addSubview:commentView];
                        
                        
//                        UILabel *cell_div2 = [[UILabel alloc] init];
//                        cell_div2.frame = CGRectMake(0, commentView.frame.origin.y+commentView.frame.size.height, self.view.frame.size.width-10, 2);
//                        cell_div2.backgroundColor = [UIColor colorWithRed:(248.0f/255.0f) green:(247.0f/255.0f) blue:(243.0f/255.0f) alpha:1.0];
//                        [cell addSubview:cell_div2];
                        
                        
                    }
                    else{
                        response2=[result2 valueForKey:@"response"];
                        
                        DebugLog(@"comment count%lu :%@",(unsigned long)[response2 count],response2);
                        
                        UIView *commentView=[[UIView alloc]initWithFrame:CGRectMake(0, cell_div.frame.origin.y+cell_div.frame.size.height,_contentTable.frame.size.width, 100)];
                        
                        UIImageView *otherProfile_img = [[UIImageView alloc] init];
                        otherProfile_img.frame = CGRectMake(10, 10, 40, 40);
                        
                        [otherProfile_img sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[response2 objectAtIndex:0]objectForKey:@"profileimage"] ]] placeholderImage:nil];
                        
                        otherProfile_img.layer.cornerRadius = otherProfile_img.frame.size.width/2;
                        otherProfile_img.clipsToBounds = YES;
                        
//                        CALayer *imageLayer = otherProfile_img.layer;
//                        [imageLayer setCornerRadius:20.5];
//                        [imageLayer setBorderWidth:0.1];
//                        [imageLayer setMasksToBounds:YES];
                        
                        
                        [commentView addSubview:otherProfile_img];
                        
                        UILabel *otherName = [[UILabel alloc] init];
                        otherName.frame = CGRectMake(otherProfile_img.frame.origin.x+otherProfile_img.frame.size.width+10, 19, 150, 15);
                        otherName.text = [NSString stringWithFormat:@"%@ : ",[[response2 objectAtIndex:0]objectForKey:@"name"]];
                        otherName.textColor = [UIColor blackColor];
                        otherName.font = [UIFont boldSystemFontOfSize:15.0];
                        [commentView addSubview:otherName];
                        [otherName sizeToFit];
                        
                        
                        
                      
                       cell.followedBy = [[[response2 objectAtIndex:0] objectForKey:@"followed_by"] intValue];
                        
                        DebugLog(@"%d",cell.followedBy);
                        
                        
                        UIButton *profile_btn = [[UIButton alloc] init];
                        profile_btn.frame = CGRectMake(0, 0, otherName.frame.origin.x+otherName.frame.size.width, otherProfile_img.frame.origin.y+otherProfile_img.frame.size.height+10);
                        
                        
                          profile_btn.tag = [[[response2 objectAtIndex:0] objectForKey:@"userid"] intValue];
                        
                        [profile_btn addTarget:self action:@selector(viewUser1:) forControlEvents:UIControlEventTouchUpInside];
                        [commentView addSubview:profile_btn];
                        
                        UILabel *commentContent=[[UILabel alloc]initWithFrame:CGRectMake( otherName.frame.origin.x+otherName.frame.size.width+5, 6, [UIScreen mainScreen].bounds.size.width-(otherName.frame.origin.x+otherName.frame.size.width+25),  45)];
                        
                        [commentContent setText:[NSString stringWithFormat:@"%@",[[response2 objectAtIndex:0]objectForKey:@"comment"]]];
                        commentContent.textColor = [UIColor blackColor];
                        
                        commentContent.font = [UIFont fontWithName:@"Helvetica Neue" size:12.0];
                        commentContent.numberOfLines = 3;
                        commentContent.lineBreakMode = YES;
                        
                        [commentView addSubview:commentContent];
                        
                        
                        UIButton *viewComments=[[UIButton alloc]initWithFrame:CGRectMake(0, commentContent.frame.origin.y+commentContent.frame.size.height+15, [UIScreen mainScreen].bounds.size.width, 20)];
                        
                        [viewComments setTitle:@"View All Comments" forState:UIControlStateNormal];
                        
                        [viewComments addTarget:self action:@selector(loadCommentView:) forControlEvents:UIControlEventTouchUpInside];
                        
                        [viewComments setBackgroundColor:[UIColor clearColor]];
                        
                        [viewComments.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:12]];
                        
                        [viewComments setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                        
                        
                        [viewComments setTag:[[[DisplayResponse objectAtIndex:indexPath.row]objectForKey:@"count_tip_id"]intValue]];
                        
                        [commentView addSubview:viewComments];
                        
                        
                        [cell addSubview:commentView];
                        
                     
                        
                    }
                    
                }
                
            }];
            
        }];
        
        
    }
    
    return cell;
}

- (void)tipDelete:(UIButton *)cross
{
    cross_index = (int)cross.tag;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"You would like to delete?"
                                       message:nil
                                      delegate:self
                             cancelButtonTitle:@"No"  otherButtonTitles:@"Yes", nil];
    alert.tag=99;
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    DebugLog(@"ALERT . TAG====%ld",(long)alertView.tag);
    if (alertView.tag == 99) {
        
        if(buttonIndex == 0)
        {
            // Do something
        }
        else
        {
            [UIView animateWithDuration:0.0f animations:^{
                
                [self startLoading:self.view];
                
            }completion:^(BOOL finished){
                
                
            // http://www.esolz.co.in/lab3/tipright/index.php/editprofile_ios/tip_delete?tipid=525&loggedin_id=158
                
              
                
                
                NSString *tipid=[[DisplayResponse objectAtIndex:cross_index]objectForKey:@"count_tip_id"];
                
                DebugLog(@"Tip Id:%@",tipid);
                
                
                [innerOperationQueue addOperationWithBlock:^{
                    NSString *urlString2=[NSString stringWithFormat:@"%@/%@/editprofile_ios/tip_delete?tipid=%@&loggedin_id=%@",GLOBALAPI,INDEX,tipid, [[NSUserDefaults standardUserDefaults]valueForKey:UDUSERID ]];
                    
                    urlString2 =[urlString2 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                    
                    DebugLog(@"url:%@",urlString2);
                    
                    result3=[[NSDictionary alloc]init];
                    
                    NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString2]];
                    
                    [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                        if(urlData==nil)
                        {
                            
                            [[[UIAlertView alloc]initWithTitle:@"Error!!!" message:@"error in internet Connection" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                            
                        }
                        else{
                            
                            result3=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                            DebugLog(@"result is:%@",result3);
                            
                            
                            if([[result3 valueForKey:@"status"]isEqualToString:@"Error" ])
                            {
                                
                                [[[UIAlertView alloc]initWithTitle:@"Error" message:[result3 valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                                
                            }
                            else{
                                
                                [self stopLoading];
                                
                                [DisplayResponse removeAllObjects];
                                
                                
                                dataCount=0;
                                totalCount=0;
                                
                                page=0;
                                [self loadData];
                                
                                
                            }
                            
                        }
                        
                    }];
                    
                }];
                
                
            }];
            
        }
    }
}


-(void)loadCommentView:(UIButton *)sender
{
    
    commentArr=[[NSMutableArray alloc]init];
    commentPage=0;
    commentCount=0;
    totalCommentCount=0;
    
    [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%d",(int)sender.tag] forKey:@"tipTableId"];
    
    
    blackView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
    [blackView setBackgroundColor:[UIColor blackColor]];
    
    [blackView setAlpha:0.9];
    
    [self.view addSubview:blackView];
    
    popupView=[[UIView alloc]initWithFrame:CGRectMake(20.0/320.0*self.view.frame.size.width,40.0/568.0*self.view.frame.size.height, 280.0/320.0*self.view.frame.size.width, 500.0/568.0*self.view.frame.size.height)];
    
    [popupView setBackgroundColor:[UIColor whiteColor]];
    [popupView setAlpha:1];
    [popupView.layer setZPosition:99];
    
    [self.view addSubview:popupView];
    
    UILabel *Comments=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, popupView.frame.size.width,50.0/568.0*self.view.frame.size.height)];
    
    [Comments setBackgroundColor:[UIColor colorWithRed:222/255.0 green:81/255.0 blue:5/255.0 alpha:1]];
    
    
    [Comments setText:@"COMMENTS"];
    [Comments setTextAlignment:NSTextAlignmentCenter];
    
    [Comments setFont:[UIFont boldSystemFontOfSize:18]];
    [popupView addSubview:Comments];
    
    
    if(CommentTable)
        [CommentTable removeFromSuperview];
    
    
    CommentTable=[[UITableView alloc]initWithFrame:CGRectMake(0, 50.0/568.0*self.view.frame.size.height, popupView.frame.size.width, 400.0/568.0*self.view.frame.size.height) style:UITableViewStylePlain];
    
    [CommentTable setTag:22];
    
    
    [CommentTable setDelegate:self];
    [CommentTable setDataSource:self];
    [CommentTable setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    
    
    [popupView addSubview:CommentTable];
    
    
    UIButton *backBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 450.0/568.0*self.view.frame.size.height, popupView.frame.size.width, 50.0/568.0*self.view.frame.size.height)];
    
    [backBtn setBackgroundColor:[UIColor colorWithRed:222/255.0 green:81/255.0 blue:5/255.0 alpha:1]];
    [backBtn.titleLabel setFont:[UIFont boldSystemFontOfSize:18]];
    
    [backBtn setTitle:@"CLOSE" forState:UIControlStateNormal];
    
    [backBtn addTarget:self action:@selector(closeComment) forControlEvents:UIControlEventTouchUpInside];
    
    [popupView addSubview:backBtn];
    
    [self loadCommentTable: commentPage];
    
}


-(void)loadCommentTable:(int)count
{
    
    NSString *tipId=[[NSUserDefaults standardUserDefaults]objectForKey:@"tipTableId"];
    
    [innerOperationQueue addOperationWithBlock:^{
        NSString *urlString2=[NSString stringWithFormat:@"%@/%@/%@/load_comments?tipid=%@&page=%d&limit=10&loggedinid=%@",GLOBALAPI,INDEX, DASHBOARD,tipId ,count,[[NSUserDefaults standardUserDefaults] valueForKey:UDUSERID]];
        
        DebugLog(@"url:%@",urlString2);
        
        urlString2 = [urlString2 stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        result3=[[NSDictionary alloc]init];
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString2]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            if(urlData==nil)
            {
                
                [[[UIAlertView alloc]initWithTitle:@"Error!!!" message:@"error in internet Connection" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                
            }
            else{
                
                result3=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                DebugLog(@"result is:%@",result3);
                
                
                if([[result3 valueForKey:@"status"]isEqualToString:@"Error" ])
                {
                    response3=[result3 valueForKey:@"response"];
                    
                }
                else{
                    response3=[result3 valueForKey:@"response"];
                    
                    DebugLog(@"comment count%lu",(unsigned long)[response3 count]);
                    
                    commentCount+=[response3 count];
                    totalCommentCount = [[result3 valueForKey:@"total"]intValue];
                    
                    
                    for(NSDictionary *commentDict in response3 )
                    {
                        [commentArr addObject:commentDict];
                        
                    }
                    
                    [self stopLoading];
                    
                    [CommentTable setUserInteractionEnabled:YES];
                    [CommentTable reloadData];
                    
                }
                
            }
            
        }];
        
    }];
    
}

-(void)closeComment
{
    [popupView setHidden:YES];
    [blackView setHidden:YES];
    [popupView removeFromSuperview];
    [blackView removeFromSuperview];
    [CommentTable removeFromSuperview];
    
}


-(void)send_func
{
    DebugLog(@"SEND");
}


- (void)viewUser:(UIButton *)sender {
  
    UITableViewCell *cCell= (UITableViewCell *)sender.superview;
    
    NSIndexPath *indexPath = [_contentTable indexPathForCell:cCell];
    
    
    
    TROwnProfileViewController *pushView = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"profile"];
    pushView.userId=[[DisplayResponse objectAtIndex:indexPath.row]objectForKey:@"user_id"];
   // [[NSUserDefaults standardUserDefaults]setObject:[[DisplayResponse objectAtIndex:indexPath.row]objectForKey:@"user_id"] forKey:@"globalID"];
    
    if(![[[DisplayResponse objectAtIndex:indexPath.row] objectForKey:@"user_id"] isEqualToString:[[NSUserDefaults standardUserDefaults]valueForKey:UDUSERID]])
    {
        pushView.isfromOther = TRUE;
    }
  
    pushView.FollowCheck=[[[DisplayResponse objectAtIndex:indexPath.row]objectForKey:@"followed_by" ]intValue] ;
    
    if([[[DisplayResponse objectAtIndex:indexPath.row]objectForKey:@"followed_by" ]intValue]==1)
    {
        
        pushView.follow_text=@"Unfollow";
        
    }
    else    if([[[DisplayResponse objectAtIndex:indexPath.row]objectForKey:@"followed_by" ]intValue]==0)
    {
        
        pushView.follow_text=@"Follow";
        
        
    }
    else{
        pushView.follow_text=@"Withdraw";
        
    }

    
    
    
    [[self navigationController ]pushViewController:pushView animated:YES];
    
}

-(void)viewUser1:(UIButton *)sender{
    
   // DebugLog(@"Response:%d, %@",(int)sender.tag, cell.followedBy.tag);
    
    TRHomeTableViewCell *cCell= (TRHomeTableViewCell *)sender.superview.superview;
    

    
    
    TROwnProfileViewController *pushView = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"profile"];
    pushView.userId = [NSString stringWithFormat:@"%ld", sender.tag];
   // [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%ld", sender.tag] forKey:@"globalID"];
    
    if(sender.tag  != [[[NSUserDefaults standardUserDefaults]valueForKey:UDUSERID] intValue])
    {
        pushView.isfromOther = TRUE;
    }
    
    pushView.FollowCheck = cCell.followedBy ;
    
    if(cCell.followedBy==1)
    {
        
        pushView.follow_text=@"Unfollow";
        
    }
    else    if(cCell.followedBy==0)
    {
        
        pushView.follow_text=@"Follow";
        
        
    }
    else{
        pushView.follow_text=@"Withdraw";
        
    }
    
    
    
    
    [[self navigationController ]pushViewController:pushView animated:YES];

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma UITextField Delegates

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{

    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:textField.tag inSection:0];
    UITableViewCell *cCell=[_contentTable cellForRowAtIndexPath:indexPath];
    
    
    DebugLog(@"str:%@,text:%@",string,textField.text);
    
    if([[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]>0 || ([[textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]>0  && textField.text.length!=1))
    {
        
        for(UIView *subView in [cCell subviews ])
        {
            
            if([subView isKindOfClass:[UIButton class]])
            {
                
                
                
                [subView setUserInteractionEnabled:YES];
                
                
            }
            
        }
        
        DebugLog(@"ok");
        
        
         return YES;
      
    }
    else{
        
        for(UIView *subView in [cCell subviews ])
        {
            
            if([subView isKindOfClass:[UIButton class]])
            {
                
              
                
                [subView setUserInteractionEnabled:NO];
                
                
            }
            
        }
        
          textField.text=@"";
      //  textField.placeholder=@"comment cannot be blank";
        //[textField setValue:[UIColor redColor] forKeyPath:@"_placeholderLabel.textColor"];
        
          DebugLog(@" not ok");
        
        return NO;
        
        
    }

    
    return YES;
    

}


-(void)sendClick:(UIButton *)sender;
{
    
    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:sender.tag inSection:0];
    UITableViewCell *cCell=[_contentTable cellForRowAtIndexPath:indexPath];
    
    
    for(UIView *subView in [cCell subviews ])
    {
        
        if([subView isKindOfClass:[UITextField class]])
        {
            
            UITextField *txt=(UITextField *)subView;
            
            
             [self postComment:txt];
         }
      }
}


-(void)postComment:(UITextField *)commentTxt
{
    [self startLoading:commentTxt.superview];
    
    DebugLog(@"Comment Value:%@",commentTxt.text);
    
    int path=(int)commentTxt.tag;
    
    NSString *tipid=[[DisplayResponse objectAtIndex:path]objectForKey:@"count_tip_id"];
    
    DebugLog(@"%@",tipid);
    
    
    [innerOperationQueue addOperationWithBlock:^{
        NSString *urlString2=[NSString stringWithFormat:@"%@/%@/%@/insert_comment?comment=%@&userid=%@&tipid=%@",GLOBALAPI,INDEX, DASHBOARD,commentTxt.text ,[[NSUserDefaults standardUserDefaults]valueForKey:UDUSERID ],tipid];
        
        
        urlString2 =[urlString2 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        DebugLog(@"url:%@",urlString2);
        
        result3=[[NSDictionary alloc]init];
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString2]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            if(urlData==nil)
            {
                
                [self stopLoading];
                
                
                [[[UIAlertView alloc]initWithTitle:@"Error!!!" message:@"error in internet Connection" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                
            }
            else{
                
                [self stopLoading];
                
                result3=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                DebugLog(@"result is:%@",result3);
                
                
                if([[result3 valueForKey:@"status"]isEqualToString:@"Error" ])
                {
                    
                    [[[UIAlertView alloc]initWithTitle:@"Error" message:[result3 valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                    
                }
                else{
                    
                    [_contentTable beginUpdates];
                    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:path inSection:0];
                    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
                    [_contentTable reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
                    [_contentTable endUpdates];
                    
                 
                    
                }
                
            }
            
        }];
        
    }];
    
}


-(void)copyBetClicked:(UIButton *)sender
{

    
//http://www.esolz.co.in/lab3/tipright/index.php/dashboard_ios/insert_bet?userid=141&tipid=392
    
    UITableViewCell *cCell= (UITableViewCell *)sender.superview;
    
    NSIndexPath *indexPath = [_contentTable indexPathForCell:cCell];
    
    
    NSString *tipid=[[DisplayResponse objectAtIndex:indexPath.row]objectForKey:@"count_tip_id"];
    
    DebugLog(@"%@",tipid);
    
    
    [innerOperationQueue addOperationWithBlock:^{
        NSString *urlString2=[NSString stringWithFormat:@"%@/%@/%@/insert_bet?userid=%@&tipid=%@",GLOBALAPI,INDEX, DASHBOARD ,[[NSUserDefaults standardUserDefaults]valueForKey:UDUSERID ],tipid];
        
        urlString2 =[urlString2 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        DebugLog(@"url:%@",urlString2);
        
        result3=[[NSDictionary alloc]init];
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString2]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            if(urlData==nil)
            {
                
                [[[UIAlertView alloc]initWithTitle:@"Error!!!" message:@"error in internet Connection" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                
            }
            else{
                
                result3=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                DebugLog(@"result is:%@",result3);
                
                
                if([[result3 valueForKey:@"status"]isEqualToString:@"Error" ])
                {
                    
                    [[[UIAlertView alloc]initWithTitle:@"Error" message:[result3 valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                    
                }
                else{
                    
                     [sender setTitle:@"COPIED" forState:UIControlStateNormal];
                    
//                    for(int i=0;i<4;i++)
//                    {
//                        [DisplayResponse removeLastObject ];
//                        dataCount--;
//                    }
//                    
//                    [self loadData];
                    
                }
                
            }
            
        }];
        
    }];
    

    
    
    



}

-(void)likeClicked:(UIButton *)sender
{
    
    //http://www.esolz.co.in/lab3/tipright/index.php/dashboard_ios/+s?userid=123&tipid=506
    
    UITableViewCell *cCell= (UITableViewCell *)sender.superview;
    
    NSIndexPath *indexPath = [_contentTable indexPathForCell:cCell];
    
    
    NSString *tipid=[[DisplayResponse objectAtIndex:indexPath.row]objectForKey:@"count_tip_id"];
    
    DebugLog(@"%@",tipid);
    
    
    [innerOperationQueue addOperationWithBlock:^{
        NSString *urlString2=[NSString stringWithFormat:@"%@/%@/%@/insert_likes?userid=%@&tipid=%@",GLOBALAPI,INDEX, DASHBOARD ,[[NSUserDefaults standardUserDefaults]valueForKey:UDUSERID ],tipid];
        
        urlString2 =[urlString2 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        DebugLog(@"url:%@",urlString2);
        
        result3=[[NSDictionary alloc]init];
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString2]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            if(urlData==nil)
            {
                
                [[[UIAlertView alloc]initWithTitle:@"Error!!!" message:@"error in internet Connection" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                
            }
            else{
                
                result3=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                DebugLog(@"result is:%@",result3);
                
                
                if([[result3 valueForKey:@"status"]isEqualToString:@"Error" ])
                {
                    
                    [[[UIAlertView alloc]initWithTitle:@"Error" message:[result3 valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                    
                }
                else{
                    
                    for(int i=0;i<4;i++)
                    {
                        [DisplayResponse removeLastObject];
                        dataCount--;
                    }
                    
                    [self loadData];
                    
                }
                
            }
            
        }];
        
    }];
    
}


-(void)refreshData
{

    page=0;
    dataCount=0;
    totalCount=0;
    [DisplayResponse removeAllObjects];
    [_contentTable setUserInteractionEnabled:NO];
    [_contentTable setDelegate:Nil];
    [_contentTable setDataSource:Nil];
    
    
     [self loadData];
    if (refreshControl) {
        
//        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//        [formatter setDateFormat:@"MMM d, h:mm a"];
//        NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
//        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor]
//                                                                    forKey:NSForegroundColorAttributeName];
//        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
//        refreshControl.attributedTitle = attributedTitle;
        
          [self performSelector:@selector(stopRefresh) withObject:nil afterDelay:2.5];
        
       
    }



}

- (void)stopRefresh

{
    
  [refreshControl endRefreshing];
    
          [_contentTable setUserInteractionEnabled:YES];
    
}

#pragma UIScrollViewDelegate

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGPoint offset = scrollView.contentOffset;
    CGRect bounds = scrollView.bounds;
    CGSize size = scrollView.contentSize;
    UIEdgeInsets inset = scrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    
    float reload_distance = -60.0f;
    if(y > h + reload_distance)
    {
        if(scrollView.tag==22)
        {
            if (commentCount<totalCommentCount)
            {
                isReloadComplete=FALSE;
                
                [CommentTable setUserInteractionEnabled:NO];
                [self startLoading:CommentTable];
                
                commentPage++;
                
                [self loadCommentTable:commentPage];
            }
        }
        else{
            
            if (dataCount<totalCount)
            {
                isReloadComplete=FALSE;
                
                [_contentTable setUserInteractionEnabled:NO];
                
                page++;
                
                [self performSelector:@selector(loadData) withObject:nil];
            }
        }
        
    }
    
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
