//
//  TRSignUpViewController.m
//  Tip Right
//
//  Created by santanu on 29/05/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import "TRSignUpViewController.h"
#import "TRGlobalHeader.h"
#import "TRTerms&ConditionViewController.h"


@interface TRSignUpViewController ()
{
    UIAlertView *alert;
    NSArray *pickerMonth, *pickerDay, *pickerSex;
    NSMutableArray *pickerYear;
    NSDictionary *json;
    UIPickerView *picker;
    BOOL month_action;
    UIToolbar *providerToolbar;
    UIBarButtonItem *done, *cancel;
    UIActivityIndicatorView *act;
    NSOperationQueue *opQueue;
    
}
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITextField *firstName;
@property (weak, nonatomic) IBOutlet UITextField *lastName;
@property (weak, nonatomic) IBOutlet UITextField *userEmail;
@property (weak, nonatomic) IBOutlet UITextField *confirmEmail;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UITextField *userName;
@property (weak, nonatomic) IBOutlet UIButton *dayBtn;
@property (weak, nonatomic) IBOutlet UIButton *yearBtn;
@property (strong, nonatomic) IBOutlet UIButton *SignUp;
@property (strong, nonatomic) IBOutlet UILabel *monthLabel;
@property (strong, nonatomic) IBOutlet UILabel *dayLabel;
@property (strong, nonatomic) IBOutlet UILabel *yearLabel;
@property (strong, nonatomic) IBOutlet UITextField *ConfirmPassword;

@property (strong, nonatomic) IBOutlet UILabel *genderLabel;
@property (strong, nonatomic) IBOutlet UIButton *monthBtn;
@property (strong, nonatomic) IBOutlet UIScrollView *backBtn;

@property (weak, nonatomic) IBOutlet UILabel *termsAndCondition;


@property (strong, nonatomic) IBOutlet UIButton *genderBtn;
@property (weak, nonatomic) IBOutlet UIImageView *backImage;

@end

@implementation TRSignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    pickerMonth = [[NSArray alloc] init];
    pickerDay = [[NSArray alloc] init];
    pickerSex = [[NSArray alloc] init];
    
    [_scrollView setContentSize:CGSizeMake(self.view.frame.size.width, 708.0/568.0*self.view.frame.size.height)];
    
    
    
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy"];
    int i2  = [[formatter stringFromDate:[NSDate date]] intValue];
    
    //Create Years Array from 1960 to This year
    pickerYear = [[NSMutableArray alloc] init];
    for (int i=1960; i<=i2; i++) {
        [pickerYear addObject:[NSString stringWithFormat:@"%d",i]];
    }
    
    pickerMonth = @[@"JAN", @"FEB", @"MAR", @"APR", @"MAY",@"JUN", @"JUL",@"AUG",@"SEP",@"OCT",@"NOV",@"DEC"];
    
    pickerDay = @[@"1", @"2", @"3", @"4", @"5", @"6",@"7",@"8",@"9",@"10",@"11",@"12",@"13", @"14", @"15", @"16", @"17", @"18",@"19",@"20",@"21",@"22",@"23",@"24",@"25",@"26",@"27",@"28",@"29",@"30",@"31"];
    pickerSex = @[@"Male",@"Female"];
    
    [_firstName setValue:[UIColor whiteColor]
              forKeyPath:@"_placeholderLabel.textColor"];
    [_lastName setValue:[UIColor whiteColor]
             forKeyPath:@"_placeholderLabel.textColor"];
    [_userEmail setValue:[UIColor whiteColor]
              forKeyPath:@"_placeholderLabel.textColor"];
    [_confirmEmail setValue:[UIColor whiteColor]
                 forKeyPath:@"_placeholderLabel.textColor"];
    [_password setValue:[UIColor whiteColor]
             forKeyPath:@"_placeholderLabel.textColor"];
    [_userName setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    [_ConfirmPassword setValue:[UIColor whiteColor]
                    forKeyPath:@"_placeholderLabel.textColor"];
    
    // Do any additional setup after loading the view.
    
    
    [_scrollView setHidden:YES];
    opQueue=[[NSOperationQueue alloc]init];
    
    UITapGestureRecognizer *tapPic=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(PopUp)];
    
    [tapPic setNumberOfTouchesRequired:1];
    
    [_termsAndCondition setUserInteractionEnabled:YES];
    [_termsAndCondition addGestureRecognizer:tapPic];
    
    [self loadBackImage];
}


-(void)PopUp
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select a option"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Terms & Conditions", @"Privacy", nil];
    [actionSheet showInView:_scrollView];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    TRTerms_ConditionViewController *terms = [[TRTerms_ConditionViewController alloc] init];
    
        if (buttonIndex==0) {
           
            terms.terms = TRUE;
            [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",@"Terms"] forKey:@"Terms"];
            
            [self.navigationController pushViewController:terms animated:YES];
            
        }
        else if(buttonIndex==1){
            
            terms.privacy = TRUE;
            [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",@"Privacy"] forKey:@"Terms"];
            
            [self.navigationController pushViewController:terms animated:YES];
        }
        else
        {
            [actionSheet dismissWithClickedButtonIndex:2 animated:YES];
        }
   
}

-(void)loadBackImage
{
    //http://esolz.co.in/lab3/tipright/index.php/app_ios/appbackgroundimage
    
    
    [self startLoading:self.view];
    
    
    [opQueue addOperationWithBlock:^{
        
        NSString *urlString =[NSString stringWithFormat:@"http://www.esolz.co.in/lab3/tipright/index.php/app_ios/appbackgroundimage"]; //&thumb=true
        
        DebugLog(@"requests url: %@",urlString);
        
        NSString *newString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSData *dataURL =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            
            [self stopLoading];
               [_scrollView setHidden:NO];
            
            NSString *error_msg;
            
            if (dataURL != nil)
            {
                NSError *error=nil;
                
                json = [NSJSONSerialization JSONObjectWithData:dataURL //1
                        
                                                       options:kNilOptions
                        
                                                         error:&error];
                DebugLog(@"json returns: %@",json);
                
                
                
                
                error_msg= [json objectForKey:@"status"];
                DebugLog(@"err  %@",[json objectForKey:@"status"]);
                if ([error_msg isEqualToString:@"Success"])
                {
                    
                    NSDictionary *dict=[json objectForKey:@"data"];
                    
                    
                    [_backImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[dict objectForKey:@"appimage"]]] placeholderImage:[UIImage imageNamed:@"bg"]];
                    
                    
                 
                    
                    
                }
                else
                {
                    
//                    alert = [[UIAlertView alloc] initWithTitle:@""
//                                                       message:[json objectForKey:@"message"]
//                                                      delegate:self
//                                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
//                    [alert show];
                }
            }
            else
            {
                
//                alert = [[UIAlertView alloc] initWithTitle:@"error in server connection!"
//                                                   message:Nil
//                                                  delegate:self
//                                         cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
//                [alert show];
            }
            
        }];
        
    }];
    
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    _monthBtn.tag = 1;
    _dayBtn.tag = 2;
    _yearBtn.tag = 3;
    _genderBtn.tag = 4;
    
    NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
    _termsAndCondition.attributedText = [[NSAttributedString alloc] initWithString:@"By clicking Sign Up, You agree to our Terms and that you have read our Data Policy, including our Cookies Use."
                                                                        attributes:underlineAttribute];
    
}

-(BOOL)isSpace:(NSString *)inputString
{
    
    
    NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];
    if ([[inputString stringByTrimmingCharactersInSet: set] length] == 0)
    {
        return YES;
    }
    return NO;
    
}


-(BOOL)isNumeric:(NSString*)inputString{
    BOOL isValid = NO;
    NSCharacterSet *alphaNumbersSet = [NSCharacterSet decimalDigitCharacterSet];
    NSCharacterSet *stringSet = [NSCharacterSet characterSetWithCharactersInString:inputString];
    isValid = [alphaNumbersSet isSupersetOfSet:stringSet];
    return isValid;
}
- (IBAction)monthFunc:(UIButton *)sender
{
    DebugLog(@"MONTH:%ld",(long)sender.tag);
    [picker removeFromSuperview];
    [providerToolbar removeFromSuperview];
    [self Picker_Func:(int)sender.tag];
}

- (IBAction)dayFunc:(UIButton *)sender
{
    [picker removeFromSuperview];
    [providerToolbar removeFromSuperview];
    DebugLog(@"DAY:%ld",(long)sender.tag);
    [self Picker_Func:(int)sender.tag];
    
}

- (IBAction)yearFunc:(UIButton *)sender
{
    [picker removeFromSuperview];
    [providerToolbar removeFromSuperview];
    DebugLog(@"YEAR:%ld",(long)sender.tag);
    [self Picker_Func:(int)sender.tag];
    
}

- (IBAction)genderFunc:(UIButton *)sender {
    
    [picker removeFromSuperview];
    [providerToolbar removeFromSuperview];
    DebugLog(@"GENDER:%ld",(long)sender.tag);
    [self Picker_Func:(int)sender.tag];
}

-(void)dismissActionSheet:(id)sender
{
    UIButton *done_btn = sender;
    
    if (_scrollView.contentOffset.y == 0)
        [_scrollView setContentOffset:CGPointMake(0, 250) animated:YES];
    else
        [_scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    
    [providerToolbar removeFromSuperview];
    [picker removeFromSuperview];
    
    if ((int)done_btn.tag == 1) {
        
        if ([_monthLabel.text isEqualToString:@"Month"]) {
            
            _monthLabel.text = [pickerMonth objectAtIndex:0];
        }
        
    }
    if ((int)done_btn.tag == 2) {
        
        if ([_dayLabel.text isEqualToString:@"Day"]) {
            
            _dayLabel.text = [pickerDay objectAtIndex:0];
        }
        
    }
    if ((int)done_btn.tag == 3) {
        
        if ([_yearLabel.text isEqualToString:@"Year"]) {
            
            _yearLabel.text = [pickerYear objectAtIndex:0];
        }
        
    }
    if ((int)done_btn.tag == 4) {
        
        if ([_genderLabel.text isEqualToString:@"Gender"]) {
            
            _genderLabel.text = [pickerSex objectAtIndex:0];
        }
        
    }
    
}

- (IBAction)Signup_func:(id)sender
{
    DebugLog(@"in signup");
    
    
    [_firstName resignFirstResponder];
    [_lastName resignFirstResponder];
    [_userEmail resignFirstResponder];
    [_confirmEmail resignFirstResponder];
    [_password resignFirstResponder];
    [providerToolbar removeFromSuperview];
    [picker removeFromSuperview];
    [_scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    
    if ([[_firstName.text stringByTrimmingCharactersInSet:whitespace] length]==0) {
        
        alert = [[UIAlertView alloc] initWithTitle:@"Please Enter First Name!"
                                           message:Nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        
        [alert show];
    }
    else if ([[_lastName.text stringByTrimmingCharactersInSet:whitespace] length]==0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Last Name!"
                                           message:Nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        
        [alert show];
    }
    else if ([[_userName.text stringByTrimmingCharactersInSet:whitespace] length]==0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Username!"
                                           message:Nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        
        [alert show];
    }
    
    else if ([[_userEmail.text stringByTrimmingCharactersInSet:whitespace] length]==0) {
        
        alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Email!"
                                           message:Nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        
        [alert show];
    }
    
    //    else
    //    {
    
    //        if ( [self isNumeric:_userEmail.text]) {
    //
    //            if ([[_userEmail.text stringByTrimmingCharactersInSet:whitespace] length] <10 || [[_userEmail.text stringByTrimmingCharactersInSet:whitespace] length] > 10 || !([_userEmail.text rangeOfCharacterFromSet:notDigits].location == NSNotFound))
    //            {
    //                alert = [[UIAlertView alloc] initWithTitle:@"Please Enter a Valid Mobile Number"
    //                                                   message:Nil
    //                                                  delegate:self
    //                                         cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
    //                [alert show];
    //            }
    //            else if (![_userEmail.text isEqualToString:_confirmEmail.text])
    //            {
    //                alert = [[UIAlertView alloc] initWithTitle:@"Mobile Number & Re-Enter Mobile Number Doesn't Match!"
    //                                                   message:Nil
    //                                                  delegate:self
    //                                         cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
    //
    //                [alert show];
    //            }
    //            else if ([[_password.text stringByTrimmingCharactersInSet:whitespace] length] == 0)
    //            {
    //                alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Your Password!"
    //                                                   message:Nil
    //                                                  delegate:self
    //                                         cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
    //                [alert show];
    //            }
    //
    //            else
    //            {
    //                [self Birthday_Func];
    //            }
    //
    //
    //
    //        }
    else if ([_userEmail.text isEqualToString:@""])
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Email"
                                           message:Nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
    }
    
    else if ([[_userEmail.text stringByTrimmingCharactersInSet:whitespace] length] > 0 && ![self NSStringIsValidEmail:_userEmail.text])
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Please Enter a Valid Email"
                                           message:Nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        
        [alert show];
        
    }
    else if (![_userEmail.text isEqualToString:_confirmEmail.text])
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Please Confirm Your Email!"
                                           message:Nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        
        [alert show];
    }
    else if ([[_password.text stringByTrimmingCharactersInSet:whitespace] length] == 0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Your Password!"
                                           message:Nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
    }
    
    else if ([[_password.text stringByTrimmingCharactersInSet:whitespace] length] < 6 ||[[_password.text stringByTrimmingCharactersInSet:whitespace] length] > 20 )
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Enter password within a range of 6 to 20 characters"
                                           message:Nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
    }
    else if (![_ConfirmPassword.text isEqualToString:_password.text])
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Password and Confirm Password must be same!"
                                           message:Nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
    }
    
    else if ([_genderLabel.text isEqualToString:@"Gender"]) {
        
        alert = [[UIAlertView alloc] initWithTitle:@"Please Select Sex!"
                                           message:Nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
    }
    
    else
    {
        [self Birthday_Func];
    }
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == _firstName)
    {
        if (_scrollView.contentOffset.y == 0)
            [_scrollView setContentOffset:CGPointMake(0, 160) animated:YES];
    }
    if (textField == _lastName)
    {
        if (_scrollView.contentOffset.y == 0)
            [_scrollView setContentOffset:CGPointMake(0, 160) animated:YES];
    }
    if (textField == _userName)
    {
        if (_scrollView.contentOffset.y == 0)
            [_scrollView setContentOffset:CGPointMake(0, 160) animated:YES];
    }
    if (textField == _userEmail)
    {
        if (_scrollView.contentOffset.y == 0)
            [_scrollView setContentOffset:CGPointMake(0, 160) animated:YES];
    }
    if (textField == _confirmEmail)
    {
        if (_scrollView.contentOffset.y == 0)
            [_scrollView setContentOffset:CGPointMake(0, 160) animated:YES] ;
    }
    if (textField == _password)
    {
        if (_scrollView.contentOffset.y == 0)
            [_scrollView setContentOffset:CGPointMake(0, 160) animated:YES];
    }
    
    if (textField == _ConfirmPassword)
    {
        if (_scrollView.contentOffset.y == 0)
            [_scrollView setContentOffset:CGPointMake(0, 200) animated:YES];
    }
    
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return  YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    [providerToolbar removeFromSuperview];
    [picker removeFromSuperview];
    [_scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    return YES;
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    checkString = [checkString lowercaseString];
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:checkString];
}

// The number of columns of data
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// returns the # of rows in each component..
// The number of rows of data
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    int comp = 0;
    if (pickerView.tag == 1)
    {
        comp = (int)pickerMonth.count;
    }
    if (pickerView.tag == 2)
    {
        comp = (int)pickerDay.count;
    }
    if(pickerView.tag == 3)
    {
        comp = (int)pickerYear.count;
    }
    if (pickerView.tag == 4) {
        comp = (int)pickerSex.count;
    }
    return comp;
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (pickerView.tag == 1) {
        
        return pickerMonth[row];
    }
    else if (pickerView.tag == 2)
    {
        return pickerDay[row];
    }
    else if (pickerView.tag == 3)
    {
        return pickerYear[row];
    }
    else
        return pickerSex[row];
    
}

// Catpure the picker view selection
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (pickerView.tag == 1)
    {
        _monthLabel.text = [pickerMonth objectAtIndex:row];
        [picker resignFirstResponder];
    }
    else if (pickerView.tag == 2)
    {
        _dayLabel.text = [pickerDay objectAtIndex:row];
        [picker resignFirstResponder];
    }
    else if (pickerView.tag == 3)
    {
        _yearLabel.text = [pickerYear objectAtIndex:row];
    }
    else
        _genderLabel.text = [pickerSex objectAtIndex:row];
    
}

-(void)Birthday_Func
{
    
    
    
    if ([_monthLabel.text isEqualToString:@"Month"]) {
        alert = [[UIAlertView alloc] initWithTitle:@"Please Select Month!"
                                           message:Nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
        
    }
    else if ([_dayLabel.text isEqualToString:@"Day"]) {
        alert = [[UIAlertView alloc] initWithTitle:@"Please Select Date!"
                                           message:Nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
        
    }
    else if ([_yearLabel.text isEqualToString:@"Year"]) {
        alert = [[UIAlertView alloc] initWithTitle:@"Please Select Year!"
                                           message:Nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
        
    }
    else {
        
        NSString *dob = [NSString stringWithFormat:@"%@-%@-%@",_yearLabel.text,_monthLabel.text,_dayLabel.text];
        
        DebugLog(@"DOB:%@",dob);
        
        
        NSDate *now=[NSDate date];
        
        NSDateFormatter *formatter2 = [[NSDateFormatter alloc]init];
        
        [formatter2 setDateFormat:@"yyyy-MM-dd"];
        
        // [formatter2 setTimeZone:[NSTimeZone timeZoneWithName:@"Japan"]];
        
        NSString *nowS=[formatter2 stringFromDate:now];
        
        NSDate *dateJ = [formatter2 dateFromString:nowS];
        
        // NSLog(@"CURRENT DATE:%@",dateJ);
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        
        [formatter setDateFormat:@"yyyy-MMMM-dd"];
        
        NSDate *date = [formatter dateFromString:dob];
        
        DebugLog(@"old DATE:%@ and %@",dob ,date);
        
        NSTimeInterval interval = [dateJ timeIntervalSinceDate:date];
        
        int hours = (int)interval / 3600;
        
        
        int days=hours/24;
        
        
        
        
        if(days<1)
        {
            alert = [[UIAlertView alloc] initWithTitle:@"Birth Day must be before Today!"
                                               message:Nil
                                              delegate:self
                                     cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
            [alert show];
            
            
        }
        else
        {
//            act = [[UIActivityIndicatorView alloc] init];
//            act.center = self.view.center;
//            act.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
//            [act setColor:[UIColor whiteColor]];
//            [self.view addSubview:act];
//            [act startAnimating];
            [self startLoading:self.view];
            
            NSString *gender;
            //http://www.esolz.co.in/lab3/tipright/index.php/signup_ios?first_name=pronoy&last_name=sarkar&email=pronoy.sarkar1@esolzmail.com&password=123456&year-month-date=1990-10-11&gender=0
            
            NSString *dob = [NSString stringWithFormat:@"%@-%@-%@",_yearLabel.text,_monthLabel.text,_dayLabel.text];
            
            DebugLog(@"DOB:%@",dob);
            
            if ([_genderLabel.text isEqualToString:@"Male"]) {
                gender = @"0";
            }
            else
            {
                gender = @"1";
            }
            
            NSString *urlString1 =[NSString stringWithFormat:@"%@/%@/signup_ios?first_name=%@&last_name=%@&username=%@&email=%@&password=%@&year-month-date=%@&gender=%@",GLOBALAPI,INDEX,_firstName.text,_lastName.text,_userName.text,_userEmail.text,_password.text,dob,gender]; //&thumb=true
            
            DebugLog(@"requests url: %@",urlString1);
            
            NSString *newString1 = [urlString1 stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            NSData *signeddataURL1 =[NSData dataWithContentsOfURL:[NSURL URLWithString:newString1]];
            NSString *error_msg;
            
            if (signeddataURL1 != nil)
            {
                NSError *error=nil;
                
                json = [NSJSONSerialization JSONObjectWithData:signeddataURL1
                        
                                                       options:kNilOptions
                        
                                                         error:&error];
                DebugLog(@"json returns: %@",json);
                
                error_msg= [json objectForKey:@"status"];
                DebugLog(@"err  %@",[json objectForKey:@"status"]);
                if ([error_msg isEqualToString:@"Success"])
                {
                    //[act removeFromSuperview];
                    [self stopLoading];
                    alert = [[UIAlertView alloc] initWithTitle:nil
                                                       message:[json objectForKey:@"message"]
                                                      delegate:self
                                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                    [alert show];
                    UIViewController *Login = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"Login"];
                    [[self navigationController ]pushViewController:Login animated:YES];
                    
                }
                else
                {
                    //[act removeFromSuperview];
                    [self stopLoading];
                    alert = [[UIAlertView alloc] initWithTitle:nil
                                                       message:[json objectForKey:@"message"]
                                                      delegate:self
                                             cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                    [alert show];
                }
            }
            else
            {
                //[act removeFromSuperview];
                [self stopLoading];
                alert = [[UIAlertView alloc] initWithTitle:@"error in server connection!"
                                                   message:Nil
                                                  delegate:self
                                         cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
                [alert show];
            }
            
        }
        
    }
}

///////////////// Picker ////////////////////////
-(void)Picker_Func:(int)sender
{
    [_scrollView setContentOffset:CGPointMake(0, 160) animated:YES];
    
    [_firstName resignFirstResponder];
    [_lastName resignFirstResponder];
    [_userEmail resignFirstResponder];
    [_confirmEmail resignFirstResponder];
    [_password resignFirstResponder];
    
    DebugLog(@"SENDER:%d",sender);
    providerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height - 210, self.view.bounds.size.width, 50)];
    
    providerToolbar.backgroundColor = [UIColor whiteColor];
    
    done = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissActionSheet:)];
    cancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissActionSheet:)];
    done.tag = sender;
    providerToolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], done];
    //    providerToolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(dismissActionSheet:)], cancel];
    [self.view addSubview:providerToolbar];
    picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height - 160, 0, 0)];
    picker.tag = sender;
    picker.backgroundColor = [UIColor whiteColor];
    picker.showsSelectionIndicator = YES;
    picker.dataSource = self;
    picker.delegate = self;
    
    [self.view addSubview:picker];
}

- (IBAction)Back:(id)sender
{
    [self popToViewController:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
