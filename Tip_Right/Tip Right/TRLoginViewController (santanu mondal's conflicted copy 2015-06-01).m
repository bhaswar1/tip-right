//
//  TRLoginViewController.m
//  Tip Right
//
//  Created by santanu on 30/05/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import "TRLoginViewController.h"
#import "TRGlobalHeader.h"


@interface TRLoginViewController ()<UITextFieldDelegate>
{
    BOOL keep;
    UIAlertView *alert;
}
@end

@implementation TRLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


-(void)viewDidAppear:(BOOL)animated
{
    
    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width,scrollView.frame.size.height+100)];
    scrollView.contentSize = CGSizeMake(1, scrollView.frame.size.height+100);
    scrollView.contentOffset=CGPointMake(0, 0);
    
    
    emailField.delegate = self;
    passwordField.delegate = self;
    emailField.autocorrectionType = NO;
    [emailField setValue:[UIColor whiteColor]
                    forKeyPath:@"_placeholderLabel.textColor"];
    [passwordField setValue:[UIColor whiteColor]
              forKeyPath:@"_placeholderLabel.textColor"];
    
    
    UIFont *font2,*font1;
    
    
    if(IsIphone6||IsIphone6plus)
    {
        font2 = [UIFont fontWithName:HELVETICAPro size:18.0f];
        font1 = [UIFont fontWithName:HELVETICAProLight  size:15.0f];
    }
    else{
       font2 = [UIFont fontWithName:HELVETICAPro size:15.0f];
       font1 = [UIFont fontWithName:HELVETICAProLight  size:12.0f];
    }
    
   
    NSDictionary *dict1 = @{NSUnderlineStyleAttributeName:@(NSUnderlineStyleNone),NSFontAttributeName:font1}; // Added line
    NSDictionary *dict2 = @{NSUnderlineStyleAttributeName:@(NSUnderlineStyleNone),NSFontAttributeName:font2}; // Added line
    
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] init];
    [attString appendAttributedString:[[NSAttributedString alloc] initWithString:@"Don't have an account?"    attributes:dict1]];
    [attString appendAttributedString:[[NSAttributedString alloc] initWithString:@" Sign Up"      attributes:dict2]];
    [Sign_up setAttributedTitle:attString forState:UIControlStateNormal];
    
    
    passwordField.secureTextEntry = YES;
    
    
}
- (IBAction)signUpClick:(id)sender {
    

    UIViewController *signUpVC=[[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"signUp"];
  [[self navigationController ]pushViewController:signUpVC animated:YES];
}

-(IBAction)keep
{
    if (!keep) {
        keepMe.image = [UIImage imageNamed:@"check-box1"];
        keep = TRUE;
    }
    else
    {
        keepMe.image = [UIImage imageNamed:@"check-box"];
        keep = FALSE;
    }
    
}

-(BOOL)isNumeric:(NSString*)inputString{
    BOOL isValid = NO;
    NSCharacterSet *alphaNumbersSet = [NSCharacterSet decimalDigitCharacterSet];
    NSCharacterSet *stringSet = [NSCharacterSet characterSetWithCharactersInString:inputString];
    isValid = [alphaNumbersSet isSupersetOfSet:stringSet];
    return isValid;
}

- (IBAction)Login:(id)sender {

     NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    
    
    
    if ([emailField.text isEqualToString:@""]) {
        
        alert = [[UIAlertView alloc] initWithTitle:@"Please Enter email or Mobile No."
                                           message:Nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        
        [alert show];
    }
    
    else  if ( [self isNumeric:emailField.text]) {
        
   
        
        
    }
  else  if ([[emailField.text stringByTrimmingCharactersInSet:whitespace] length] > 0 && ![self NSStringIsValidEmail:emailField.text])
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Please Enter a Valid email"
                                           message:Nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        
        [alert show];
        
    }
    else if ([[passwordField.text stringByTrimmingCharactersInSet:whitespace] length] == 0)
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Please Enter Your Password!"
                                           message:Nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
    }
  
 
    
    else
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Successfull Login!"
                                           message:Nil
                                          delegate:self
                                 cancelButtonTitle:@"OK"  otherButtonTitles:Nil, nil];
        [alert show];
    }
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == emailField)
    {
        if (scrollView.contentOffset.y == 0)
            [scrollView setContentOffset:CGPointMake(0, 50)];
        
    }
    if (textField == passwordField)
    {
        if (scrollView.contentOffset.y == 50)
            [scrollView setContentOffset:CGPointMake(0, 100)];
    }
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return  YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [emailField resignFirstResponder];
    [passwordField resignFirstResponder];
    return YES;
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    checkString = [checkString lowercaseString];
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:checkString];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
