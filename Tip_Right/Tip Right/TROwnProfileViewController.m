//
//  TROwnProfileViewController.m
//  Tip Right
//
//  Created by intel on 12/06/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import "TROwnProfileViewController.h"
#import "TRMessageDetailsViewController.h"
#import "TRNotificationDetailsViewController.h"
#import "UIImageView+WebCache.h"
#import "TRFollowerListViewController.h"
#import "TRFollowingListViewController.h"


@interface TROwnProfileViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    NSDictionary *result, *response, *tip_response, *tip_result, *resultDict, *accept_result, *deny_result;
    NSMutableDictionary *sectionedData;
    NSMutableArray *rows_count, *date_array,*finalTipArray;
    
    NSMutableDictionary *finalTipDict;
    NSDictionary *firstTipDict,*secondTipDict;
    NSArray *tipImageArray;
    NSMutableArray *tempArray;
    
    int page,dataCount,totalCount,totalrow,sectionCount;
    NSMutableArray *data_array;
    
    NSOperationQueue *follow_operationQueue;
    
    CGRect posRect;
    BOOL dotTap;
    
    __weak IBOutlet UIButton *aboutBtn;
    __weak IBOutlet UIView *followerView;
    
    __weak IBOutlet UIView *betView;
    
    __weak IBOutlet UIView *postView;
    __weak IBOutlet UIView *followingView;
    
    __weak IBOutlet UILabel *noTipLbl;
}

@property (weak, nonatomic) IBOutlet UILabel *aboutLabel;

@end

@implementation TROwnProfileViewController
@synthesize userId,isFromRequest,privateCheck,FollowCheck;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [_contentTable setScrollEnabled:NO];
    
    
    
    tipImageArray=@[@"horse",@"ball",@"hand",@"football"];
    finalTipArray=[[NSMutableArray alloc]init];
    finalTipDict=[[NSMutableDictionary alloc]init];
    [self Header:self.view];
    [self Footer:self.view];
    
    self.view.backgroundColor = [UIColor colorWithRed:(248.0f/255.0f) green:(247.0f/255.0f) blue:(243.0f/255.0f) alpha:1.0f];
    
    _mainScroll.hidden = YES;
    _mainScroll.scrollEnabled = YES;
    _mainScroll.userInteractionEnabled = YES;
    //[_mainScroll setContentSize:CGSizeMake(_mainScroll.frame.size.width,_mainScroll.frame.size.height)];
    _mainScroll.contentSize = CGSizeMake(_mainScroll.frame.size.width, _mainScroll.frame.size.height);
    _mainScroll.showsVerticalScrollIndicator=NO;
    _mainScroll.contentOffset=CGPointMake(0, 0);
    
    _profilePic.layer.cornerRadius = _profilePic.frame.size.width/2;
    _profilePic.clipsToBounds = YES;
    
    
    
    CALayer *imageLayer2 = _message_btn.layer;
    [imageLayer2 setCornerRadius:15.0/375.0*self.view.frame.size.width];
    
    [imageLayer2 setMasksToBounds:YES];
    
    // [copyBet setBackgroundImage:[UIImage imageNamed:@"copy-ber"] forState:UIControlStateNormal];
    
    //  [_message_btn setBackgroundColor:[UIColor colorWithRed:(52.0f/255.0f) green:(217.0f/255.0f) blue:(25.0f/255.0f) alpha:1]];
    
    
    
    CALayer *imageLayer = _follw_unfollw.layer;
    [imageLayer setCornerRadius:15.0/375.0*self.view.frame.size.width];
    
    [imageLayer setMasksToBounds:YES];
    
    // [copyBet setBackgroundImage:[UIImage imageNamed:@"copy-ber"] forState:UIControlStateNormal];
    
    //[_follw_unfollw setBackgroundColor:[UIColor colorWithRed:(255.0f/255.0f) green:(127.0f/255.0f) blue:(0.0f/255.0f) alpha:1]];
    
    [[NSUserDefaults standardUserDefaults]setObject:userId forKey:@"globalID"];
    

    
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    
    data_array = [[NSMutableArray alloc] init];
    page=0;
    
    dataCount=0;
    totalrow=0;
    sectionCount=0;
    [self startLoading:self.view];
    
     [_contentTable setHidden:YES];
    
    [self loadData];
    //[self displayData];
}

-(void)loadData
{
    // http://esolz.co.in/lab3/tipright/index.php/profile_ios/view_user?user_id=141
    //http://esolz.co.in/lab3/tipright/index.php/profile_ios/show_own_tip?id=123&page=0&limit=1
    
    
   
    
    NSOperationQueue *operationQueue = [NSOperationQueue new];
    [operationQueue addOperationWithBlock:^{
        NSString *tip_url=[NSString stringWithFormat:@"%@/%@/profile_ios/show_own_tip?id=%@&page=%d&limit=1",GLOBALAPI,INDEX,userId,page];
        
        DebugLog(@"TIP URL:%@",tip_url);
        
        tip_url = [tip_url stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        tip_result=[[NSDictionary alloc]init];
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:tip_url]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            [self stopLoading];
            
            
            
            if (urlData!=nil) {
                tip_result=[NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                // DebugLog(@"Tip result is:%@",tip_result);
                
                
                if([[tip_result valueForKey:@"status"]isEqualToString:@"Error" ])
                {
                    
                    [noTipLbl setHidden:NO];
                    
                    
                }
                else{
                    
                    
                    totalCount=[[tip_result valueForKey:@"count"]intValue];
                    
                    
                    tip_response = [[tip_result objectForKey:@"response"] objectForKey:@"data"];
                    
                    
                    for (NSDictionary *dict in tip_response) {
                        
                        dataCount++;
                        
                        
                        for (NSDictionary *tip_dict in [dict objectForKey:@"tip"]) {
                            
                            
                            
                            [data_array addObject:tip_dict];
                            
                        }
                        
                    }
                    
                    date_array = [[NSMutableArray alloc] init];
                    [date_array addObject:[[[data_array objectAtIndex:0] objectForKey:@"added_date"] substringWithRange:NSMakeRange(0, 10)]];
                    for (int index=1; index<[data_array count]; index++) {
                        int count=0;
                        NSString *date = [NSString stringWithFormat:@"%@",[[[data_array objectAtIndex:index] objectForKey:@"added_date"] substringWithRange:NSMakeRange(0, 10)]];
                        for (int check=0; check<[date_array count]; check++) {
                            if ([[date_array objectAtIndex:check] isEqualToString:date]) {
                                count++;
                                break;
                            }
                        }
                        if (count==0) {
                            [date_array addObject:date];
                        }
                    }
                    
                    sectionedData = [[NSMutableDictionary alloc] init];
                    
                    for (int i=0; i<[date_array count]; i++) {
                        NSMutableArray *temp_array = [[NSMutableArray alloc] init];
                        
                        NSMutableArray *datewise_array = [[NSMutableArray alloc] init];
                        
                        for (int j=0; j<[data_array count]; j++) {
                            
                            if ([[[[data_array objectAtIndex:j] objectForKey:@"added_date"] substringWithRange:NSMakeRange(0, 10)] isEqualToString:[date_array objectAtIndex:i]]) {
                                
                                [temp_array addObject:[data_array objectAtIndex:j]];
                                
                            }
                        }
                        
                        
                        DebugLog(@"Temp Array For:%@",temp_array);
                        //////////// Idwise Sectioned ///////////////
                        
                        NSSet *uniqueStates = [NSSet setWithArray:[temp_array valueForKey:@"count_tip_id"]];
                        
                        
                        NSSortDescriptor *sd = [[NSSortDescriptor alloc] initWithKey:nil ascending:NO];
                        NSArray *a_ray = [[NSArray alloc] init];
                        a_ray =  [uniqueStates sortedArrayUsingDescriptors:@[sd]];
                        
                        
                        NSMutableArray *uniqueArray;
                        
                        DebugLog(@"Unique Value:%@",uniqueStates);
                        
                        NSMutableArray *temp_id_array;
                        for(NSString *unique in a_ray)
                        {
                            uniqueArray = [[NSMutableArray alloc] init];
                            temp_id_array = [[NSMutableArray alloc] init];
                            //DebugLog(@"Unique:%@",unique);
                            for(NSDictionary *dict in temp_array)
                            {
                                if([[dict objectForKey:@"count_tip_id"] isEqualToString:unique])
                                {
                                    //DebugLog(@"Dictionary:%@",dict);
                                    [temp_id_array addObject:dict];
                                }
                            }
                            
                            DebugLog(@"Array with unique:%@\n%@",unique,temp_id_array);
                            [uniqueArray addObject:temp_id_array];
                            DebugLog(@"DICTIONARY with unique:%@\n%@",unique,uniqueArray);
                            //                    temp_array = [[NSMutableArray alloc] init];
                            [datewise_array addObject:uniqueArray];
                        }
                        
                        DebugLog(@"IDWISE unique:%@",datewise_array);
                        
                        
                        
                        [sectionedData setObject:datewise_array forKey:[date_array objectAtIndex:i]];
                        
                    }
                    
                    
                    DebugLog(@"SECTIONED DATA:%@",sectionedData);
                    DebugLog(@"Date Array:%@",date_array);
                    //DebugLog(@"Datewise from sectioned:%@",[sectionedData objectForKey:[date_array objectAtIndex:0]]);
                    
                    //                NSSet *uniqueStates = [NSSet setWithArray:[[sectionedData objectForKey:[date_array objectAtIndex:0]] valueForKey:@"count_tip_id"]];
                    //
                    //                NSMutableDictionary *uniqueArray = [[NSMutableDictionary alloc] init];
                    //
                    //                DebugLog(@"Unique Value:%@",uniqueStates);
                    //
                    //                NSMutableArray *temp_id_array;
                    //                for(NSString *unique in uniqueStates)
                    //                {
                    //                    temp_id_array = [[NSMutableArray alloc] init];
                    //                    DebugLog(@"Unique:%@",unique);
                    //                    for(NSDictionary *dict in [sectionedData objectForKey:[date_array objectAtIndex:0]])
                    //                    {
                    //                        if([[dict objectForKey:@"count_tip_id"] isEqualToString:unique ])
                    //                        {
                    //                            DebugLog(@"Dictionary:%@",dict);
                    //                            [temp_id_array addObject:dict];
                    //                        }
                    //                    }
                    //
                    //                    [uniqueArray setObject:temp_id_array forKey:unique];
                    //                }
                    //
                    //                DebugLog(@"Array with unique:%@",uniqueArray);
                    
                    totalrow=0;
                    
                    for(NSString *str in date_array)
                    {
                        
                       
                        
                        NSInteger Count=[[sectionedData objectForKey:[NSString stringWithFormat:@"%@",str]] count]/2;
                        Count=([[sectionedData objectForKey:[NSString stringWithFormat:@"%@",str]]  count]%2>0)?Count+1:Count+0;
                        
                        totalrow+=Count;
                        
                        
                    }
                    
                    
                    DebugLog(@"datacount:%d and rowcount: %d",dataCount,totalrow);
                    
                    
                }
                
            }
            else
            {
                [self stopLoading];
                [[[UIAlertView alloc]initWithTitle:nil message:@"Error in Server Connection" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
            }
            
            rows_count = [[NSMutableArray alloc] init];
            for (int i=0; i<[date_array count]; i++) {
                
                [rows_count addObject:[NSNumber numberWithInteger:[[sectionedData objectForKey:[date_array objectAtIndex:i]] count] ]];
                
            }
            
            DebugLog(@"ROWS:%@",rows_count);
            
        }];
        
        [self performSelector:@selector(loadOwnData) withObject:nil];
        
        
    }];
    
    
}

-(void)loadOwnData
{
    NSString *urlString=[NSString stringWithFormat:@"%@/%@/profile_ios/view_user?user_id=%@&loggedin_id=%@",GLOBALAPI,INDEX,userId,[[NSUserDefaults standardUserDefaults] valueForKey:UDUSERID]];
    
    
    DebugLog(@"profile url:%@",urlString);
    
    urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    result=[[NSDictionary alloc]init];
    
    NSData *urlData1=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
    
    [[NSOperationQueue mainQueue]addOperationWithBlock:^{
        
        
        if(urlData1==nil)
        {
            [self stopLoading];
            [[[UIAlertView alloc]initWithTitle:nil message:@"Error in Server Connection" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
            
        }
        else{
            [self stopLoading];
            result=[NSJSONSerialization JSONObjectWithData:urlData1 options:kNilOptions error:nil];
            // DebugLog(@"Profile result is:%@",result);
            
            
            if([[result valueForKey:@"status"]isEqualToString:@"Error" ])
            {
                
                [[[UIAlertView alloc]initWithTitle:@"Sorry!!!" message:@"ERROR" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                
            }
            else{
                
                response = [result objectForKey:@"response"];
                
                [self displayData];
                
            }
            
        }
        
        
        
    }];
    
}


-(void)getFinalArray:(NSArray *)dict withSection:(NSInteger)section{
    
    
    DebugLog(@"dict is: %@",dict);
    
    
    tempArray=[[NSMutableArray alloc]init];
    
    NSInteger Count=[dict count]/2;
    Count=([dict count]%2>0)?Count+1:Count+0;
    NSInteger totalcount=0;
    for (NSInteger J=0; J<Count; J++)
    {
        NSMutableDictionary *mutyDic=[[NSMutableDictionary alloc] init];
        if ([dict count]%2>0 && J==Count-1)
        {
            [mutyDic setObject:[dict objectAtIndex:totalcount] forKey:@"firstCell"];
            totalcount+=1;
            [mutyDic setObject:@"" forKey:@"secondCell"];
            [tempArray addObject:mutyDic];
        }
        else
        {
            [mutyDic setObject:[dict objectAtIndex:totalcount] forKey:@"firstCell"];
            totalcount+=1;
            [mutyDic setObject:[dict objectAtIndex:totalcount] forKey:@"secondCell"];
            totalcount+=1;
            [tempArray addObject:mutyDic];
        }
    }
    
    
    [finalTipDict setObject:tempArray forKey:[NSString stringWithFormat:@"%ld",(long)section]];
    
    //[finalTipArray addObject:tempArray];
    
    DebugLog(@"no of rows %ld",(unsigned long)tempArray.count);
    
    
}

- (IBAction)showAbout:(id)sender {
    
    
    [UIView animateWithDuration:0.35
     
                     animations:^{
                         
                         aboutBtn.userInteractionEnabled = NO;
                         
                         if (!dotTap) {
                             
                             CGSize cSize=[_mainScroll contentSize];
                             
                             cSize.height=cSize.height+_aboutLabel.frame.size.height;
                             
                             
                             [_mainScroll setContentSize:cSize];
                             
                             
                             CGRect  view=_contentTable.frame;
                             view.origin.y=view.origin.y+_aboutLabel.frame.size.height;
                             _contentTable.frame=view;
                             
                             view=betView.frame;
                             view.origin.y=view.origin.y+_aboutLabel.frame.size.height;
                             betView.frame=view;
                             
                             view=postView.frame;
                             view.origin.y=view.origin.y+_aboutLabel.frame.size.height;
                             postView.frame=view;
                             
                             view=followerView.frame;
                             view.origin.y=view.origin.y+_aboutLabel.frame.size.height;
                             followerView.frame=view;
                             
                             view=followingView.frame;
                             view.origin.y=view.origin.y+_aboutLabel.frame.size.height;
                             followingView.frame=view;
                             
                             
                         }
                         else
                         {
                             CGSize cSize=[_mainScroll contentSize];
                             
                             cSize.height=cSize.height-_aboutLabel.frame.size.height;
                             
                             
                             [_mainScroll setContentSize:cSize];
                             
                             
                             
                             
                             CGRect  view=betView.frame;
                             view.origin.y=view.origin.y-_aboutLabel.frame.size.height;
                             betView.frame=view;
                             
                             view=postView.frame;
                             view.origin.y=view.origin.y-_aboutLabel.frame.size.height;
                             postView.frame=view;
                             
                             view=followerView.frame;
                             view.origin.y=view.origin.y-_aboutLabel.frame.size.height;
                             followerView.frame=view;
                             
                             view=followingView.frame;
                             view.origin.y=view.origin.y-_aboutLabel.frame.size.height;
                             followingView.frame=view;
                             
                             view=_contentTable.frame;
                             view.origin.y=view.origin.y-_aboutLabel.frame.size.height;
                             _contentTable.frame=view;
                         }
                         
                         
                     }
                     completion:^(BOOL finished){
                         
                         // [aboutBtn setHidden:NO];
                         
                         if (!dotTap) {
                             dotTap = TRUE;
                             [_aboutLabel setHidden:NO];
                             
                         }
                         else
                         {
                             dotTap = FALSE;
                             [_aboutLabel setHidden:YES];
                         }
                         
                         aboutBtn.userInteractionEnabled = YES;
                         
                     }];
    
    
    
}



-(void)displayData
{
    DebugLog(@"OTHER USER DETAILS:%@",response);
    
    
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    [self stopLoading];
    [_contentTable setUserInteractionEnabled:YES];
    [_mainScroll setUserInteractionEnabled:YES];
    
    
    _mainScroll.hidden = NO;
    //   [[NSUserDefaults standardUserDefaults] setObject:[NSURL URLWithString:[response objectForKey:@"profileimage"]] forKey:@"profilepic"];
    
    
    if (_isfromOther) {
        
        
        
        
        [_message_btn setBackgroundColor:[UIColor colorWithRed:(255.0f/255.0f) green:(127.0f/255.0f) blue:(0.0f/255.0f) alpha:1]];
        
        [_message_btn setTitle:@"Message" forState:UIControlStateNormal];
        _message_btn.hidden = NO;
        _message_btn.enabled = YES;
        _message_btn.userInteractionEnabled = YES;
        [_message_btn addTarget:self action:@selector(send_message:) forControlEvents:UIControlEventTouchUpInside];
        
        
        [_follw_unfollw setBackgroundColor:[UIColor colorWithRed:(255.0f/255.0f) green:(127.0f/255.0f) blue:(0.0f/255.0f) alpha:1]];
        
        
        FollowCheck=[[response objectForKey:@"followed_by"]intValue];
        privateCheck=[[response objectForKey:@"private_status"]intValue];
        
        
        if(FollowCheck==1)
        {
            
            _follow_text = @"Unfollow";
            
        }
        else    if(FollowCheck==0)
        {
            
            _follow_text = @"Follow";
            
            
        }
        else{
            _follow_text = @"Withdraw";
            
        }
        
        
        [_follw_unfollw setTitle:_follow_text forState:UIControlStateNormal];
        
        [_follw_unfollw addTarget:self action:@selector(follow_unfollow_Func:) forControlEvents:UIControlEventTouchUpInside];
        
        _message_btn.hidden = NO;
        _message_btn.enabled = YES;
        _message_btn.userInteractionEnabled = YES;
        
        
        _follw_unfollw.hidden = NO;
        _follw_unfollw.enabled = YES;
        _follw_unfollw.userInteractionEnabled = YES;
        
        
    }
    else if (isFromRequest)
    {
        FollowCheck=[[response objectForKey:@"followed_by"]intValue];
        privateCheck=[[response objectForKey:@"private_status"]intValue];
        
        [_message_btn setBackgroundColor:[UIColor redColor]];
        [_message_btn setTitle:@"Reject" forState:UIControlStateNormal];
        [_message_btn addTarget:self action:@selector(rejectRequest:) forControlEvents:UIControlEventTouchUpInside];
        
        [_follw_unfollw setBackgroundColor:[UIColor colorWithRed:(52.0f/255.0f) green:(217.0f/255.0f) blue:(25.0f/255.0f) alpha:1]];
        [_follw_unfollw setTitle:@"Accept" forState:UIControlStateNormal];
        [_follw_unfollw addTarget:self action:@selector(acceptRequest:) forControlEvents:UIControlEventTouchUpInside];
        
        _message_btn.hidden = NO;
        _message_btn.enabled = YES;
        _message_btn.userInteractionEnabled = YES;
        
        _follw_unfollw.hidden = NO;
        _follw_unfollw.enabled = YES;
        _follw_unfollw.userInteractionEnabled = YES;
        
        
    }
    
    [_profilePic sd_setImageWithURL:[NSURL URLWithString:[response objectForKey:@"profileimage"]] placeholderImage:    [UIImage imageNamed:@"profile-pic-2"]];
    
    [_coverView sd_setImageWithURL:[NSURL URLWithString:[response objectForKey:@"background_img"]] placeholderImage:    [UIImage imageNamed:@"loading-image"]];
    
    _coverView.backgroundColor = [UIColor blackColor];
    _coverView.contentMode = UIViewContentModeScaleAspectFit;
    
    
    
    _profileName.text = [[response objectForKey:@"first_name"] stringByAppendingString:[NSString stringWithFormat:@" %@", [response objectForKey:@"last_name"] ]];
    
    
    if([[response objectForKey:@"username"] length]>0)
        _dummyText.text = [NSString stringWithFormat:@" @%@", [response objectForKey:@"username"]];
    else
        _dummyText.text = [NSString stringWithFormat:@" %@", [response objectForKey:@"username"]];
    
    _aboutLabel.text = [NSString stringWithFormat:@" %@", [response objectForKey:@"bio"]];
    _aboutLabel.numberOfLines = 0;
    CGSize size = [ _aboutLabel.text sizeWithFont:_aboutLabel.font
                                constrainedToSize:CGSizeMake(_aboutLabel.frame.size.width, MAXFLOAT)
                                    lineBreakMode:NSLineBreakByWordWrapping];
    CGRect  labelFrame =  _aboutLabel.frame;
    labelFrame.size.height = size.height;
    _aboutLabel.frame = labelFrame;
    
    if ([[_aboutLabel.text stringByTrimmingCharactersInSet:whitespace] length]>0) {
        [aboutBtn setHidden:NO];
    }
    
    
    _followers_count.text = [NSString stringWithFormat:@"%@", [response objectForKey:@"followers"] ];
    _following_count.text = [NSString stringWithFormat:@"%@", [response objectForKey:@"following"] ];
    _post_count.text = [NSString stringWithFormat:@"%@", [response objectForKey:@"total_post"] ];
    _bet_count.text = [NSString stringWithFormat:@"%@", [response objectForKey:@"total_bets"] ];
    privateCheck=[[response objectForKey:@"private_status"]intValue];
    
    DebugLog(@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:UDUSERID]);
    
    if(totalCount>0 && (privateCheck==0 || (privateCheck==1 && FollowCheck==1) || (privateCheck==1 && [userId isEqualToString:[[NSUserDefaults standardUserDefaults] valueForKey:UDUSERID]])))
    {
        
        
        [_contentTable setHidden:NO];
        
        _contentTable.delegate = self;
        _contentTable.dataSource = self;
        _contentTable.backgroundColor = [UIColor whiteColor];
        
        
        
        DebugLog(@"initial scroll height %f",_mainScroll.contentSize.height);
        
        CGRect tableRectB = _contentTable.frame;
        
        CGRect tableRect = _contentTable.frame;
        
        
        
        
        tableRect.size.height=(110.0/568.0*self.view.frame.size.height)*totalrow+((40.0/568.0*self.view.frame.size.height)*date_array.count)+50.0/568.0*self.view.frame.size.height;
        
        
        DebugLog(@" table height %f",tableRect.size.height);
        
        
        _contentTable.frame=tableRect;
        
        
        
        //    if([_aboutLabel isHidden])
        //    {
        //        DebugLog(@"hidden");
        
        [_mainScroll setContentSize:CGSizeMake(_contentTable.frame.size.width, _contentTable.frame.size.height+(_mainScroll.contentSize.height-tableRectB.size.height))];
        //}
        //    else{
        //           DebugLog(@"not hidden");
        //
        //        [_mainScroll setContentSize:CGSizeMake(_contentTable.frame.size.width, _contentTable.frame.size.height+(_mainScroll.contentSize.height-tableRectB.size.height)+_aboutLabel.frame.size.height)];
        //
        //    }
        
        
        DebugLog(@" scroll height %f",_mainScroll.contentSize.height);
        
        [_contentTable reloadData];
        
    }
    else{
        
        
        
        
        
        if(privateCheck==1 && ![userId isEqualToString:[[NSUserDefaults standardUserDefaults]valueForKey:UDUSERID]] && (FollowCheck==0 || FollowCheck==2))
        {
            DebugLog(@"Follow CHeck:%d",FollowCheck);
            noTipLbl.text=@"Posts are not available because this user is private";
            [noTipLbl setHidden:NO];
            [noTipLbl setNumberOfLines:2];
             [_contentTable setHidden:YES];
        }
        else if(totalCount<1 && ![userId isEqualToString:[[NSUserDefaults standardUserDefaults]valueForKey:UDUSERID]])
        {
            noTipLbl.text=@"No Post Available";
            [noTipLbl setHidden:NO];
            [noTipLbl setNumberOfLines:2];
             [_contentTable setHidden:YES];
            
            
        }
        
        
        
    }
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    
    NSString * dateString = [NSString stringWithFormat:@"%@",[[date_array objectAtIndex:section] substringWithRange:NSMakeRange(5, 2)]];
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM"];
    NSDate* myDate = [dateFormatter dateFromString:dateString];
    
    [dateFormatter setDateFormat:@"MMMM"];
    NSString *stringFromDate = [dateFormatter stringFromDate:myDate];
    DebugLog(@"MONTH:%@",stringFromDate);
    stringFromDate = [stringFromDate stringByAppendingString:[NSString stringWithFormat:@" %@",[[date_array objectAtIndex:section] substringWithRange:NSMakeRange(8, 2)] ]];
    
    stringFromDate = [stringFromDate stringByAppendingString:[NSString stringWithFormat:@",%@",[[date_array objectAtIndex:section] substringWithRange:NSMakeRange(0, 4)] ]];
    return stringFromDate;
    //return [date_array objectAtIndex:section];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40.0/568.0*self.view.frame.size.height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 110.0/568.0*self.view.frame.size.height;
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    [self getFinalArray:[sectionedData objectForKey:[NSString stringWithFormat:@"%@", [date_array objectAtIndex:section] ]] withSection:section];
    
    DebugLog(@"no of rows for section %ld is %ld",(long)section,tempArray.count);
    
    //    totalrow+=tempArray.count;
    
    return tempArray.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellIdentifier";
    
    TRProfileCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    
    if (cell == nil)
    {
        // DebugLog(@"CELL ALLOC");
        
        cell = [[TRProfileCellTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    //    cell.View1.hidden = YES;
    
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    
    
    DebugLog(@"in section:%ld",(long)indexPath.section);
    
    
   
    
    
    NSArray *displayArr=[[finalTipDict objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.section] ]objectAtIndex:indexPath.row]  ;
    

    NSArray *firstTipArr=[[displayArr valueForKey:@"firstCell"]objectAtIndex:0];
    
    
            if(firstTipArr.count==1 )
            {
    
               [cell.cell_image1 setFrame:CGRectMake(55.0/145.0*cell.View1.frame.size.width, 27.5/90.0*cell.View1.frame.size.height,35 , 35)];

    
    
                cell.cell_image1.image=[UIImage imageNamed:[tipImageArray objectAtIndex:[[NSString stringWithFormat:@"%@",[[firstTipArr objectAtIndex:0 ]valueForKey:@"tip_status"] ] intValue] -1]];
    
    
                      [cell.cell_image1 setHidden:NO];
                [cell.cell_image2 setHidden:YES];
                   [cell.cell_image3 setHidden:YES];
                   [cell.cell_image4 setHidden:YES];
    
            }
            if(firstTipArr.count==2 )
            {
    
                [cell.cell_image1 setFrame:CGRectMake(25.0/145.0*cell.View1.frame.size.width, 27.5/90.0*cell.View1.frame.size.height,35, 35)];
    
                   [cell.cell_image2 setFrame:CGRectMake(85.0/145.0*cell.View1.frame.size.width, 27.5/90.0*cell.View1.frame.size.height,35 , 35)];
    
    
                [cell.cell_image1 setHidden:NO];
                [cell.cell_image2 setHidden:NO];
                [cell.cell_image3 setHidden:YES];
                [cell.cell_image4 setHidden:YES];
    
                    cell.cell_image1.image=[UIImage imageNamed:[tipImageArray objectAtIndex:[[NSString stringWithFormat:@"%@",[[firstTipArr objectAtIndex:0 ]valueForKey:@"tip_status"] ] intValue] -1]];
    
                            cell.cell_image2.image=[UIImage imageNamed:[tipImageArray objectAtIndex:[[NSString stringWithFormat:@"%@",[[firstTipArr objectAtIndex:1 ]valueForKey:@"tip_status"] ] intValue] -1]];
    
    
    
    
    
            }
            if(firstTipArr.count==3 )
            {
    
                [cell.cell_image1 setFrame:CGRectMake(25.0/145.0*cell.View1.frame.size.width, 8.0/90.0*cell.View1.frame.size.height,35 , 35)];
    
                [cell.cell_image2 setFrame:CGRectMake(85.0/145.0*cell.View1.frame.size.width, 8.0/90.0*cell.View1.frame.size.height,35 , 35)];
    
                    [cell.cell_image3 setFrame:CGRectMake(25.0/145.0*cell.View1.frame.size.width, 47.0/90.0*cell.View1.frame.size.height,35, 35)];
    
                [cell.cell_image1 setHidden:NO];
                [cell.cell_image2 setHidden:NO];
                [cell.cell_image3 setHidden:NO];
                [cell.cell_image4 setHidden:YES];
    
    
                cell.cell_image1.image=[UIImage imageNamed:[tipImageArray objectAtIndex:[[NSString stringWithFormat:@"%@",[[firstTipArr objectAtIndex:0 ]valueForKey:@"tip_status"] ] intValue] -1]];
    
                cell.cell_image2.image=[UIImage imageNamed:[tipImageArray objectAtIndex:[[NSString stringWithFormat:@"%@",[[firstTipArr objectAtIndex:1 ]valueForKey:@"tip_status"] ] intValue] -1]];
    
                   cell.cell_image3.image=[UIImage imageNamed:[tipImageArray objectAtIndex:[[NSString stringWithFormat:@"%@",[[firstTipArr objectAtIndex:2 ]valueForKey:@"tip_status"] ] intValue] -1]];
    
    
    
    
            }
            if(firstTipArr.count>=4 )
            {
    
                [cell.cell_image1 setFrame:CGRectMake(25.0/145.0*cell.View1.frame.size.width, 8.0/90.0*cell.View1.frame.size.height,35 , 35)];
                
                [cell.cell_image2 setFrame:CGRectMake(85.0/145.0*cell.View1.frame.size.width, 8.0/90.0*cell.View1.frame.size.height,35 , 35)];
                
                [cell.cell_image3 setFrame:CGRectMake(25.0/145.0*cell.View1.frame.size.width, 47.0/90.0*cell.View1.frame.size.height,35, 35)];
    
                   [cell.cell_image4 setFrame:CGRectMake(85.0/145.0*cell.View1.frame.size.width, 47.0/90.0*cell.View1.frame.size.height,35, 35)];
    
                [cell.cell_image1 setHidden:NO];
                [cell.cell_image2 setHidden:NO];
                [cell.cell_image3 setHidden:NO];
                [cell.cell_image4 setHidden:NO];
    
    
                cell.cell_image1.image=[UIImage imageNamed:[tipImageArray objectAtIndex:[[NSString stringWithFormat:@"%@",[[firstTipArr objectAtIndex:0 ]valueForKey:@"tip_status"] ] intValue] -1]];
    
                cell.cell_image2.image=[UIImage imageNamed:[tipImageArray objectAtIndex:[[NSString stringWithFormat:@"%@",[[firstTipArr objectAtIndex:1 ]valueForKey:@"tip_status"] ] intValue] -1]];
    
                cell.cell_image3.image=[UIImage imageNamed:[tipImageArray objectAtIndex:[[NSString stringWithFormat:@"%@",[[firstTipArr objectAtIndex:2 ]valueForKey:@"tip_status"] ] intValue] -1]];
                
                  cell.cell_image4.image=[UIImage imageNamed:[tipImageArray objectAtIndex:[[NSString stringWithFormat:@"%@",[[firstTipArr objectAtIndex:3 ]valueForKey:@"tip_status"] ] intValue] -1]];
                
     
                
            }
    

    
    
    
    

    
  
 
 
    
    
    
    
    if (![[displayArr valueForKey:@"secondCell"] isEqual:@""])
    {
         NSArray *secondTipArr=[[displayArr valueForKey:@"secondCell"]objectAtIndex:0];
        
        
        
        
        if(secondTipArr.count==1 )
        {
            
           [cell.cell_image5 setFrame:CGRectMake(55.0/145.0*cell.View2.frame.size.width, 27.5/90.0*cell.View2.frame.size.height,35 , 35)];
            
       
            
            
            cell.cell_image5.image=[UIImage imageNamed:[tipImageArray objectAtIndex:[[NSString stringWithFormat:@"%@",[[secondTipArr objectAtIndex:0 ]valueForKey:@"tip_status"] ] intValue] -1]];
            
            
            [cell.cell_image5 setHidden:NO];
            [cell.cell_image6 setHidden:YES];
            [cell.cell_image7 setHidden:YES];
            [cell.cell_image8 setHidden:YES];
            
        }
        if(secondTipArr.count==2 )
        {
            
            [cell.cell_image5 setFrame:CGRectMake(25.0/145.0*cell.View2.frame.size.width, 27.5/90.0*cell.View2.frame.size.height,35, 35)];
            
            [cell.cell_image6 setFrame:CGRectMake(85.0/145.0*cell.View2.frame.size.width, 27.5/90.0*cell.View2.frame.size.height,35 , 35)];
            
       
            
            [cell.cell_image5 setHidden:NO];
            [cell.cell_image6 setHidden:NO];
            [cell.cell_image7 setHidden:YES];
            [cell.cell_image8 setHidden:YES];
            
            cell.cell_image5.image=[UIImage imageNamed:[tipImageArray objectAtIndex:[[NSString stringWithFormat:@"%@",[[secondTipArr objectAtIndex:0 ]valueForKey:@"tip_status"] ] intValue] -1]];
            
            cell.cell_image6.image=[UIImage imageNamed:[tipImageArray objectAtIndex:[[NSString stringWithFormat:@"%@",[[secondTipArr objectAtIndex:1 ]valueForKey:@"tip_status"] ] intValue] -1]];
            
            
            
            
            
        }
        if(secondTipArr.count==3 )
        {
            
            [cell.cell_image5 setFrame:CGRectMake(25.0/145.0*cell.View2.frame.size.width, 8.0/90.0*cell.View2.frame.size.height,35 , 35)];
            
            [cell.cell_image6 setFrame:CGRectMake(85.0/145.0*cell.View2.frame.size.width, 8.0/90.0*cell.View2.frame.size.height,35 , 35)];
            
            [cell.cell_image7 setFrame:CGRectMake(25.0/145.0*cell.View2.frame.size.width, 47.0/90.0*cell.View2.frame.size.height,35, 35)];
            
         
            
            [cell.cell_image5 setHidden:NO];
            [cell.cell_image6 setHidden:NO];
            [cell.cell_image7 setHidden:NO];
            [cell.cell_image8 setHidden:YES];
            
            
            cell.cell_image5.image=[UIImage imageNamed:[tipImageArray objectAtIndex:[[NSString stringWithFormat:@"%@",[[secondTipArr objectAtIndex:0 ]valueForKey:@"tip_status"] ] intValue] -1]];
            
            cell.cell_image6.image=[UIImage imageNamed:[tipImageArray objectAtIndex:[[NSString stringWithFormat:@"%@",[[secondTipArr objectAtIndex:1 ]valueForKey:@"tip_status"] ] intValue] -1]];
            
            cell.cell_image7.image=[UIImage imageNamed:[tipImageArray objectAtIndex:[[NSString stringWithFormat:@"%@",[[secondTipArr objectAtIndex:2 ]valueForKey:@"tip_status"] ] intValue] -1]];
            
            
        }
        if(secondTipArr.count>=4 )
        {
            
            [cell.cell_image5 setFrame:CGRectMake(25.0/145.0*cell.View2.frame.size.width, 8.0/90.0*cell.View2.frame.size.height,35 , 35)];
            
            [cell.cell_image6 setFrame:CGRectMake(85.0/145.0*cell.View2.frame.size.width, 8.0/90.0*cell.View2.frame.size.height,35 , 35)];
            
            [cell.cell_image7 setFrame:CGRectMake(25.0/145.0*cell.View2.frame.size.width, 47.0/90.0*cell.View2.frame.size.height,35, 35)];
            
            [cell.cell_image8 setFrame:CGRectMake(85.0/145.0*cell.View2.frame.size.width, 47.0/90.0*cell.View2.frame.size.height,35, 35)];
            
            [cell.cell_image5 setHidden:NO];
            [cell.cell_image6 setHidden:NO];
            [cell.cell_image7 setHidden:NO];
            [cell.cell_image8 setHidden:NO];
            
            
            cell.cell_image5.image=[UIImage imageNamed:[tipImageArray objectAtIndex:[[NSString stringWithFormat:@"%@",[[secondTipArr objectAtIndex:0 ]valueForKey:@"tip_status"] ] intValue] -1]];
            
            cell.cell_image6.image=[UIImage imageNamed:[tipImageArray objectAtIndex:[[NSString stringWithFormat:@"%@",[[secondTipArr objectAtIndex:1 ]valueForKey:@"tip_status"] ] intValue] -1]];
            
            cell.cell_image7.image=[UIImage imageNamed:[tipImageArray objectAtIndex:[[NSString stringWithFormat:@"%@",[[secondTipArr objectAtIndex:2 ]valueForKey:@"tip_status"] ] intValue] -1]];
            
            cell.cell_image8.image=[UIImage imageNamed:[tipImageArray objectAtIndex:[[NSString stringWithFormat:@"%@",[[secondTipArr objectAtIndex:3 ]valueForKey:@"tip_status"] ] intValue] -1]];
            
            
            
        }
        
        
        
        
        
        

        
        
        
        [cell.View2 setHidden:NO];
        
    }
    else
    {
        [cell.View2 setHidden:YES];
        
    }

    
    
   
    
        
    
        
  
   
    UILabel *separator = [[UILabel alloc] init];
    separator.frame = CGRectMake(0, 139.5f, [UIScreen mainScreen].bounds.size.width-10, 0.5f);
    separator.backgroundColor = [UIColor colorWithRed:(248.0f/255.0f) green:(247.0f/255.0f) blue:(243.0f/255.0f) alpha:1.0f];
    [cell.contentView addSubview:separator];
    
    
    
    return cell;
}




- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [date_array count];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma UIScrollViewDelegate

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGPoint offset = scrollView.contentOffset;
    CGRect bounds = scrollView.bounds;
    CGSize size = scrollView.contentSize;
    UIEdgeInsets inset = scrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    
    float reload_distance = -60.0f;
    if(y > h + reload_distance)
    {
        
        DebugLog(@"total count is%d and current count is %d",totalCount,dataCount);
        
        if (dataCount<totalCount)
        {
            [_contentTable setUserInteractionEnabled:NO];
            [_mainScroll setUserInteractionEnabled:NO];
            
            [self startLoading:self.view];
            
            page++;
            
            [self performSelector:@selector(loadData) withObject:nil];
        }
    }
    
}

- (void)acceptRequest:(id)sender
{
    
    
    
    if(!_isfromOther)
    {
    NSOperationQueue *operationQueue = [NSOperationQueue new];
    [operationQueue addOperationWithBlock:^{
        NSString *accept_url=[NSString stringWithFormat:@"http://www.esolz.co.in/lab3/tipright/index.php/follow_req_ios/req_accepted?id=%@&loggedin_id=%@",userId,[[NSUserDefaults standardUserDefaults] valueForKey:UDUSERID]];
        
        DebugLog(@"Accept URL:%@",accept_url);
        
        accept_url = [accept_url stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        
        accept_result = [[NSDictionary alloc]init];
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:accept_url]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            [self stopLoading];
            
            
            
            if (urlData!=nil) {
                accept_result = [NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                
                DebugLog(@"ACCEPT RESULT:%@",accept_result);
                
                if([[accept_result valueForKey:@"status"]isEqualToString:@"Success" ])
                {
                    isFromRequest = FALSE;
                    _isfromOther = TRUE;
                    
                    if(![noTipLbl isHidden])
                    {
                        
                        [noTipLbl setHidden:YES];
                        
                    }
                    
                    
                    data_array = [[NSMutableArray alloc] init];
                    page=0;
                    
                    dataCount=0;
                    totalrow=0;
                    sectionCount=0;
                    [self startLoading:self.view];
                    
                    
                    [self loadData];
                    
                }
                else{
                    
                    
                }
                
            }
            else
            {
                [self stopLoading];
                [[[UIAlertView alloc]initWithTitle:nil message:@"Error in Server Connection" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
            }
            
            
        }];
        
        
        
    }];
    }
    
}
- (void)rejectRequest:(id)sender
{
    NSOperationQueue *operationQueue = [NSOperationQueue new];
    [operationQueue addOperationWithBlock:^{
        NSString *deny_url=[NSString stringWithFormat:@"http://www.esolz.co.in/lab3/tipright/index.php/follow_req_ios/req_denied?id=%@&loggedin_id=%@",userId,[[NSUserDefaults standardUserDefaults] valueForKey:UDUSERID]];
        
        DebugLog(@"Accept URL:%@",deny_url);
        
        deny_url = [deny_url stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        
        deny_result = [[NSDictionary alloc]init];
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:deny_url]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            [self stopLoading];
            
            
            
            if (urlData!=nil) {
                deny_result = [NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                
                DebugLog(@"ACCEPT RESULT:%@",deny_result);
                
                if([[deny_result valueForKey:@"status"]isEqualToString:@"Success"])
                {
                    isFromRequest = FALSE;
                    _isfromOther = TRUE;
                    
                    data_array = [[NSMutableArray alloc] init];
                    page=0;
                    
                    dataCount=0;
                    totalrow=0;
                    sectionCount=0;
                    [self startLoading:self.view];
                    
                    [self loadData];
                    
                }
                else{
                    
                    
                }
                
            }
            else
            {
                [self stopLoading];
                [[[UIAlertView alloc]initWithTitle:nil message:@"Error in Server Connection" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
            }
            
            
        }];
        
        
        
    }];
    
    
}



- (void)send_message:(id)sender
{
    TRMessageDetailsViewController *pushView = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"TRmessageDetails"];
    DebugLog(@"MSG Id:%@",userId);
    
    pushView.senderID=[NSString stringWithFormat:@"%@",userId];
    [[self navigationController ]pushViewController:pushView animated:YES];
    
}

- (void)follow_unfollow_Func:(id)sender
{
    
    [self startLoading:self.view];
    
    
    [_follw_unfollw setUserInteractionEnabled:NO];
    
    
    follow_operationQueue = [NSOperationQueue new];
    [follow_operationQueue addOperationWithBlock:^{
        NSString *urlString=[NSString stringWithFormat:@"%@/%@/%@/index?userid=%@&following_id=%@",GLOBALAPI,INDEX,FOLLOW,[[NSUserDefaults standardUserDefaults]valueForKey:UDUSERID ],userId];
        
        
        
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        //urlString =[urlString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        DebugLog(@"url:%@",urlString);
        resultDict=[[NSDictionary alloc]init];
        
        NSData *urlData=[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
        
        [[NSOperationQueue mainQueue]addOperationWithBlock:^{
            
            [self stopLoading];
            
            
            if(urlData==nil)
            {
                
                [[[UIAlertView alloc]initWithTitle:@"Error!!!" message:@"error in internet Connection" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                
                
            }
            else{
                
                resultDict = [NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:nil];
                DebugLog(@"result is:%@",resultDict);
                
                NSDictionary  *follow_response = [resultDict valueForKey:@"response"];
                
                
                if([[follow_response valueForKey:@"status"]isEqualToString:@"Success" ])
                {
                    
                    data_array = [[NSMutableArray alloc] init];
                    page=0;
                    
                    dataCount=0;
                    totalrow=0;
                    sectionCount=0;
                  
                
                    [self startLoading:self.view];
                    
                    [self loadData];
                    
                       [_follw_unfollw setUserInteractionEnabled:YES];
                }
                else{
                    
                    [_follw_unfollw setUserInteractionEnabled:YES];
                    
                    [[[UIAlertView alloc]initWithTitle:@"Sorry!!!" message:[follow_response valueForKey:@"message"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show ];
                    
                }
            }
        }];
    }];
    
}


- (IBAction)leftTipClicked:(UIButton *)sender {
    
    UITableViewCell *cCell=(UITableViewCell *)sender.superview.superview.superview;
    
    NSIndexPath *indexPath = [_contentTable indexPathForCell:cCell];
    
    NSArray *displayArr=[[finalTipDict objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.section] ]objectAtIndex:indexPath.row]  ;
    
    
    NSArray *firstTipArr=[[displayArr valueForKey:@"firstCell"]objectAtIndex:0];
    

    
    TRNotificationDetailsViewController *pushView = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"notiDetails"];
    
    
 
    pushView.tipId=[NSString stringWithFormat:@"%@",[[firstTipArr objectAtIndex:0] valueForKey:@"count_tip_id"]];
    [[self navigationController ]pushViewController:pushView animated:YES];
    
    
}
- (IBAction)rightTipClicked:(UIButton *)sender {
    
    UITableViewCell *cCell=(UITableViewCell *)sender.superview.superview.superview;
    NSIndexPath *indexPath = [_contentTable indexPathForCell:cCell];
    
    NSArray *displayArr=[[finalTipDict objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.section] ]objectAtIndex:indexPath.row]  ;
    
    
    NSArray *secondTipArr=[[displayArr valueForKey:@"secondCell"]objectAtIndex:0];
    
    TRNotificationDetailsViewController *pushView = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"notiDetails"];
    
    
    
    pushView.tipId=[NSString stringWithFormat:@"%@",[[secondTipArr objectAtIndex:0] valueForKey:@"count_tip_id"]];
    [[self navigationController ]pushViewController:pushView animated:YES];
    
    
}

- (IBAction)followingBtn_Press:(id)sender {
    
    TRFollowingListViewController *pushView = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"following"];
    pushView.userId=userId;
    
    //pushView.isFromRequest=YES;
    
    
    [[self navigationController ]pushViewController:pushView animated:YES];
    
    
}

- (IBAction)followersBtn_Press:(id)sender {
    TRFollowerListViewController *pushView = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"followers"];
    //pushView.userId=[[NSUserDefaults standardUserDefaults]valueForKey:UDUSERID];
    //pushView.isFromRequest=YES;
    pushView.userId=userId;
    
    [[self navigationController ]pushViewController:pushView animated:YES];
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
