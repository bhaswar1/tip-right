//
//  TRChangePasswordViewController.h
//  Tip Right
//
//  Created by intel on 25/06/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import "TRGlobalMethods.h"

@interface TRChangePasswordViewController : TRGlobalMethods
@property (strong, nonatomic) IBOutlet UITextField *oldPass_Field;
@property (strong, nonatomic) IBOutlet UITextField *NewPass_Field;
@property (strong, nonatomic) IBOutlet UITextField *confirmPass_Field;
@property (strong, nonatomic) IBOutlet UIButton *save_btn;

@end
