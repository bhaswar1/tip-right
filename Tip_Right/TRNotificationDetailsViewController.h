//
//  TRNotificationDetailsViewController.h
//  Tip Right
//
//  Created by intel on 26/06/15.
//  Copyright (c) 2015 esolz. All rights reserved.
//

#import "TRGlobalMethods.h"

@interface TRNotificationDetailsViewController : TRGlobalMethods
@property (strong, nonatomic) IBOutlet UIImageView *profilePic;
@property (strong, nonatomic) IBOutlet UILabel *profName;
@property (strong, nonatomic) IBOutlet UIImageView *heartImage;
@property (strong, nonatomic) IBOutlet UIButton *like_btn;
@property (strong, nonatomic) IBOutlet UILabel *like_status;
@property (strong, nonatomic) IBOutlet UILabel *timeAgo;
@property (strong, nonatomic) IBOutlet UIImageView *banner_Image;
@property(nonatomic,retain) NSString *tipId;
@property (nonatomic, strong) NSDictionary *dict;
@property (nonatomic, assign) BOOL like;
@property (nonatomic, strong) NSString *date;


@end
